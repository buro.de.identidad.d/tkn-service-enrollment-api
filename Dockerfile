FROM openjdk:8-jre-alpine
RUN apk update
RUN apk add --no-cache tzdata
ENV TZ=America/Mexico_City
COPY target/tkn-service-enrollment-api-1.0.2.jar /home/tkn-service-enrollment-api.jar
COPY script_init.sh /home/script_init.sh
RUN chmod 777 /home/script_init.sh
#RUN mkdir -p /opt/bidserver/logs
COPY docs/ /home/
ENTRYPOINT ["/home/script_init.sh"]
