package com.teknei.bid.service.ine.request;

import lombok.Data;

@Data
public class ValidateIneRequest {
	private float ocr;
	private float cic;
	private String name;
	private String fatherLastName;
	private String motherLastName;
	private String registryYear;
	private String emissionYear;
	private String electorCode;
	private String curp;
	private String emissionNumber;
	Others OthersObject;

}
