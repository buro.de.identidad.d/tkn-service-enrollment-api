package com.teknei.bid.service.ine.request;

import java.util.ArrayList;

import lombok.Data;

@Data
public class Others {
	ArrayList<Bimetric> bimetric = new ArrayList<Bimetric>();
	Location LocationObject;
	private boolean consent;

	@Data
	public class Bimetric {
		String dataType;
		String fingerNumber;
		String fingerPrint;
	}

	@Data
	public class Location {
		private float latitude;
		private float longitude;
		private String country;
		private String state;
		private String zipCode;

	}
}
