package com.teknei.bid.service.remote;

import com.teknei.bid.dto.*;

import org.json.JSONObject;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(value = "${tkn.feign.management-name}")
public interface ManagementClient {

    @RequestMapping(value = "/admin/usua", method = RequestMethod.GET)
    ResponseEntity<List<BidUsuaDTO>> findAllActive();

    @RequestMapping(value = "/admin/usua", method = RequestMethod.POST)
    ResponseEntity<BidUsuaDTO> insertUsua(@RequestBody BidUsuaDTO dto);

    @RequestMapping(value = "/admin/usua", method = RequestMethod.PUT)
    ResponseEntity<BidUsuaDTO> modifyUsua(@RequestBody BidUsuaDTO dto);
    
    /**
     * Servicio para la modificacion de contraseña 
     * @author AJGD
     * @param dto tipo oper 0 = vigencia, 1 = recuperacion;
     * @param request
     * @return
     */
    @RequestMapping(value = "/admin/usua/updatePassword", method = RequestMethod.PUT)
    ResponseEntity<String> updatePassword(@RequestBody BidUpdatePassUsuaDTO dto);

    @RequestMapping(value = "/admin/usua", method = RequestMethod.DELETE)
    ResponseEntity<BidUsuaDTO> deleteUsua(@RequestBody BidUsuaDTO dto);

    @RequestMapping(value = "/admin/empr", method = RequestMethod.GET)
    ResponseEntity<List<BidEmprDTO>> findAllEmprActive();

    @RequestMapping(value = "/admin/empr", method = RequestMethod.POST)
    ResponseEntity<BidEmprDTO> saveEmpr(@RequestBody BidEmprDTO dto);

    @RequestMapping(value = "/admin/empr", method = RequestMethod.PUT)
    ResponseEntity<BidEmprDTO> updateEmpr(@RequestBody BidEmprDTO dto);

    @RequestMapping(value = "/admin/empr", method = RequestMethod.DELETE)
    ResponseEntity<BidEmprDTO> deleteEmpr(@RequestBody BidEmprDTO dto);

    @RequestMapping(value = "/admin/disp", method = RequestMethod.GET)
    ResponseEntity<List<BidDispDTO>> findAllDispActive();

    @RequestMapping(value = "/admin/disp", method = RequestMethod.POST)
    ResponseEntity<BidDispDTO> createDisp(@RequestBody BidDispDTO dto);

    @RequestMapping(value = "/admin/disp", method = RequestMethod.PUT)
    ResponseEntity<BidDispDTO> updateDisp(@RequestBody BidDispDTO dto);

    @RequestMapping(value = "/admin/disp", method = RequestMethod.DELETE)
    ResponseEntity<BidDispDTO> deleteDisp(@RequestBody BidDispDTO dto);

    @RequestMapping(value = "/admin/assign/companyUser", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Boolean> assignCompanyUser(@RequestBody BidAssignmentRequestDTO requestDTO);

    @RequestMapping(value = "/admin/assign/companyDevice", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Boolean> assignCompanyDevice(@RequestBody BidAssignmentRequestDTO requestDTO);

    @RequestMapping(value = "/admin/assign/companyUser/{idUser}", method = RequestMethod.GET)
    ResponseEntity<List<BidEmprUsua>> findCompanyFromUser(@PathVariable("idUser") Long idUser);

    @RequestMapping(value = "/admin/assign/companyDevice/{idDevice}", method = RequestMethod.GET)
    ResponseEntity<List<BidEmprDisp>> findCompanyFromDevice(@PathVariable("idDevice") Long idDevice);

}