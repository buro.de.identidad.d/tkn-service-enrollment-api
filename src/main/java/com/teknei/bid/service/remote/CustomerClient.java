package com.teknei.bid.service.remote;

import java.util.List;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.teknei.bid.dto.BidClieCtaDest;
import com.teknei.bid.dto.BidClientAccountDestinyDTO;
import com.teknei.bid.dto.BidCreditInstitutionRequest;
import com.teknei.bid.dto.BidDocuRelDTO;
import com.teknei.bid.dto.BidIfeValidationDTO;
import com.teknei.bid.dto.BidIfeValidationRequestDTO;
import com.teknei.bid.dto.BidInstCred;
import com.teknei.bid.dto.BidReasonFingers;
import com.teknei.bid.dto.BidServiceConfiguration;
import com.teknei.bid.dto.BidTipoClienteDTO;
import com.teknei.bid.dto.ClieQRDTO;
import com.teknei.bid.dto.ClieQRDTORequest;
import com.teknei.bid.dto.ClientDTO;
import com.teknei.bid.dto.ClientDetailDTO;
import com.teknei.bid.dto.CurpDTO;
import com.teknei.bid.dto.DetailTSRecordDTO;
import com.teknei.bid.dto.OpenCasefileDTO;
import com.teknei.bid.dto.StepStatusDTO;
import com.teknei.bid.oauth.domain.User;

@FeignClient(value = "${tkn.feign.customer-name}")
public interface CustomerClient {

    @RequestMapping(value = "/client/validate/information/ine", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity<List<BidIfeValidationDTO>> findValidation(@RequestBody BidIfeValidationRequestDTO validationRequestDTO);
    
    @RequestMapping(value = "/client/validate/information/service", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, 
    		produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity<BidServiceConfiguration> findServices(@RequestBody String isActivo);
    
    @RequestMapping(value = "/client/information/tipocliente", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, 
    		produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity<List<BidTipoClienteDTO>> getCatalogTipoCliente();

    @RequestMapping(value = "/client/update", method = RequestMethod.PUT)
    ResponseEntity<ClientDTO> updateClient(@RequestBody ClientDTO clientDTO);

    @RequestMapping(value = "/client/add", method = RequestMethod.POST)
    ResponseEntity<ClientDTO> addClient(@RequestBody ClientDTO clientDTO);
    
    @RequestMapping(value = "/client/addDemo", method = RequestMethod.POST)
    ResponseEntity<ClientDTO> addClientDemo(@RequestBody ClientDTO clientDTO);

    @RequestMapping(value = {"/client/ts/id/{id}"}, method = RequestMethod.GET)
    ResponseEntity<DetailTSRecordDTO> findDetailTSId(@PathVariable("id") Long id);

    @RequestMapping(value = {"/client/ts/curp/{curp}"}, method = RequestMethod.GET)
    ResponseEntity<DetailTSRecordDTO> findDetailTSCurp(@PathVariable("curp") String curp);

    @RequestMapping(value = {"/client/detail/id/{id}"}, method = RequestMethod.GET)
    ResponseEntity<ClientDetailDTO> findDetailId(@PathVariable("id") Long id);

    @RequestMapping(value = {"/client/obtienePerfilCliente/{id}"}, method = RequestMethod.GET)
    ResponseEntity<Integer> obtienePerfilCliente(@PathVariable("id") long  id);
    
    @RequestMapping(value = "/client/updatePerfil", method = RequestMethod.PUT)
    ResponseEntity<Boolean> updateClientPerfil(@RequestBody String  clientDTO);
    
    @RequestMapping(value = {"/client/detail/curp/{curp}"}, method = RequestMethod.GET)
    ResponseEntity<ClientDetailDTO> findDetailCurp(@PathVariable("curp") String curp);

    @RequestMapping(value = {"/client/detailByPerfil/curp/{curp}/{perfil}"}, method = RequestMethod.GET)
    ResponseEntity<ClientDetailDTO> FindDetailByCurpAndPerfil(@PathVariable("curp") String curp,@PathVariable("perfil") String perfil);    
    
    @RequestMapping(value = {"/client/detail/mail/{mail}"}, method = RequestMethod.GET)
    ResponseEntity<ClientDetailDTO> findDetaiMail(@PathVariable("mail") String mail);

    @RequestMapping(value = {"/client/detail/tel/{tel}"}, method = RequestMethod.GET)
    ResponseEntity<ClientDetailDTO> findDetailTel(@PathVariable("tel") String tel);

    //@RequestMapping(value = {"/client/detail/cel/{cel}"}, method = RequestMethod.GET)
    //ResponseEntity<ClientDetailDTO> findDetailCel(@PathVariable("cel") String cel);

    @RequestMapping(value = {"/client/step/id/{id}"})
    ResponseEntity<Integer> findStepFromReferenceId(@PathVariable("id") Long id);

    @RequestMapping(value = {"/client/step/curp/{curp}"})
    ResponseEntity<Integer> findStepFromReferenceCurp(@PathVariable("curp") String curp);

    @RequestMapping(value = "/client/findProcesses/openedForToday", method = RequestMethod.GET)
    ResponseEntity<List<CurpDTO>> findOpenProcess();

    @RequestMapping(value = "/client/findProcesses/contractsForToday", method = RequestMethod.GET)
    ResponseEntity<List<CurpDTO>> findOpenForContractProcess();

    @RequestMapping(value = "/client/verify/biometric/{operationId}", method = RequestMethod.GET)
    ResponseEntity<Boolean> findVerificationForCandidate(@PathVariable("operationId") Long operationId);

    @RequestMapping(value = "/client/accountDestiny/{idClient}", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity<List<BidClieCtaDest>> findRelatedAccounts(@PathVariable("idClient") Long idClient);

    @RequestMapping(value = "/client/accountDestinyInactive/{idClient}", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity<List<BidClieCtaDest>> findRelatedAccountsInactive(@PathVariable("idClient") Long idClient);

    @RequestMapping(value = "/client/accountDestiny", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity<BidClieCtaDest> saveCtaDest(@RequestBody BidClientAccountDestinyDTO request);

    @RequestMapping(value = "/client/creditInstitution", method = RequestMethod.GET)
    ResponseEntity<List<BidInstCred>> findInstCred();

    @RequestMapping(value = "/client/creditInstitution", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity<BidInstCred> saveInstCred(@RequestBody BidCreditInstitutionRequest request);
    
    
    @RequestMapping(value = "/client/creditInstitution", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity<Boolean> saveInstCred(@RequestBody List<BidInstCred> bankList);

    //TODO AJGD  -->>
    @RequestMapping(value = "/client/validaUsuario/{usuario}/{password}", method = RequestMethod.GET)
    ResponseEntity<String> validaUsuario(@PathVariable("usuario") String usuario,@PathVariable("password") String password);
    
    @RequestMapping(value = "/client/getPrivacyNotice", method = RequestMethod.GET)
    ResponseEntity<String> getPrivacyNotice();   

    @RequestMapping(value = "/client/getReasonFingers", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, 
    		produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity<List<BidReasonFingers>> getReasonFingers();
    
    @RequestMapping(value = "/client/getUsuarioLogin/{nombre}/{paterno}/{materno}", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, 
    		produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity<String> getUsuarioLogin(@PathVariable("nombre") String nombre,@PathVariable("paterno") String paterno,@PathVariable("materno") String materno);
    
    @RequestMapping(value = "/client/initCasefile", method = RequestMethod.POST)
    ResponseEntity<Boolean> openCasefile(@RequestBody OpenCasefileDTO openCasefileDTO);

    @RequestMapping(value = "/client/checkCasefile", method = RequestMethod.GET)
    ResponseEntity<String> findCasefile(@RequestBody OpenCasefileDTO openCasefileDTO);

    @RequestMapping(value = "/client/openRegProc", method = RequestMethod.POST)
    ResponseEntity<Boolean> openRegProc(@RequestBody OpenCasefileDTO openCasefileDTO);

    @RequestMapping(value = "/clieDocs/findRelationsActive", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity<List<BidDocuRelDTO>> findActiveDocumentRelations();

    @RequestMapping(value = "/client/accomplishment/{id}", method = RequestMethod.GET)
    ResponseEntity<String> findAccomplishmnet(@PathVariable("id") Long id);

    @RequestMapping(value = "/clientQR/qr", method = RequestMethod.POST)
    ResponseEntity<ClieQRDTO> generateQRToCotinueWithBiometric(@RequestBody ClieQRDTORequest qrdtoRequest);

    @RequestMapping(value = "/clientQR/qr", method = RequestMethod.PUT)
    ResponseEntity<String> validateQR(@RequestBody ClieQRDTO qrdto);
}