package com.teknei.bid.service.remote;

import com.teknei.bid.dto.DocumentPictureRequestDTO;
import com.teknei.bid.dto.RequestEncFilesDTO;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "${tkn.feign.facial-name}")
public interface FacialClient {

    @RequestMapping(value = "/facial/download", method = RequestMethod.POST)
    ResponseEntity<byte[]> getImageFromReference(@RequestBody DocumentPictureRequestDTO dto);

    @RequestMapping(value = "/facial/uploadPlain", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<String> uploadPlain(@RequestBody RequestEncFilesDTO requestEncFilesDTO);

}