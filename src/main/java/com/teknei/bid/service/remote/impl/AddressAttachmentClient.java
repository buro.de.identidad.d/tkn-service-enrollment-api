package com.teknei.bid.service.remote.impl;

import com.teknei.bid.service.remote.Invoker;
import com.teknei.bid.util.EnrollmentVerificationSingleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;

@Component
public class AddressAttachmentClient {
	private static final Logger log = LoggerFactory.getLogger(AddressAttachmentClient.class);
    //@Value(value = "${tkn.feign.address-url}")
    //private String url;
    @Value(value = "${tkn.feign.address-name}")
    private String serviceName;

    @Autowired
    private DiscoveryClient discoveryClient;

    public ResponseEntity<String> upload(MultipartFile file, Long id) 
    {
    	//log.info("lblancas: "+this.getClass().getName()+".{upload() }");
        return Invoker.execute(Invoker.findUrl(serviceName, "address", discoveryClient), id, Arrays.asList(file));
        //return Invoker.execute(url, id, Arrays.asList(file));
    }

}
