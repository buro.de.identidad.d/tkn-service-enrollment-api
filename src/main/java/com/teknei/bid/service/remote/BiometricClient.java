package com.teknei.bid.service.remote;

import com.teknei.bid.dto.BiometricCaptureRequestIdentDTO;
import com.teknei.bid.dto.BiometricComplexResponse;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "${tkn.feign.biometric-name}")
public interface BiometricClient {

    @RequestMapping(value = "/biometric/upload/{id}/{type}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity<String> addMinucias(@RequestBody String jsonRequest, @PathVariable("id") Long id, @PathVariable("type") Integer type);
    
    @RequestMapping(value = "/biometric/uploadForce/{id}/{type}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity<String> addMinuciasForce(@RequestBody String jsonRequest, @PathVariable("id") Long id, @PathVariable("type") Integer type);
    
    @RequestMapping(value = "/biometric/start", method = RequestMethod.POST)
    String initCaptureFor(@RequestBody String serialNumber);

    @RequestMapping(value = "/biometric/status/{serialNumber}", method = RequestMethod.GET)
    String getStatusFor(@PathVariable("serialNumber") String serialNumber);
    
    @RequestMapping(value = "/biometric/isCorrectforce/{idOld}/{idNew}", method = RequestMethod.GET)
    String isCorrectforce(@PathVariable("idOld") String idOld,@PathVariable("idNew") String idNew);

    @RequestMapping(value = "/biometric/getidClieRelacion/{id}", method = RequestMethod.GET)
    String getidClieRelacion(@PathVariable("id") String id);    
    
    @RequestMapping(value = "/biometric/search/customer", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<String> searchByFinger(@RequestBody String jsonStringRequest);

    @RequestMapping(value = "/biometric/search/customerId", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    ResponseEntity<String> searchByFingerAndId(@RequestBody String jsonStringRequest);

    @RequestMapping(value = "/biometric/search/customerSlaps", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    ResponseEntity<String> searchBySlaps(@RequestBody String jsonStringRequest);

    @RequestMapping(value = "/biometric/search/customerSlapsId", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    ResponseEntity<String> searchBySlapsId(@RequestBody String jsonStringRequest);

    @RequestMapping(value = "/biometric/search/customerFace", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    ResponseEntity<String> searchByFace(@RequestBody String jsonStringRequest);

    @RequestMapping(value = "/biometric/search/customerFaceId", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    ResponseEntity<String> searchByFaceId(@RequestBody String jsonStringRequest);

    @RequestMapping(value = "/biometric/startAuth", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    String initCaptureForIdentification(@RequestBody BiometricCaptureRequestIdentDTO requestIdentDTO);

    @RequestMapping(value = "/biometric/startSign", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    String initCaptureSignForIdentification(@RequestBody BiometricCaptureRequestIdentDTO requestIdentDTO);

    @RequestMapping(value = "/biometric/statusAuth/{serial}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    BiometricCaptureRequestIdentDTO getStatusForAuth(@PathVariable("serial") String serial);

    @RequestMapping(value = "/biometric/statusSign/{serial}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    BiometricCaptureRequestIdentDTO getStatusForAuthSign(@PathVariable("serial") String serial);

    @RequestMapping(value = "/biometric/confirmAuth/{status}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    String confirmCaptureAuth(@RequestBody BiometricCaptureRequestIdentDTO requestIdentDTO, @PathVariable("status") Integer status);

    @RequestMapping(value = "/biometric/confirmSign/{status}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    String confirmCaptureSign(@RequestBody BiometricCaptureRequestIdentDTO requestIdentDTO, @PathVariable("status") Integer status);

    @RequestMapping(value = "/biometric/queryAuth", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    String queryCaptureAuth(@RequestBody BiometricCaptureRequestIdentDTO requestIdentDTO);

    @RequestMapping(value = "/biometric/queryAuthSign", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    String queryCaptureSign(@RequestBody BiometricCaptureRequestIdentDTO requestIdentDTO);

    @RequestMapping(value = "/biometric/searchDetail/{id}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    ResponseEntity<BiometricComplexResponse> findDetailRecord(@PathVariable("id") String id);

    @RequestMapping(value = "/biometric/getHash/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity<String> getHashForCustomer(@PathVariable("id") String id);

    @RequestMapping(value = "/biometric/matchFacial", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<String> compareFaces(@RequestBody String jsonStringRequest);

    
    //---------------add type to curp
    @RequestMapping(value = "/biometric/addTypeToCurp/{idClie}/{type}", method = RequestMethod.GET)
    String addTypeToCurp(@PathVariable("idClie") Long idClie, @PathVariable("type") String type);
}