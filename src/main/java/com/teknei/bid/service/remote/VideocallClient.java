package com.teknei.bid.service.remote;

import com.teknei.bid.dto.DocumentPictureRequestDTO;
import com.teknei.bid.dto.VideocallRequestDTO;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "${tkn.feign.videocall-name}")
public interface VideocallClient {

    @RequestMapping(value = "/video/add", method = RequestMethod.POST)
    ResponseEntity<String> addVideocallData(@RequestBody VideocallRequestDTO videocallRequestDTO);


    @RequestMapping(value = "/video/download", method = RequestMethod.POST)
    ResponseEntity<byte[]> getImageFromReference(@RequestBody DocumentPictureRequestDTO dto);

    @RequestMapping(value = "/video/downloadQS", method = RequestMethod.POST)
    ResponseEntity<byte[]> getImageFromQuobisStorage(@RequestBody DocumentPictureRequestDTO dto);

}