package com.teknei.bid.service.remote;

import com.teknei.bid.dto.*;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@FeignClient(value = "${tkn.feign.contract-name}")
public interface ContractClient {

	@RequestMapping(value = "/contract/contrato/{id}/{username}", method = RequestMethod.GET)
	ResponseEntity<byte[]> getUnsignedContract(@PathVariable("id") Long id, @PathVariable("username") String username);

	@RequestMapping(value = "/contract/contractWithCert/{id}/{username}", method = RequestMethod.GET)
	ResponseEntity<byte[]> getUnsignedContractWithCerts(@PathVariable("id") Long id,
			@PathVariable("username") String username);

	@RequestMapping(value = "/contract/contratoB64/{id}/{username}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	ResponseEntity<String> getUnsignedContractB64(@PathVariable("id") Long id,
			@PathVariable("username") String username);

	@RequestMapping(value = "/contract/contrato/sign", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	ResponseEntity<String> signContract(@RequestBody SignContractRequestDTO contractRequestDTO);

	@RequestMapping(value = "/contract/uploadPlain", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	ResponseEntity<String> addPlainSignedContract(@RequestBody ContractSignedDTO contractSignedDTO);

	@RequestMapping(value = "/cert/cert", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	ResponseEntity<String> generateCert(@RequestBody CertKey certKey);

	@RequestMapping(value = "/cert/cert/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<String> getCert(@PathVariable("id") Long id);

	@RequestMapping(value = "/acceptance/acceptancePerCustomer", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<String> updateAcceptancePerCustomer(@RequestBody List<AcceptancePerCustomerDTO> dtoList);

	@RequestMapping(value = "/acceptance/acceptancePerCustomer", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<List<AcceptancePerCustomerDTO>> getAcceptancesPerCustomer(
			@RequestBody OperationIdDTO operationIdDTO);

	@RequestMapping(value = "/contract/demo", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	ResponseEntity<String> getDemo(@RequestBody ContractDemoDTO dto);

	@RequestMapping(value = "/contractTMS/contratoTms", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	ResponseEntity<String> getContratoTms(@RequestBody ContratoTmsDTO dto);

	@RequestMapping(value = "/contractTMS/TMSFideicomiso", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	ResponseEntity<String> getContracTMSFideicomiso(@RequestBody TMSFideicomisoDTO dto);

	@RequestMapping(value = "/contractTMS/TMSFideicomiso2", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	ResponseEntity<byte[]> getContracTMSFideicomiso2(@RequestBody String dto);

	@RequestMapping(value = "/contractTMS/PDFRecursos", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	ResponseEntity<String> getPDFRecursos(@RequestBody TMSDocumentosDTO dto);

	@RequestMapping(value = "/contractTMS/PDFInmuebles", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	ResponseEntity<String> getPDFInmuebles(@RequestBody TMSDocumentosDTO dto);

	@RequestMapping(value = "/contractTMS/saveContracts/{id}", method = RequestMethod.POST)
	ResponseEntity<String> getSaveContractTMS(@PathVariable("id") Long id, @RequestBody SaveDocTmsDTO saveDoc);

//---------------------------------------------
//	@RequestMapping(value = "/contractTMS/manualSiginService", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
//	ResponseEntity<String> manualSiginService(@RequestPart(value = "doc") MultipartFile doc, @RequestBody String request);

	
	@RequestMapping(value = "/contract/testContract", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	ResponseEntity<String> testContract(@RequestBody String dto);
	
	
	@RequestMapping(value = "/getContratoManual/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	ResponseEntity<String> getContratoManual(@PathVariable("id") Long id);
}