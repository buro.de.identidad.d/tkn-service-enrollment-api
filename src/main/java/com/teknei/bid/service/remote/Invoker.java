package com.teknei.bid.service.remote;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public class Invoker {

    private static final Logger log = LoggerFactory.getLogger(Invoker.class);

    public static String findUrl(String serviceName, String toAppend, DiscoveryClient discoveryClient) {
    	log.info("lblancas: Invoker.{findUrl() }");
        List<ServiceInstance> instanceList = discoveryClient.getInstances(serviceName);
        if (instanceList == null) {
            return null;
        }
        ServiceInstance first = instanceList.get(0);
        String foudUri = new StringBuilder(first.getUri().toString()).append("/").append(toAppend).toString();
        log.info("lblancas: Invoker.{findUrl() }   ::"+foudUri);
        return foudUri;
    }

    public static ResponseEntity<String> execute(String urlRemote, Long id, List<MultipartFile> files, String uploadAddressPart){
    	  if (urlRemote == null) {
            return new ResponseEntity<>((String) null, HttpStatus.UNPROCESSABLE_ENTITY);
        }
        log.info("lblancas....com.teknei.bid.service.remote.Invoker.execute...public static ResponseEntity<String> execute(String urlRemote, Long id, List<MultipartFile> files, String uploadAddressPart)  ");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        RestTemplate restTemplate = new RestTemplate();
        String url = new StringBuilder(urlRemote).append("/").append(uploadAddressPart).append("/").append(id).toString();
        HttpEntity<MultiValueMap<String, Object>> httpEntity = null;
        MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        try {
            for (MultipartFile file : files) {
                final String filename = file.getOriginalFilename();
                map.add("name", filename);
                map.add("filename", filename);
                ByteArrayResource contentsAsResource = new ByteArrayResource(file.getBytes()) {
                    @Override
                    public String getFilename() {
                        return filename;
                    }
                };
                map.add("file", contentsAsResource);
            }
        } catch (IOException e) {
            log.error("Error in attachments: {}", e.getMessage());
        }
        httpEntity = new HttpEntity<>(map, headers);
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
        return responseEntity;
    }

    public static ResponseEntity<String> execute(String urlRemote, Long id, List<MultipartFile> files) {
    	 log.info("lblancas..execute(url: "+urlRemote+" Long id:"+id);
        return execute(urlRemote, id, files, "upload");
    }

}
