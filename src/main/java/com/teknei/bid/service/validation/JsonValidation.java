package com.teknei.bid.service.validation;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.teknei.bid.util.EnrollmentVerificationSingleton;

public class JsonValidation {
	private static final Logger log = LoggerFactory.getLogger(JsonValidation.class);
    public static boolean validateJson(String json) 
    {
    	//log.info("lblancas: JsonValidation.{validateJson() }");
        try {
            JSONObject jsonObject = new JSONObject(json);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
