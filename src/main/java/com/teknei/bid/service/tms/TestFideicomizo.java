package com.teknei.bid.service.tms;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BadPdfFormatException;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImage;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfIndirectObject;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;

@Service
public class TestFideicomizo {	

	// parte 1 y 2
	private final String PART1 = "/home/ContraFideicomisoP1.pdf";
	private final String PART1TEMP = "/home/ContraFideicomisoP1temp.pdf";
	private final String PART2 = "/home/ContraFideicomisoP2.pdf";
	private final String PART2TEMP = "/home/ContraFideicomisoP2temp.pdf";
	private final String MERGED = "/home/ContraFideicomisoFinal.pdf";

//	// plantilla jasper
//	private final String JASPERPDFTEMP = "/home/jasper/FideicomizoTemp.pdf";
//	private final String JASPERPATH = "/home/jasper/report1.jrxml";

	// tabla dinamica
	private final String PART17 = "/home/ContraFideicomiso17.pdf";
	private final String PART17TEMP = "/home/ContraFideicomiso17temp.pdf";

	/**
	 * @return MMM_dd,_yyyy
	 */
	private String getDate() {
		SimpleDateFormat objSDF = new SimpleDateFormat("MMM_dd,_yyyy",  new Locale("es", "ES"));
		String date = objSDF.format(new Date());
		return date;
	}

	public void setDataFideicomizo(String obj) throws DocumentException {
		try {
			JSONObject fideicomisoDTO = new JSONObject(obj);
			JSONObject patrimonial = (JSONObject) fideicomisoDTO.get("patrimonial");
			JSONObject datosGenerales = (JSONObject) patrimonial.get("DatosGenerales");
//			Pagina 1 
			String[] fe = getDate().split("_");
			String num = "001";
			String nombreTitulo = datosGenerales.getString("nombres") +" "+ datosGenerales.getString("apPat")+" "+
					 datosGenerales.getString("apMat");// "Alex Josue Garcia Dominguez";
			String fechaTitulo = fe[1] + "          " + fe[0] + "         " + fe[2];

//			Pagina 2
			// BLOQUE 1
			String nombres = datosGenerales.getString("nombres");// "Alex Josue";
			String aPat = datosGenerales.getString("apPat");// "Garcia";
			String aMat = datosGenerales.getString("apMat");// "Dominguez";
			String prof = datosGenerales.getString("ocupacion");// "Ingeniero";
			String fNac = datosGenerales.getString("fechaNaci");// "17/04/87";
			String nation = datosGenerales.getString("nacionalidad");// "Mexicana";
			String residencia = datosGenerales.optBoolean("residencia") ? "E" : "N";// "N";
			String curp = datosGenerales.getString("CURP");// "XGG2YU2KK91SHX2Y";
			String eCivil = datosGenerales.optBoolean("estadoCivil") ? "C" : "S";// S;
			String matReg = datosGenerales.optBoolean("regimen") ? "2" : "1";// "2";
			// BLOQUE 2
			JSONObject datosDomicilio = (JSONObject) patrimonial.get("DatosDomicilio");
			String calle = datosDomicilio.getString("calle"); // "Calle 1519, #123B";
			String colonia = datosDomicilio.getString("colonia"); // "San Juan de Aragon";
			String delegacion = datosDomicilio.getString("delegacion"); // "Gustavo A. Madero";
			String edo = datosDomicilio.getString("estado"); // "CDMX";
			String cp = datosDomicilio.getString("codigoP"); // "07918";
			String pais = datosDomicilio.getString("pais"); // "Mexico";
			String eMail = datosDomicilio.getString("email"); // "agarciad0705@gmail.com";
			String telCasa = "      52         55        " + datosGenerales.getString("numTel_Principal");
			String telOficina = "      52         55       " + datosGenerales.getString("numTel_Principal");
			String rendicionCuentas = "1";// DEFAULT
			// BLOQUE 4
			JSONObject intrdiccion = (JSONObject) patrimonial.get("Intrdiccion");
//			String b4 = "c";
			org.json.JSONArray b4al = (org.json.JSONArray) intrdiccion.getJSONArray("Interdiccion_A");
			org.json.JSONArray b4bl = (org.json.JSONArray) intrdiccion.getJSONArray("Interdicion_B");
			org.json.JSONArray b4cl = (org.json.JSONArray) intrdiccion.getJSONArray("Interdicion_C");
			Boolean b4a = (Boolean) b4al.get(0);
			Boolean b4b = (Boolean) b4bl.get(0);
			Boolean b4c = (Boolean) b4cl.get(0);
			// BLOQUE 5
			JSONObject casoFallesimiento = (JSONObject) patrimonial.get("CasoFallesimiento");
			String poderCobroo = casoFallesimiento.getString("Albacea"); // "Elenita";
			// BLOQUE 6 depositario datos
			String depNom = nombres; // "nombre1 nombre 2";
			String depProf = prof; // "Ingeniero";
			String depFeNac = fNac;// "17/04/87";
			String depNaci = nation;// "Mexicana";
			String depAPaterno = aPat;// "apellido paterno";
			String depAMaterno = aMat;// "apellido materno";
//			Pagina 3 
			String ciudad = "Ciudad de Mexico";
			String fechaP3 = fe[1] + "             " + fe[0] + "             " + fe[2];
			String fideicomitante = "TMS";// "Fideicomitante";
			String fideicomitantePor = "FideicomitantePor";
			String fideicomitanteCargo = "fideicomitanteCargo";
			String fiduciario = nombres;
			String fiduciarioPor = "fiduciarioPor";
			String fiduciarioCargo = "fiduciarioCargo";
			JSONObject datosGFamilia = (JSONObject) patrimonial.get("DatosGFamilia");
			org.json.JSONArray nameConyuges = (org.json.JSONArray) datosGFamilia.getJSONArray("nameConyuges");
			String conyuge = nameConyuges.length() > 0 ? nameConyuges.get(0).toString() : "NR";
			String conyugePor = "ConyugePor";
			String conyugeCargo = "ConyugeCargo";
			// huella
			String b64 = (String) fideicomisoDTO.optString("huellaIndex", "");			
			//// Tabla dinamica ->>>>

			// INICiO PARTE FINAL --->
			String tipoValor1 = "Chekes";
			String porcentaje = "100";

			String numDias = "15";
			String fideicomitente = nombres;// "Maria De La Luz Gutierres Gutierrez";
			String conyugeP2 = "Maria De La Luz Gutierres Gutierrez";
			String fiduciarioP2 = "Maria De La Luz Gutierres Gutierrez";
			String interFina = "Maria De La Luz Gutierres Gutierrez";

			int fontSizeTitle = 12;
			int fontSizeData = 7;
			String SELECT_MARCK = "X";

			String DEST = PART1TEMP;

			File file = new File(DEST);
			file.getParentFile().mkdirs();

			PdfReader reader = new PdfReader(PART1);
			PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(DEST));
			// ************** Pagina 1 **************
			PdfContentByte pag1 = stamper.getOverContent(1);
			ColumnText.showTextAligned(pag1, com.itextpdf.text.Element.ALIGN_CENTER,
					new Phrase(num, new Font(FontFamily.TIMES_ROMAN, fontSizeTitle)), 315, 660, 0); // num
			ColumnText.showTextAligned(pag1, com.itextpdf.text.Element.ALIGN_CENTER,
					new Phrase(nombreTitulo, new Font(FontFamily.TIMES_ROMAN, fontSizeTitle)), 305, 520, 0); // nombre
			ColumnText.showTextAligned(pag1, com.itextpdf.text.Element.ALIGN_CENTER,
					new Phrase(fechaTitulo, new Font(FontFamily.TIMES_ROMAN, fontSizeTitle)), 350, 207, 0); // fecha

			// ************** Pagina 2 **************
			PdfContentByte pag2 = stamper.getOverContent(2);
			// BLOQUE 1 GENERALES DEL Fideicomitente
			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(nombres, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 150, 680, 0); // nombres
			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(aPat, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 150, 670, 0); // apellido paterno
			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(aMat, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 400, 670, 0); // apellido materno
			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(prof, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 150, 650, 0); // profecion
			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(fNac, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 400, 650, 0); // fecha Nacimiento
			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(nation, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 150, 630, 0); // nacionalidad
			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)),
					(residencia.equals("E") ? 455 : 404), 630, 0); // recidencia
			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(curp, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 150, 600, 0); // Curp
			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)),
					(eCivil.equals("C") ? 453 : 407), 600, 0); // estado civil
			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)),
					(matReg.equals("2") ? 442 : 348), 580, 0); // Regimen Matrimonial
			// BLOQUE 2 DOMICILIO FÍSICO QUE SE ESPECÍFICA PARA OÍR Y RECIBIR
			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(calle, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 150, 507, 0); // calle
			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(colonia, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 150, 498, 0); // colonia
			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(delegacion, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 400, 498, 0); // delegacion
			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(edo, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 150, 488, 0); // estado
			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(cp, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 400, 488, 0); // cp
			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(pais, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 150, 476, 0); // pais
			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(eMail, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 400, 476, 0); // email
			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(telCasa, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 150, 458, 0); // Telefono de
																										// Casa
			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(telOficina, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 400, 458, 0); // Telefono
																											// oficina
			// BLOQUE 3 RENDICIÓN DE CUENTAS.
			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 202,
					(rendicionCuentas.equals("1") ? 405 : 385), 0); // En mi domicilio particular 1.

			// BLOQUE 4 NTERDICCIÓN.
//			int y = 0;
//			if (b4.equals("a")) { 
//				y =  332;
//			} else if (b4.equals("b")) { 
//				y = 322;
//			} else {
//				y = 312;
//			}
			if (b4a)
				ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
						new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 73, 332, 0); // INTERDICCIÓN.
																												// A
			if (b4b)
				ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
						new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 73, 322, 0); // INTERDICCIÓN.
																												// B
			if (b4c)
				ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
						new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 73, 312, 0); // INTERDICCIÓN.
																												// C
			// BLOQUE 5 PODERES PARA COBRO DE SEGURO
			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(poderCobroo, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 364, 269, 0); // Nombre
																											// Apoderado
			// BLOQUE 6 Depositario
			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(depNom, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 150, 207, 0); // nombre
																										// Depositario
			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(depAPaterno, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 150, 196, 0); // paterno
			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(depAMaterno, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 400, 196, 0); // materno
			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(depProf, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 150, 176, 0); // Profesión
			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(depFeNac, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 400, 176, 0); // Fnacimiento
			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(depNaci, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 150, 156, 0); // Nacionalidad
			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 405, 157, 0); // Residencia.
																											// a 405 b
																											// 455
			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(curp, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 150, 125, 0); // Curp
			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 407, 127, 0); // Estado
																											// civil
																											// soltero
																											// 407
																											// casado
																											// 453 X
			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 349, 107, 0); // regimen a
																											// 349 b 443
			// ************** Pagina 3 **************
			PdfContentByte pag3 = stamper.getOverContent(3);
			// Documentos Anexosv DEL FIDEYCOMITENTE
			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 273, 692, 0); // Identificación
																											// oficial
			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 273, 682, 0); // Comprobante
																											// de
																											// domicilio
																											// reciente
			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 273, 671, 0); // C.U.R.P.
//			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 273, 661, 0); //Acta de matrimonio
			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 273, 650, 0); // Acta de
																											// nacimiento
//			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 273, 639, 0); //Formato vigente de Conoce a tu Cliente
			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 273, 629, 0); // otros
			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 273, 618, 0); // otros
			// --------------------CONYUGAL
			if (!conyuge.equals("NR")) {
//			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 546, 692, 0); //Identificación oficial
//			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 546, 682, 0); //omprobante de domicilio reciente
//			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 546, 671, 0); //C.U.R.P
//			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 546, 661, 0); //Acta de matrimonio
//			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 546, 650, 0); ///Acta de nacimiento
//			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 546, 639, 0); ///
//			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 546, 629, 0); //Acta de nacimiento
//			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 546, 618, 0); //Identificación oficial
			}
			// datos
			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(ciudad, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 190, 598, 0); // fecha
			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(fechaP3, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 273, 598, 0); // fecha
//			Firmas
			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(fideicomitante, new Font(FontFamily.TIMES_ROMAN, 7)), 63, 550, 0); // Fideicomitante
			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(fiduciario, new Font(FontFamily.TIMES_ROMAN, 7)), 228, 550, 0); // Fiduciario
			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(conyuge, new Font(FontFamily.TIMES_ROMAN, 7)), 405, 550, 0); // conyuge

			huella(b64, 3, 73, 510, stamper);

			// ************** Pagina 16 **************
			PdfContentByte pag16 = stamper.getOverContent(16);
			ColumnText.showTextAligned(pag16, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(fideicomitante, new Font(FontFamily.TIMES_ROMAN, 7)), 106, 474, 0); // fideicomitante
			ColumnText.showTextAligned(pag16, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(fideicomitantePor, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 80, 462, 0); // por
			ColumnText.showTextAligned(pag16, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(fideicomitanteCargo, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 80, 454, 0); // cargo
			ColumnText.showTextAligned(pag16, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(fiduciario, new Font(FontFamily.TIMES_ROMAN, 7)), 366, 474, 0); // fiduciario
			ColumnText.showTextAligned(pag16, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(fiduciarioPor, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 346, 462, 0); // por
			ColumnText.showTextAligned(pag16, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(fiduciarioCargo, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 346, 454, 0); // cargo
			ColumnText.showTextAligned(pag16, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(conyuge, new Font(FontFamily.TIMES_ROMAN, 7)), 106, 399, 0); // conyuge
			ColumnText.showTextAligned(pag16, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(conyugePor, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 80, 387, 0); // por
			ColumnText.showTextAligned(pag16, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(conyugeCargo, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 80, 379, 0); // cargo

			// FIN PAGINA 16
			stamper.close();
			reader.close();
			System.out.println("COMPLETADO Parte 1");

			// - JASPER ---->>>>
			generatep3((JSONObject) fideicomisoDTO.get("beneficiario"));
//			JasperReport jasperReport;
//			JasperPrint jasperPrint;
//			Map<String, Object> parameters = new HashMap<String, Object>();
//			parameters.put("num", "555");
//			parameters.put("nom", "Alex Urrutia");
//			try {
//				jasperReport = JasperCompileManager.compileReport(JASPERPATH);
//				jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
//				JasperExportManager.exportReportToPdfFile(jasperPrint, JASPERPDFTEMP);
//			} catch (JRException e) {
//				e.printStackTrace();
//			}
			// - FIN DE CREACION PARTE <<---

			File fileP2 = new File(PART2TEMP);
			fileP2.getParentFile().mkdirs();
			PdfReader readerP2 = new PdfReader(PART2);
			PdfStamper stamperP2 = new PdfStamper(readerP2, new FileOutputStream(PART2TEMP));
			PdfContentByte p1f = stamperP2.getOverContent(1);
			// tabla tipo valor
			ColumnText.showTextAligned(p1f, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(tipoValor1, new Font(FontFamily.TIMES_ROMAN, 8)), 250, 462, 0); // Tipo Valor1
			ColumnText.showTextAligned(p1f, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(porcentaje, new Font(FontFamily.TIMES_ROMAN, 8)), 398, 462, 0); // Tipo Valor1
			ColumnText.showTextAligned(p1f, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(tipoValor1, new Font(FontFamily.TIMES_ROMAN, 8)), 250, 451, 0); // Tipo Valor2
			ColumnText.showTextAligned(p1f, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(porcentaje, new Font(FontFamily.TIMES_ROMAN, 8)), 398, 451, 0); // Tipo Valor2
			ColumnText.showTextAligned(p1f, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(tipoValor1, new Font(FontFamily.TIMES_ROMAN, 8)), 250, 440, 0); // Tipo Valor3
			ColumnText.showTextAligned(p1f, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(porcentaje, new Font(FontFamily.TIMES_ROMAN, 8)), 398, 440, 0); // Tipo Valor3
			ColumnText.showTextAligned(p1f, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(tipoValor1, new Font(FontFamily.TIMES_ROMAN, 8)), 250, 429, 0); // Tipo Valor4
			ColumnText.showTextAligned(p1f, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(porcentaje, new Font(FontFamily.TIMES_ROMAN, 8)), 398, 429, 0); // Tipo Valor4
			ColumnText.showTextAligned(p1f, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(tipoValor1, new Font(FontFamily.TIMES_ROMAN, 8)), 250, 418, 0); // Tipo Valor4
			ColumnText.showTextAligned(p1f, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(porcentaje, new Font(FontFamily.TIMES_ROMAN, 8)), 398, 418, 0); // Tipo Valor4
//			Plazo
			ColumnText.showTextAligned(p1f, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(numDias, new Font(FontFamily.TIMES_ROMAN, 8)), 323, 336, 0); // Tipo Valor4
//			Firmas
			ColumnText.showTextAligned(p1f, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(fideicomitente, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 60, 130, 0); // fideicomitente//
			ColumnText.showTextAligned(p1f, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(conyugeP2, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 180, 130, 0); // fideicomitente
			ColumnText.showTextAligned(p1f, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(fiduciarioP2, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 300, 130, 0); // fideicomitente
			ColumnText.showTextAligned(p1f, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(interFina, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 420, 130, 0); // fideicomitente
			// fin parte 2
			stamperP2.close();
			readerP2.close();
			System.out.println("COMPLETADO Parte final ");

			try {
				File mFile = new File(MERGED);
				mFile.getParentFile().mkdirs();
				List<InputStream> pdfs = new ArrayList<InputStream>();
				pdfs.add(new FileInputStream(DEST));
				pdfs.add(new FileInputStream(PART17TEMP));
				pdfs.add(new FileInputStream(PART2TEMP));
				OutputStream output = new FileOutputStream(mFile);
				concatPDFs(pdfs, output, true);
			} catch (Exception e) {
				e.printStackTrace();
			}

			System.out.println("COMPLETADO UNION");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Imprime huella en lugar indicado
	 * 
	 * @param b64
	 * @param pag
	 * @param x
	 * @param y
	 * @param stamper
	 * @throws MalformedURLException
	 * @throws IOException
	 * @throws DocumentException
	 */
	private void huella(String b64, int pag, int x, int y, PdfStamper stamper) {
		if (!b64.isEmpty()) {		
			
//			 desenrollando base 64
//			log.info("> recibe :"+b64.length());
//			String huellaFinal = decrypt.decrypt(b64);
//			log.info("> decript :"+huellaFinal.length());
//			byte[] imageByte = Base64Utils.decodeFromString(huellaFinal);
//			log.info("> imageByte :"+huellaFinal.length());
//			File rutaHuella = new File("/home/fingerToFirma.png");
//			if (rutaHuella.exists()) {
//				rutaHuella.delete();
//				rutaHuella = new File("/home/fingerToFirma.png");
//			}
//			InputStream in = new ByteArrayInputStream(imageByte);
//			BufferedImage bImageFromConvert = ImageIO.read(in);
//			ImageIO.write(bImageFromConvert, "png", rutaHuella);
			
			try {
				
				Image image = Image.getInstance("/home/fingerToFirma.png");
				PdfImage stream = new PdfImage(image, "", null);
				PdfIndirectObject ref = stamper.getWriter().addToBody(stream);
				image.setDirectReference(ref.getIndirectReference());
				image.scaleAbsolute(71F, 71f);
				image.setAbsolutePosition(x, y);
				PdfContentByte add_watermark4 = stamper.getOverContent(pag);
				add_watermark4.addImage(image);
				
			} catch (BadElementException e) {
				e.printStackTrace();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (BadPdfFormatException e) {
				e.printStackTrace();
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			
			
		}
	}

	/**
	 * Llenado de tabla dinamica
	 * 
	 * @throws IOException
	 * @throws DocumentException
	 */
	private void generatep3(JSONObject beneficiario) throws IOException, DocumentException {
		File fileP2 = new File(PART17TEMP);
		fileP2.getParentFile().mkdirs();
		PdfReader readerP2 = new PdfReader(PART17);
		PdfStamper stamperP2 = new PdfStamper(readerP2, new FileOutputStream(PART17TEMP));
		PdfContentByte p1 = stamperP2.getOverContent(1);

		// TABLA 1
		JSONObject primerBenefi = (JSONObject) beneficiario.get("PrimerBenefi");		
		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
				new Phrase(primerBenefi.getString("beneficiario"), new Font(FontFamily.TIMES_ROMAN, 6)), 45, 622, 0); // nombre
		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
				new Phrase("--", new Font(FontFamily.TIMES_ROMAN, 6)), 178, 622, 0); // edad
		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
				new Phrase(primerBenefi.getString("parentesco"), new Font(FontFamily.TIMES_ROMAN, 6)), 218, 622, 0); // parentesco
		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
				new Phrase(primerBenefi.get("porcentaje")+"", new Font(FontFamily.TIMES_ROMAN, 6)), 272, 623, 0); // Porcentaje
		// subs
		llenaTabla(p1, 622, primerBenefi.getJSONArray("HerederosSubs"));		
		int yplus = -103;
		
		
		// TABLA 2
		JSONObject segundoBenefi = (JSONObject) beneficiario.opt("SegundoBenefi");
		if(segundoBenefi!=null) {
		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
				new Phrase(segundoBenefi.getString("beneficiario"), new Font(FontFamily.TIMES_ROMAN, 6)), 45,
				622 + yplus, 0); // nombre
		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
				new Phrase("33 años", new Font(FontFamily.TIMES_ROMAN, 6)), 178, 622 + yplus, 0); // edad
		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
				new Phrase(segundoBenefi.getString("parentesco"), new Font(FontFamily.TIMES_ROMAN, 6)), 218,
				622 + yplus, 0); // parentesco
		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
				new Phrase(segundoBenefi.get("porcentaje")+"", new Font(FontFamily.TIMES_ROMAN, 6)), 272,
				623 + yplus, 0); // Porcentaje
		// subs
		llenaTabla(p1, 622 + yplus, primerBenefi.getJSONArray("HerederosSubs"));
		}
		
		
		// TABLA 3
		JSONObject tercerBenefi = (JSONObject) beneficiario.opt("TercerBenefi");
		if(tercerBenefi!=null) {
		yplus = yplus - 104;
		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
				new Phrase(tercerBenefi.getString("beneficiario"), new Font(FontFamily.TIMES_ROMAN, 6)), 45,
				622 + yplus, 0); // nombre
		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
				new Phrase("--", new Font(FontFamily.TIMES_ROMAN, 6)), 178, 622 + yplus, 0); // edad
		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
				new Phrase(tercerBenefi.getString("parentesco"), new Font(FontFamily.TIMES_ROMAN, 6)), 218, 622 + yplus,
				0); // parentesco
		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
				new Phrase(tercerBenefi.get("porcentaje")+"", new Font(FontFamily.TIMES_ROMAN, 6)), 272, 623 + yplus,
				0); // Porcentaje
		llenaTabla(p1, 622 + yplus, tercerBenefi.getJSONArray("HerederosSubs"));
		}
		
		
		// TABLA 4
		JSONObject cuartoBenefi = (JSONObject) beneficiario.opt("CuartoBenefi");
		if(cuartoBenefi!=null) {
		yplus = yplus - 104;
		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
				new Phrase(cuartoBenefi.getString("beneficiario"), new Font(FontFamily.TIMES_ROMAN, 6)), 45,
				622 + yplus, 0); // nombre
		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
				new Phrase("--", new Font(FontFamily.TIMES_ROMAN, 6)), 178, 622 + yplus, 0); // edad
		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
				new Phrase(cuartoBenefi.getString("parentesco"), new Font(FontFamily.TIMES_ROMAN, 6)), 218, 622 + yplus,
				0); // parentesco
		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
				new Phrase(cuartoBenefi.get("porcentaje")+"", new Font(FontFamily.TIMES_ROMAN, 6)), 272, 623 + yplus,
				0); // Porcentaje
		llenaTabla(p1, 622 + yplus, cuartoBenefi.getJSONArray("HerederosSubs"));
		}
//		opciones 

//		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//				new Phrase("X", new Font(FontFamily.TIMES_ROMAN, 6)), 46, 236, 0); // a
//		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//				new Phrase("X", new Font(FontFamily.TIMES_ROMAN, 6)), 46, 228, 0); // b
//
//		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//				new Phrase("X", new Font(FontFamily.TIMES_ROMAN, 6)), 74, 212, 0); // i
//		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//				new Phrase("X", new Font(FontFamily.TIMES_ROMAN, 6)), 74, 204, 0); // ii
//		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//				new Phrase("X", new Font(FontFamily.TIMES_ROMAN, 6)), 74, 196, 0); // iii
//
//		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//				new Phrase("X", new Font(FontFamily.TIMES_ROMAN, 6)), 46, 187, 0); // c
//
//		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//				new Phrase("X", new Font(FontFamily.TIMES_ROMAN, 6)), 74, 171, 0); // i
//		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//				new Phrase("X", new Font(FontFamily.TIMES_ROMAN, 6)), 74, 163, 0); // ii
//		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//				new Phrase("X", new Font(FontFamily.TIMES_ROMAN, 6)), 74, 156, 0); // iii

// firmas
		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
				new Phrase("fideicomitente", new Font(FontFamily.TIMES_ROMAN, 7)), 50, 50, 0); // fideicomitente//
		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
				new Phrase("conyugeP2", new Font(FontFamily.TIMES_ROMAN, 7)), 175, 50, 0); // fideicomitente
		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
				new Phrase("fiduciarioP2", new Font(FontFamily.TIMES_ROMAN, 7)), 320, 50, 0); // fideicomitente
		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
				new Phrase("interFina", new Font(FontFamily.TIMES_ROMAN, 7)), 480, 50, 0); // fideicomitente

		stamperP2.close();
		readerP2.close();
		System.out.println("COMPLETADO Parte final ");
	}

	/**
	 * Metodo que llena tabla "dinamicA"
	 * 
	 * @param p1
	 * @param yinit
	 */
	private void llenaTabla(PdfContentByte p1, int yinit, JSONArray subs) {
		if (subs != null && subs.length() > 0) {
			int yminus = 0;
			int count = 0;
			int max = 5;
			if (subs.length() < 5) {
				max = subs.length();
			}
			JSONArray sublist = subs.getJSONArray(0);
			if (sublist.length() < 5) {
				max = sublist.length();
			}
			while (count < max) {
				JSONObject sub = (JSONObject) sublist.get(count);
				
				ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
						new Phrase(sub.getString("nombre"), new Font(FontFamily.TIMES_ROMAN, 6)), 45 + 270,
						yinit - yminus, 0); // nombre
				ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
						new Phrase("--", new Font(FontFamily.TIMES_ROMAN, 6)), 178 + 270, yinit - yminus, 0); // edad
				ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
						new Phrase(sub.getString("parentesco"), new Font(FontFamily.TIMES_ROMAN, 6)), 218 + 270,
						yinit - yminus, 0); // parentesco
				ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
						new Phrase(sub.getString("porcentaje"), new Font(FontFamily.TIMES_ROMAN, 6)), 272 + 270,
						yinit - yminus, 0); // Porcentaje

				if (count >= 3) {
					yminus = yminus + 9;
				} else {
					yminus = yminus + 8;
				}
				count++;
			}
		}
	}

	/**
	 * Metodo para unir 2 o mas pdf
	 * 
	 * @param streamOfPDFFiles
	 * @param outputStream
	 * @param paginate
	 */
	private void concatPDFs(List<InputStream> streamOfPDFFiles, OutputStream outputStream, boolean paginate) {

		Document document = new Document();
		try {
			List<InputStream> pdfs = streamOfPDFFiles;
			List<PdfReader> readers = new ArrayList<PdfReader>();
//			int totalPages = 0;
			Iterator<InputStream> iteratorPDFs = pdfs.iterator();

			while (iteratorPDFs.hasNext()) {
				InputStream pdf = iteratorPDFs.next();
				PdfReader pdfReader = new PdfReader(pdf);
				readers.add(pdfReader);
//				totalPages += pdfReader.getNumberOfPages();
			}

			PdfWriter writer = PdfWriter.getInstance(document, outputStream);

			document.open();
			PdfContentByte cb = writer.getDirectContent();

			PdfImportedPage page;
//			int currentPageNumber = 0;
			int pageOfCurrentReaderPDF = 0;
			Iterator<PdfReader> iteratorPDFReader = readers.iterator();

			while (iteratorPDFReader.hasNext()) {
				PdfReader pdfReader = iteratorPDFReader.next();

				while (pageOfCurrentReaderPDF < pdfReader.getNumberOfPages()) {

					Rectangle rectangle = pdfReader.getPageSizeWithRotation(1);
					document.setPageSize(rectangle);
					document.newPage();

					pageOfCurrentReaderPDF++;
//					currentPageNumber++;
					page = writer.getImportedPage(pdfReader, pageOfCurrentReaderPDF);
					switch (rectangle.getRotation()) {
					case 0:
						cb.addTemplate(page, 1f, 0, 0, 1f, 0, 0);
						break;
					case 90:
						cb.addTemplate(page, 0, -1f, 1f, 0, 0, pdfReader.getPageSizeWithRotation(1).getHeight());
						break;
					case 180:
						cb.addTemplate(page, -1f, 0, 0, -1f, 0, 0);
						break;
					case 270:
						cb.addTemplate(page, 0, 1.0F, -1.0F, 0, pdfReader.getPageSizeWithRotation(1).getWidth(), 0);
						break;
					default:
						break;
					}
					if (paginate) {
						cb.beginText();
						cb.getPdfDocument().getPageSize();
						cb.endText();
					}
				}
				pageOfCurrentReaderPDF = 0;
			}
			outputStream.flush();
			document.close();
			outputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (document.isOpen())
				document.close();
			try {
				if (outputStream != null)
					outputStream.close();
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
		}
	}
}
