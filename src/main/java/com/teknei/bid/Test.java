package com.teknei.bid;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImage;
import com.itextpdf.text.pdf.PdfIndirectObject;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;

public class Test {
////https://www.freeformatter.com/base64-encoder.html
//
	public static void main(String[] a) {
		contratoSabadell();
//		getpdfMap();
	}
////
////	private static void getpdfMap() {		
////		String SRC = "D:\\downloads\\tmvDocs\\Contrato-Pospago-B2C-Formulario-V2.pdf";
////		String DEST = "C:\\Users\\BID\\Desktop\\Contrato-Pospago-B2C-Formulario-V2-MATRIX.pdf";
////		int numPag = 1;		
////		try {		
////			PdfReader reader = new PdfReader(SRC);
////			PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(DEST));			
////			for (int i = 1; i < 5; i++) {	numPag  = i; 	//TODO para varias paginas	
////				PdfContentByte pag = stamper.getOverContent(numPag);
////				for (int x = 0; x < 700; x += 14) {
////					for (int y = 0; y < 800; y += 5) {
////						ColumnText.showTextAligned(pag, com.itextpdf.text.Element.ALIGN_LEFT,
////								new Phrase("|" + x + ":" + y, new Font(FontFamily.TIMES_ROMAN, 4)), x, y, 0);
////					}
////				}				
////			}			
////			stamper.close();
////			reader.close();
////			System.out.println("COMPLETADO.");			
////		}catch (Exception e) {			
////			System.out.println("ERROR:"+e.getMessage());			
////		}
////	}
////
//	@SuppressWarnings("unused")
	
	private static final String FECHA = "Apr_8,_2021";
	private static void  contratoSabadell() {
		String PATH = "D:\\downloads\\Sabadell\\Contracts_08042021\\";
		String NUME_CLIE = "25687";
		String ID_CLIE = NUME_CLIE;
		String SRC = PATH+"\\"+ID_CLIE+".pdf";
		String DEST = PATH+"\\"+ID_CLIE+"-huella.pdf";
		String IMG = PATH+"\\"+ID_CLIE+".jpg";
		String GID = "5c2b8735ff87c939d72eb8549002935e67023bd3";
		try {
			PdfReader reader = new PdfReader(SRC);
			PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(DEST));
			Image image = Image.getInstance(IMG);
			PdfImage stream = new PdfImage(image, "", null);
			stream.put(new PdfName("ITXT_SpecialId"), new PdfName("123456789"));
			PdfIndirectObject ref = stamper.getWriter().addToBody(stream);
			image.setDirectReference(ref.getIndirectReference());
			image.scaleAbsolute(71F, 71f);

			int y1 = 65;// TODO aqui mover
			image.setAbsolutePosition(165, y1);// primera huella Pag1
			PdfContentByte over = stamper.getOverContent(1);
			over.addImage(image);
			ColumnText.showTextAligned(over, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(GID, new Font(FontFamily.TIMES_ROMAN, 4)), 285, y1 + 35, 0);
			ColumnText.showTextAligned(over, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(getDate(), new Font(FontFamily.TIMES_ROMAN, 4)), 312, y1 + 31, 0);

			image.scaleAbsolute(80f, 80f);

			int mat = 240;// TODO aqui mover
			int y2a = mat + 225;
			image.setAbsolutePosition(100, y2a);
			PdfContentByte add_watermark2 = stamper.getUnderContent(2);
			add_watermark2.addImage(image);
			ColumnText.showTextAligned(add_watermark2, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(GID, new Font(FontFamily.TIMES_ROMAN, 4)), 455, y2a + 65, 0);
			ColumnText.showTextAligned(add_watermark2, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(getDate(), new Font(FontFamily.TIMES_ROMAN, 4)), 455, y2a + 61, 0);

			int y2b = mat + 108;
			image.setAbsolutePosition(100, y2b);
			PdfContentByte add_watermark3 = stamper.getUnderContent(2);
			add_watermark3.addImage(image);
			ColumnText.showTextAligned(add_watermark2, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(GID, new Font(FontFamily.TIMES_ROMAN, 4)), 455, y2b + 63, 0);
			ColumnText.showTextAligned(add_watermark2, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(getDate(), new Font(FontFamily.TIMES_ROMAN, 4)), 455, y2b + 59, 0);

			int y2c = mat;
			image.setAbsolutePosition(100, y2c);
			PdfContentByte add_watermark4 = stamper.getUnderContent(2);
			add_watermark4.addImage(image);
			ColumnText.showTextAligned(add_watermark2, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(GID, new Font(FontFamily.TIMES_ROMAN, 4)), 455, y2c + 55, 0);
			ColumnText.showTextAligned(add_watermark2, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(getDate(), new Font(FontFamily.TIMES_ROMAN, 4)), 455, y2c + 51, 0);

			image.setAbsolutePosition(92, 630);
			PdfContentByte add_watermark5 = stamper.getUnderContent(11);
			add_watermark5.addImage(image);
			ColumnText.showTextAligned(add_watermark5, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(GID, new Font(FontFamily.TIMES_ROMAN, 4)), 178, 685, 0);
			ColumnText.showTextAligned(add_watermark5, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(getDate(), new Font(FontFamily.TIMES_ROMAN, 4)), 178, 681, 0);

			image.setAbsolutePosition(100, 315); // primera huella Pag1
			PdfContentByte add_watermark6 = stamper.getUnderContent(18);
			add_watermark6.addImage(image);
			ColumnText.showTextAligned(add_watermark6, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(GID, new Font(FontFamily.TIMES_ROMAN, 4)), 438, 350, 0);
			ColumnText.showTextAligned(add_watermark6, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(getDate(), new Font(FontFamily.TIMES_ROMAN, 4)), 465, 345, 0);

			image.setAbsolutePosition(358, 80);// primera huella Pag19
			PdfContentByte add_watermark7 = stamper.getUnderContent(19);
			add_watermark7.addImage(image);
			ColumnText.showTextAligned(add_watermark7, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(GID, new Font(FontFamily.TIMES_ROMAN, 4)), 208, 138, 0);
			ColumnText.showTextAligned(add_watermark7, com.itextpdf.text.Element.ALIGN_LEFT,
					new Phrase(getDate(), new Font(FontFamily.TIMES_ROMAN, 4)), 235, 134, 0);

			stamper.close();

			reader.close();
		} catch (Exception e) {
			System.out.println("ERROR"+e);
		}
		System.out.println("COMPLETADO");
	}


	private static String getDate() throws ParseException {
		SimpleDateFormat objSDF = new SimpleDateFormat("MMM dd, yyyy", Locale.US);
		//
		Date someDate = new Date();
		Date temp = new Date(someDate.getTime() + TimeUnit.DAYS.toMillis(-2));
		//
		String date = objSDF.format(temp);
//		System.out.println(date);
		 date = FECHA;
//		System.out.println(date);
		return date;
	}
}
////
////	@SuppressWarnings("unused")
////	private static void contratoTMS() {
////		ByteArrayOutputStream stream = null;
////		try {
////
////			Document document = new Document(PageSize.A4, 35, 30, 35, 50);
////			stream = new ByteArrayOutputStream();
////			PdfWriter.getInstance(document, stream);
////			// Abrir el documento
////			document.open();
////			Image image = null;
////			// Crear las fuentes para el contenido y los titulos
////			Font fontContenido = FontFactory.getFont(FontFactory.TIMES_ROMAN.toString(), 11, Font.NORMAL,
////					BaseColor.DARK_GRAY);
////
////			Font fontSubrayado = FontFactory.getFont(FontFactory.TIMES_ROMAN.toString(), 11, Font.UNDERLINE,
////					BaseColor.DARK_GRAY);
////
////			Font fontTitulos = FontFactory.getFont(FontFactory.HELVETICA, 9, Font.BOLD, BaseColor.BLACK);
////
////			Font fontContentDescrptivo = FontFactory.getFont(FontFactory.HELVETICA, 9, BaseColor.BLACK);
////
////			Font tituloSubrayado = FontFactory.getFont(FontFactory.TIMES_BOLDITALIC, 9, Font.UNDERLINE,
////					BaseColor.BLACK);
////
////			// Creacion del parrafo
////			Paragraph paragraph0 = new Paragraph();
////
////			// Agregar un titulo con su respectiva fuente
////			paragraph0.add(new Phrase("Formato de autorización definido para las SOFOM E.N.R. (Entidades No Reguladas)",
////					fontTitulos));
////			// log.info("Agregar saltos de linea");
////			// Agregar saltos de linea
////			paragraph0.add(new Phrase(Chunk.NEWLINE));
////
////			paragraph0.add(new Phrase("Autorización para solicitar Reportes de Crédito", fontTitulos));
////			// Agregar saltos de linea
////			paragraph0.add(new Phrase(Chunk.NEWLINE));
////
////			paragraph0.add(new Phrase("Personas Físicas / Personas Morales", fontTitulos));
////			paragraph0.add(new Phrase(Chunk.NEWLINE));
////			paragraph0.add(new Phrase(Chunk.NEWLINE));
////			paragraph0.add(new Phrase(Chunk.NEWLINE));
////			paragraph0.setAlignment(Element.ALIGN_CENTER);
////			paragraph0.setLeading(10);
////			document.add(paragraph0);
////
////			Paragraph paragraph1 = new Paragraph();
////
////			paragraph1.add(new Phrase("Por este conducto autorizo expresamente a ", fontContentDescrptivo));
////
////			paragraph1.add(new Phrase("CLICK SEGURIDAD JURIDICA SAPI DE CV SOFOM, E.N.R.", fontTitulos));
////
////			paragraph1.add(new Phrase(" , para que por conducto de"
////					+ " sus funcionarios facultados lleve a cabo Investigaciones, sobre mi comportamiento crediticio o el de la Empresa que represento en Trans"
////					+ " Union de México, S. A. SIC y/o Dun & Bradstreet, S.A. SIC", fontContentDescrptivo));
////
////			paragraph1.add(new Phrase(Chunk.NEWLINE));
////			paragraph1.add(new Phrase(Chunk.NEWLINE));
////
////			paragraph1.add(new Phrase(
////					"Asimismo, declaro que conozco la naturaleza y alcance de las sociedades de información crediticia y de la información contenida en los"
////							+ " reportes de crédito y reporte de crédito especial, declaro que conozco la naturaleza y alcance de la información que se solicitará, del uso"
////							+ " que ",
////					fontContentDescrptivo));
////
////			paragraph1.add(new Phrase("CLICK SEGURIDAD JURIDICA SAPI DE CV SOFOM, E.N.R.", fontTitulos));
////
////			paragraph1.add(new Phrase(", hará de tal información y de que ésta podrá realizar consultas"
////					+ " periódicas sobre mi historial o el de la empresa que represento, consintiendo que esta autorización se encuentre vigente por un período"
////					+ " de 3 años contados a partir de su expedición y en todo caso durante el tiempo que se mantenga la relación jurídica.",
////					fontContentDescrptivo));
////			paragraph1.add(new Phrase(Chunk.NEWLINE));
////			paragraph1.add(new Phrase(Chunk.NEWLINE));
////
////			paragraph1.add(new Phrase(
////					"En caso de que la solicitante sea una Persona Moral, declaro bajo protesta de decir verdad Ser Representante Legal de la empresa"
////							+ " mencionada en esta autorización; manifestando que a la fecha de firma de la presente autorización los poderes no me han sido"
////							+ " revocados, limitados, ni modificados en forma alguna.",
////					fontContentDescrptivo));
////			paragraph1.add(new Phrase(Chunk.NEWLINE));
////			paragraph1.add(new Phrase(Chunk.NEWLINE));
////
////			paragraph1.setLeading(10);
////			document.add(paragraph1);
////			String space = "                                                                                 ";
////			String space2 = "                                      ";
////			String space3 = "                ";
////			String space4 = "      ";
////			Paragraph paragraph2 = new Paragraph();
////			paragraph2.add(new Phrase("Autorización para:", fontContentDescrptivo));
////			paragraph2.add(new Phrase(Chunk.NEWLINE));
////			paragraph2
////					.add(new Phrase("Persona Física (PF)  ___" + " Persona Física con Actividad Empresarial (PFAE)  ___"
////							+ " Persona Moral  (PM)  ___", fontTitulos));
////			paragraph2.add(new Phrase(Chunk.NEWLINE));
////			paragraph2.add(new Phrase(Chunk.NEWLINE));
////
////			paragraph2.add(new Phrase("Nombre del solicitante (Persona Física o Razón Social de la Persona Moral):",
////					fontTitulos));
////			paragraph2.add(new Phrase(Chunk.NEWLINE));
////			paragraph2.add(new Phrase(space + space, fontSubrayado));
////			paragraph2.add(new Phrase(Chunk.NEWLINE));
////			paragraph2.add(new Phrase(Chunk.NEWLINE));
////
////			paragraph2.add(new Phrase("Para el caso de Persona Moral, nombre del Representante Legal:", fontTitulos));
////			paragraph2.add(new Phrase(Chunk.NEWLINE));
////			paragraph2.add(new Phrase(Chunk.NEWLINE));
////
////			paragraph2.add(new Phrase("RFC o CURP: ", fontTitulos));
////			paragraph2.add(new Phrase(space, fontSubrayado));
////			paragraph2.add(new Phrase(Chunk.NEWLINE));
////			paragraph2.add(new Phrase(Chunk.NEWLINE));
////
////			paragraph2.add(new Phrase("Domicilo: ", fontContentDescrptivo));
////			paragraph2.add(new Phrase(space, fontSubrayado));
////			paragraph2.add(new Phrase("    Colonia: ", fontContentDescrptivo));
////			paragraph2.add(new Phrase(space2 + "       ", fontSubrayado));
////			paragraph2.add(new Phrase(Chunk.NEWLINE));
////			paragraph2.add(new Phrase(Chunk.NEWLINE));
////
////			paragraph2.add(new Phrase("Municipio: ", fontContentDescrptivo));
////			paragraph2.add(new Phrase(space2, fontSubrayado));
////			paragraph2.add(new Phrase("    Estado: ", fontContentDescrptivo));
////			paragraph2.add(new Phrase(space2, fontSubrayado));
////			paragraph2.add(new Phrase("    Código postal: ", fontContentDescrptivo));
////			paragraph2.add(new Phrase(space3, fontSubrayado));
////			paragraph2.add(new Phrase(Chunk.NEWLINE));
////			paragraph2.add(new Phrase(Chunk.NEWLINE));
////
////			paragraph2.add(new Phrase("Teléfono:  ", fontContentDescrptivo));
////			paragraph2.add(new Phrase(space, fontSubrayado));
////			paragraph2.add(new Phrase(Chunk.NEWLINE));
////			paragraph2.add(new Phrase(Chunk.NEWLINE));
////			document.add(paragraph2);
////
////			Date d = new Date();
////			Calendar c = new GregorianCalendar();
////			c.setTime(d);
////
////			String dia, mes, anio;
////
////			dia = Integer.toString(c.get(Calendar.DATE));
////			mes = Integer.toString(c.get(Calendar.MONTH) + 1);
////			anio = Integer.toString(c.get(Calendar.YEAR));
////
////			Paragraph paragraph = new Paragraph();
////			mes = " ";
////			paragraph.add(new Phrase("Lugar y Fecha en que se firma la autorización: ", fontTitulos));
////			paragraph.add(new Phrase(Chunk.NEWLINE));
////			paragraph.add(new Phrase(Chunk.NEWLINE));
////
////			paragraph.add(new Phrase(
////					"Nombre del funcionario que recaba la autorización:  ______________________________", fontTitulos));
////			paragraph.add(new Phrase(Chunk.NEWLINE));
////			paragraph.add(new Phrase(Chunk.NEWLINE));
////			document.add(paragraph);
////
////			Paragraph paragraph4 = new Paragraph();
////			paragraph4.add(new Phrase(
////					"Estoy consciente y acepto que este documento quede bajo custodia de CLICK SEGURIDAD JURIDICA SAPI DE CV SOFOM, "
////							+ "E.N.R  y/o Sociedad de Información Crediticia consultada para efectos de control y cumplimiento del artículo 28 de la Ley para "
////							+ "Regular las Sociedades de Información Crediticia; mismo que señala que las Sociedades sólo podrán proporcionar "
////							+ "información a un Usuario, cuando éste cuente con la autorización expresa del Cliente mediante su firma autógrafa.",
////					fontTitulos));
////			paragraph4.setLeading(10);
////			paragraph4.add(new Phrase(Chunk.NEWLINE));
////			paragraph4.add(new Phrase(Chunk.NEWLINE));
////			paragraph4.add(new Phrase(Chunk.NEWLINE));
////			document.add(paragraph4);
////
////			// log.info(" Tabla PDF....");
////			PdfPTable table = new PdfPTable(5);
////			float dimension[] = { 80, 80, 40, 30, 80 };
////			table.setTotalWidth(dimension);
////			// Agregar la imagen anterior a una celda de la tabla
////			PdfPCell cel10 = new PdfPCell(new Phrase(" "));
////			cel10.setFixedHeight(50);
////			cel10.setBorder(Rectangle.NO_BORDER);
////			cel10.setColspan(1);
////			table.addCell(cel10);
////
////			Paragraph FIRMA = new Paragraph();
////			FIRMA.add(new Phrase(" ", tituloSubrayado));
////			FIRMA.add(new Phrase(Chunk.NEWLINE));
////			FIRMA.add(new Phrase(Chunk.NEWLINE));
////			FIRMA.add(new Phrase("Firma de PF, PFAE o Representante Legal de la empresa", fontTitulos));
////			FIRMA.setAlignment(Element.ALIGN_CENTER);
////			PdfPCell cel20 = new PdfPCell(FIRMA);
////			cel20.setColspan(2);
////			cel20.setBorder(Rectangle.NO_BORDER);
////			cel20.setFixedHeight(50);
////			table.addCell(cel20);
////			table.setWidthPercentage(120f);
////
////			PdfPCell cel30 = new PdfPCell(new Phrase(" "));
////			cel30.setBorder(Rectangle.NO_BORDER);
////			cel30.setColspan(1);
////			cel30.setFixedHeight(50);
////			table.addCell(cel30);
////
////			PdfPCell cel40 = new PdfPCell(new Phrase(" "));
////			cel40.setFixedHeight(50);
////			cel40.setBorder(Rectangle.NO_BORDER);
////			cel40.setColspan(1);
////			table.addCell(cel40);
////			// log.info(" Crea tabla ");
////
////			document.add(table);
////
////			PdfPTable tabla = new PdfPTable(1);
////			PdfPCell celda1 = new PdfPCell();
////			celda1.addElement(new Phrase(
////					"Para uso exclusivo de la Empresa que efectúa la consulta CLICK SEGURIDAD JURIDICA SAPI DE CV SOFOM, E.N.R ",
////					fontTitulos));
////			celda1.addElement(new Phrase("Fechas de Consulta BC:  _____", fontContentDescrptivo));
////			celda1.addElement(new Phrase("Folio de Consulta BC: _____", fontContentDescrptivo));
////			celda1.addElement(new Phrase(Chunk.NEWLINE));
////			celda1.setBackgroundColor(BaseColor.LIGHT_GRAY);
////			celda1.setFixedHeight(50f);
////			tabla.setWidthPercentage(99f);
////			tabla.addCell(celda1);
////			document.add(tabla);
////
////			Paragraph paragraph3 = new Paragraph();
////			paragraph3.add(new Phrase(Chunk.NEWLINE));
////			paragraph3.add(new Phrase("IMPORTANTE: ", tituloSubrayado));
////			paragraph3.add(new Phrase(
////					"Este formato debe ser llenado individualmente, para una sola persona física ó para una sola empresa. En caso de requerir el Historial crediticio del representante legal, favor de llenar un formato adicional.",
////					fontContentDescrptivo));
////			paragraph3.setLeading(10);
////			document.add(paragraph3);
////
////			// Cerrar el documento
////			document.close();
////			// Desktop.getDesktop().open();
////		} catch (Exception ex) {
////
////			ex.printStackTrace();
////		}
////
////		byte[] encoded = Base64.getEncoder().encode(stream.toByteArray());
////		System.out.println(new String(encoded));
////
////	}
////
////}
