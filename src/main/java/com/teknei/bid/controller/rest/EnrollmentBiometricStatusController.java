package com.teknei.bid.controller.rest;

import com.teknei.bid.service.remote.BiometricClient;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/rest/v3/enrollment-status-capture")
@CrossOrigin
public class EnrollmentBiometricStatusController {

    private static final Logger log = LoggerFactory.getLogger(EnrollmentBiometricStatusController.class);
    @Autowired
    private BiometricClient biometricClient;

    @ApiOperation(value = "Starts a process on biometric hardware", response = String.class)
    @RequestMapping(value = "/start", method = RequestMethod.POST)
    public String initCaptureFor(@RequestBody String serialNumber) {
    	//log.info("lblancas: "+this.getClass().getName()+".{initCaptureFor() }");
        try {
            return biometricClient.initCaptureFor(serialNumber);
        } catch (Exception e) {
            log.error("Error for initCapture: {} with message: {}", serialNumber, e.getMessage());
        }
        return "1";
    }

    @ApiOperation(value = "Gets the status for the requested serial number", response = String.class)
    @RequestMapping(value = "/status/{serialNumber}", method = RequestMethod.GET)
    public String getStatusFor(@PathVariable String serialNumber) {
    	//log.info("lblancas: "+this.getClass().getName()+".{getStatusFor() }");
        try {
            return biometricClient.getStatusFor(serialNumber);
        } catch (Exception e) {
            log.error("Error finding status for: {} with message: {}", serialNumber, e.getMessage());
        }
        return "0";
    }

}
