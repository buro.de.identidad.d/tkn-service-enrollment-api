package com.teknei.bid.controller.rest.unauth;

import com.teknei.bid.controller.rest.EnrollmentBiometricStatusController;
import com.teknei.bid.service.remote.BiometricClient;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@RestController
@RequestMapping(value = "/rest/unauth/enrollment-status-capture")
@CrossOrigin
public class EnrollmentBiometricStatusControllerUnauth {
	private static final Logger log = LoggerFactory.getLogger(EnrollmentBiometricStatusControllerUnauth.class);
    @Autowired
    private EnrollmentBiometricStatusController controller;

    @ApiOperation(value = "Starts a process on biometric hardware", response = String.class)
    @RequestMapping(value = "/start", method = RequestMethod.POST)
    public String initCaptureFor(@RequestBody String serialNumber) {
    	//log.info("lblancas: "+this.getClass().getName()+".{initCaptureFor(POST) }");
        return controller.initCaptureFor(serialNumber);
    }

    @ApiOperation(value = "Gets the status for the requested serial number", response = String.class)
    @RequestMapping(value = "/status/{serialNumber}", method = RequestMethod.GET)
    public String getStatusFor(@PathVariable String serialNumber) {
    	//log.info("lblancas: "+this.getClass().getName()+".{getStatusFor(GET) }");
        return controller.getStatusFor(serialNumber);
    }

}
