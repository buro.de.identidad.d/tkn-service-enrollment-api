package com.teknei.bid.controller.rest.unauth;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;

import com.teknei.bid.controller.rest.EnrollmentBiometricController;
import com.teknei.bid.controller.rest.util.crypto.Decrypt;
import com.teknei.bid.dto.BiometricCaptureRequestIdentDTO;
import com.teknei.bid.dto.BiometricCaptureRequestIdentToReceiveDTO;
import com.teknei.bid.dto.CompareFacialRequestDTO;
import com.teknei.bid.dto.OperationResult;
import com.teknei.bid.service.remote.BiometricClient;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "/rest/unauth/enrollment/biometric")
@CrossOrigin
public class EnrollmentBiometricControllerUnauth {
	private static final Logger log = LoggerFactory.getLogger(EnrollmentBiometricControllerUnauth.class);
	@Autowired
	private EnrollmentBiometricController controller;
	@Autowired
	private Decrypt decrypt;

	@Autowired
	private BiometricClient biometricClient;

	@ApiOperation(value = "Finds the status of the process for given serial number", response = BiometricCaptureRequestIdentDTO.class)
	@RequestMapping(value = "/asigna/{idclie}/{idclierel}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> asigna(@PathVariable String idclie, @PathVariable String idclierel) {
		log.info("lblancas: " + this.getClass().getName() + ".{cambia}");
		try {
			biometricClient.isCorrectforce(idclie, idclierel);
			return new ResponseEntity<String>("OK", HttpStatus.OK);
		} catch (Exception e) {
			log.error("Error for getStatusForAuth for : {} with message: {}", e.getMessage());
			return new ResponseEntity<>((String) null, HttpStatus.NOT_FOUND);
		}
	}

	@ApiOperation(value = "Expects a json with format: {'test' : 'value'} and returns the value in clear")
	@RequestMapping(value = "/testCypher", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public String testCypher(@RequestBody String jsonTest) {
		// log.info("lblancas: "+this.getClass().getName()+".{testCypher(POST) }");
		JSONObject request = new JSONObject(jsonTest);
		String source = request.getString("test");
		String clear = decrypt.decrypt(source);
		JSONObject returnJson = new JSONObject();
		returnJson.put("value", clear);
		return returnJson.toString();
	}

	@ApiOperation(value = "Adds the binary information from the capture of the slaps. Expects a JSON like {'operationId' : 1, 'scanId' : 'currentScan' , 'documentId' : 'currentDocumentManagerId' , 'rs' : 'Right slap' , 'ls : 'left slap' , 'ts' : 'thumbs slap'")
	@RequestMapping(value = "/minuciasSlapsCyphered", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
	public ResponseEntity<OperationResult> addMinuciasSlapsCyphered(@RequestBody String jsonRequest,
			HttpServletRequest request) {
		// log.info("lblancas:
		// "+this.getClass().getName()+".{addMinuciasSlapsCyphered(POST) }");
		return controller.addMinuciasSlapsCyphered(jsonRequest, request);
	}

	@Deprecated
	@ApiOperation(value = "Adds the binary information from the capture of the slaps. Expects a JSON like {'operationId' : 1, 'scanId' : 'currentScan' , 'documentId' : 'currentDocumentManagerId' , 'rs' : 'Right slap' , 'ls : 'left slap' , 'ts' : 'thumbs slap'")
	@RequestMapping(value = "/minuciasSlaps", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
	public ResponseEntity<OperationResult> addMinuciasSlaps(@RequestBody String jsonRequest,
			HttpServletRequest request) {
		// log.info("lblancas: "+this.getClass().getName()+".{addMinuciasSlaps(POST)
		// }");
		return controller.addMinuciasSlaps(jsonRequest, request);
	}

	@ApiOperation(value = "Adds the binary information from the capture of the fingers. Expects a JSON like {'operationId' : 1, 'scanId' : 'currentScan' , 'documentId' : 'currentDocumentManagerId' , 'll' : 'x' , 'lr' : 'x', 'lm' : 'x' , 'li': 'x', 'lt' : 'x', 'rl' : 'x', 'rr' : 'x', 'rm' : 'x', 'ri' : 'x'} as first 'l' stands for left and  'r' for right and second letter stands for little, ring, middle, index and thumb")
	@RequestMapping(value = "/minuciasCyphered", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
	public ResponseEntity<OperationResult> addMinuciasCyphered(@RequestPart(value = "json") String jsonRequest,
			HttpServletRequest request) {
		log.info("lblancas: " + this.getClass().getName() + ".{findAccomplishmnet() }");
		return controller.addMinuciasCyphered(jsonRequest, request);
	}

	@SuppressWarnings("deprecation")
	@ApiOperation(value = "Adds the binary information from the capture of the fingers. Expects a JSON like {'operationId' : 1, 'scanId' : 'currentScan' , 'documentId' : 'currentDocumentManagerId' , 'll' : 'x' , 'lr' : 'x', 'lm' : 'x' , 'li': 'x', 'lt' : 'x', 'rl' : 'x', 'rr' : 'x', 'rm' : 'x', 'ri' : 'x'} as first 'l' stands for left and  'r' for right and second letter stands for little, ring, middle, index and thumb")
	@RequestMapping(value = "/minucias", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
	public ResponseEntity<OperationResult> addMinucias(@RequestPart(value = "json") String jsonRequest,
			HttpServletRequest request) {
		// log.info("lblancas: "+this.getClass().getName()+".{findAccomplishmnet() }");
		return controller.addMinucias(jsonRequest, request);
	}

	@ApiOperation(value = "Finds the customer related to the given biometric data", notes = "The request must be a valid json like the following: {ll,li,lm,lr,lt,rl,ri,rm,rr,rt,contentType}", response = String.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "The customer is found"),
			@ApiResponse(code = 404, message = "The customer is not found"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@RequestMapping(value = "/search/customerCyphered", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ResponseEntity<String> searchByFingerCyphered(@RequestBody String jsonStringRequest) {
		// log.info("lblancas: "+this.getClass().getName()+".{findAccomplishmnet() }");
		return controller.searchByFingerCyphered(jsonStringRequest);
	}

	@Deprecated
	@ApiOperation(value = "Finds the customer related to the given biometric data", notes = "The request must be a valid json like the following: {ll,li,lm,lr,lt,rl,ri,rm,rr,rt,contentType}", response = String.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "The customer is found"),
			@ApiResponse(code = 404, message = "The customer is not found"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@RequestMapping(value = "/search/customer", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ResponseEntity<String> searchByFinger(@RequestBody String jsonStringRequest) {
		// log.info("lblancas: "+this.getClass().getName()+".{findAccomplishmnet() }");
		return controller.searchByFinger(jsonStringRequest);
	}

	@ApiOperation(value = "Finds the customer related to the given cyphered biometric data", notes = "The request must be a valid json like the following: {ll,li,lm,lr,lt,rl,ri,rm,rr,rt,contentType,id}", response = String.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "The customer is found"),
			@ApiResponse(code = 404, message = "The customer is not found"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@RequestMapping(value = "/search/customerIdCyphered", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ResponseEntity<String> searchByFingerAndIdCyphered(@RequestBody String jsonStringRequest) {
		// log.info("lblancas: "+this.getClass().getName()+".{findAccomplishmnet() }");
		return controller.searchByFingerAndIdCyphered(jsonStringRequest);
	}

	@Deprecated
	@ApiOperation(value = "Finds the customer related to the given biometric data", notes = "The request must be a valid json like the following: {ll,li,lm,lr,lt,rl,ri,rm,rr,rt,contentType,id}", response = String.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "The customer is found"),
			@ApiResponse(code = 404, message = "The customer is not found"),
			@ApiResponse(code = 500, message = "Internal server error") })

	@RequestMapping(value = "/search/customerId", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ResponseEntity<String> searchByFingerAndId(@RequestBody String jsonStringRequest) {
		// log.info("lblancas: "+this.getClass().getName()+".{findAccomplishmnet() }");
		return controller.searchByFingerAndId(jsonStringRequest);
	}

	@ApiOperation(value = "Finds the customer related to the given cyphered biometric data", notes = "The request must be a valid json like the following: {id,sl,sr,st}", response = String.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "The customer is found"),
			@ApiResponse(code = 404, message = "The customer is not found"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@RequestMapping(value = "/search/customerSlapsCyphered", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ResponseEntity<String> searchBySlapsCyphered(@RequestBody String jsonStringRequest) {
		// log.info("lblancas: "+this.getClass().getName()+".{findAccomplishmnet() }");
		return controller.searchBySlapsCyphered(jsonStringRequest);
	}

	@Deprecated
	@ApiOperation(value = "Finds the customer related to the given biometric data", notes = "The request must be a valid json like the following: {id,sl,sr,st}", response = String.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "The customer is found"),
			@ApiResponse(code = 404, message = "The customer is not found"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@RequestMapping(value = "/search/customerSlaps", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ResponseEntity<String> searchBySlaps(@RequestBody String jsonStringRequest) {
		// log.info("lblancas: "+this.getClass().getName()+".{findAccomplishmnet() }");
		return controller.searchBySlaps(jsonStringRequest);
	}

	@ApiOperation(value = "Finds the customer related to the given cyphered biometric data", notes = "The request must be a valid json like the following: {id,sl,sr,st,id}", response = String.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "The customer is found"),
			@ApiResponse(code = 404, message = "The customer is not found"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@RequestMapping(value = "/search/customerSlapsIdCyphered", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ResponseEntity<String> searchBySlapsIdCyphered(@RequestBody String jsonStringRequest) {
		// log.info("lblancas: "+this.getClass().getName()+".{findAccomplishmnet() }");
		return controller.searchBySlapsIdCyphered(jsonStringRequest);
	}

	@Deprecated
	@ApiOperation(value = "Finds the customer related to the given biometric data", notes = "The request must be a valid json like the following: {id,sl,sr,st,id}", response = String.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "The customer is found"),
			@ApiResponse(code = 404, message = "The customer is not found"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@RequestMapping(value = "/search/customerSlapsId", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ResponseEntity<String> searchBySlapsId(@RequestBody String jsonStringRequest) {
		// log.info("lblancas: "+this.getClass().getName()+".{findAccomplishmnet() }");
		return controller.searchBySlapsId(jsonStringRequest);
	}

	@ApiOperation(value = "Finds the customer related to the given biometric data", notes = "The request must be a valid json like the following: {id,facial}", response = String.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "The customer is found"),
			@ApiResponse(code = 404, message = "The customer is not found"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@RequestMapping(value = "/search/customerFace", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ResponseEntity<String> searchByFace(@RequestBody String jsonStringRequest) {
		// log.info("lblancas: "+this.getClass().getName()+".{findAccomplishmnet() }");
		return controller.searchByFace(jsonStringRequest);
	}

	@ApiOperation(value = "Finds the customer related to the given biometric data", notes = "The request must be a valid json like the following: {id,facial}", response = String.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "The customer is found"),
			@ApiResponse(code = 404, message = "The customer is not found"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@RequestMapping(value = "/search/customerFaceCyphered", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ResponseEntity<String> searchByFaceCyphered(@RequestBody String jsonStringRequest) {
		// log.info("lblancas: "+this.getClass().getName()+".{findAccomplishmnet() }");
		return controller.searchByFaceCyphered(jsonStringRequest);
	}

	@ApiOperation(value = "Finds the customer related to the given biometric data", notes = "The request must be a valid json like the following: {id,facial}", response = String.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "The customer is found"),
			@ApiResponse(code = 404, message = "The customer is not found"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@RequestMapping(value = "/search/customerFaceId", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ResponseEntity<String> searchByFaceId(@RequestBody String jsonStringRequest) {
		// log.info("lblancas: "+this.getClass().getName()+".{findAccomplishmnet() }");
		return controller.searchByFaceId(jsonStringRequest);
	}

	@ApiOperation(value = "Finds the customer related to the given biometric data", notes = "The request must be a valid json like the following: {id,facial}", response = String.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "The customer is found"),
			@ApiResponse(code = 404, message = "The customer is not found"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@RequestMapping(value = "/search/customerFaceIdCyphered", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ResponseEntity<String> searchByFaceIdCyphered(@RequestBody String jsonStringRequest) {
		// log.info("lblancas: "+this.getClass().getName()+".{findAccomplishmnet() }");
		return controller.searchByFaceIdCyphered(jsonStringRequest);
	}

	@ApiOperation(value = "Starts process on client for verification on biometric system", response = String.class, notes = "Response 1 is error, response 0 is OK")
	@RequestMapping(value = "/startAuth", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String initCaptureForIdentification(@RequestBody BiometricCaptureRequestIdentDTO requestIdentDTO) {
		// log.info("lblancas: "+this.getClass().getName()+".{findAccomplishmnet() }");
		return controller.initCaptureForIdentification(requestIdentDTO);
	}

	@ApiOperation(value = "Starts process on client for sign contract on biometric system", response = String.class, notes = "Response 1 is error, response 0 is OK")
	@RequestMapping(value = "/startSign", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String initCaptureSignForIdentification(
			@RequestBody BiometricCaptureRequestIdentToReceiveDTO requestIdentDTO) {
		// log.info("lblancas: "+this.getClass().getName()+".{findAccomplishmnet() }");
		return controller.initCaptureSignForIdentification(requestIdentDTO);
	}

	@ApiOperation(value = "Finds the status of the process for given serial number", response = BiometricCaptureRequestIdentDTO.class)
	@RequestMapping(value = "/statusAuth/{serial}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BiometricCaptureRequestIdentDTO> getStatusForAuth(@PathVariable String serial) {
		// log.info("lblancas: "+this.getClass().getName()+".{findAccomplishmnet() }");
		return controller.getStatusForAuth(serial);
	}

	@ApiOperation(value = "Finds the status of the process for contract sign based on given serial number", response = BiometricCaptureRequestIdentDTO.class)
	@RequestMapping(value = "/statusSign/{serial}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BiometricCaptureRequestIdentDTO> getStatusForAuthSign(@PathVariable String serial) {
		// log.info("lblancas: "+this.getClass().getName()+".{findAccomplishmnet() }");
		return controller.getStatusForAuthSign(serial);
	}

	@ApiOperation(value = "Confirms the current capture result for the associated serial number and customer id")
	@RequestMapping(value = "/confirmAuth/{status}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String confirmCaptureAuth(@RequestBody BiometricCaptureRequestIdentToReceiveDTO requestIdentDTO,
			@PathVariable Integer status) {
		// log.info("lblancas: "+this.getClass().getName()+".{findAccomplishmnet() }");
		return controller.confirmCaptureAuth(requestIdentDTO, status);
	}

	@ApiOperation(value = "Confirms the current capture result for the associated serial number and customer id for contract sign")
	@RequestMapping(value = "/confirmSign/{status}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String confirmCaptureSign(@RequestBody BiometricCaptureRequestIdentToReceiveDTO requestIdentDTO,
			@PathVariable Integer status) {
		// log.info("lblancas: "+this.getClass().getName()+".{findAccomplishmnet() }");
		return controller.confirmCaptureSign(requestIdentDTO, status);
	}

	@ApiOperation(value = "Queries the results for the given data")
	@RequestMapping(value = "/queryAuth", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> queryCaptureAuth(
			@RequestBody BiometricCaptureRequestIdentToReceiveDTO requestIdentDTO) {
		// log.info("lblancas: "+this.getClass().getName()+".{findAccomplishmnet() }");
		return controller.queryCaptureAuth(requestIdentDTO);
	}

	@ApiOperation(value = "Queries the results for the given data")
	@RequestMapping(value = "/querySign", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> queryCaptureSigm(
			@RequestBody BiometricCaptureRequestIdentToReceiveDTO requestIdentDTO) {
		// log.info("lblancas: "+this.getClass().getName()+".{findAccomplishmnet() }");
		return controller.queryCaptureSigm(requestIdentDTO);
	}

	@ApiOperation(value = "Compares two faces according to biometric templates if it passes the requested umbral(score)")
	@RequestMapping(value = "/compareFacial", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> compareFacial(@RequestBody CompareFacialRequestDTO compareFacialRequestDTO,
			HttpServletRequest request) {
		// log.info("lblancas: "+this.getClass().getName()+".{findAccomplishmnet() }");
		return controller.compareFacial(compareFacialRequestDTO, request);
	}

	@ApiOperation(value = "Compares two faces according to biometric templates if it passes the requested umbral(score) with cyphered data")
	@RequestMapping(value = "/compareFacialCyphered", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> compareFacialCyphered(@RequestBody CompareFacialRequestDTO compareFacialRequestDTO,
			HttpServletRequest request) {
		// log.info("lblancas: "+this.getClass().getName()+".{findAccomplishmnet() }");
		return controller.compareFacialCyphered(compareFacialRequestDTO, request);
	}

	@ApiOperation(value = "Adds the binary information from the capture of the fingers. Expects a JSON like {'operationId' : 1, 'scanId' : 'currentScan' , 'documentId' : 'currentDocumentManagerId' , 'll' : 'x' , 'lr' : 'x', 'lm' : 'x' , 'li': 'x', 'lt' : 'x', 'rl' : 'x', 'rr' : 'x', 'rm' : 'x', 'ri' : 'x'} as first 'l' stands for left and  'r' for right and second letter stands for little, ring, middle, index and thumb")
	@RequestMapping(value = "/validateRepeatMinucias", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
	public ResponseEntity<OperationResult> validateRepeatMinucias(@RequestPart(value = "json") String jsonRequest,
			HttpServletRequest request) {
		log.info("lblancas: " + this.getClass().getName() + ".{findAccomplishmnet() }");		
		return new ResponseEntity<>(controller.validateFingers(jsonRequest), HttpStatus.OK);	
	}
}
