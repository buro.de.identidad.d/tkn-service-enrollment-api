package com.teknei.bid.controller.rest.unauth;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.teknei.bid.controller.rest.EnrollmentCredentialsTempController;
import com.teknei.bid.dto.*;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/rest/unauth/enrollment/credentials-temp")
@CrossOrigin
public class EnrollmentCredentialsTempControllerUnauth {
	private static final Logger log = LoggerFactory.getLogger(EnrollmentCredentialsTempControllerUnauth.class);
    @Autowired
    private EnrollmentCredentialsTempController controller;

    @ApiOperation(value = "Find the available status for credentials", response = Map.class)
    @RequestMapping(value = "/credentials/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, Long>> findStatusCred() {
    	//log.info("lblancas: "+this.getClass().getName()+".{findStatusCred(GET) }");
        return controller.findStatusCred();
    }

    @ApiOperation(value = "Finds the credentials with given status value", response = CredentialsTempDTO.class)
    @RequestMapping(value = "/credentials/{idEstaCred}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CredentialsTempDTO>> findAllByIdStatus(@PathVariable Long idEstaCred) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findAllByIdStatus(GET) }");
        return controller.findAllByIdStatus(idEstaCred);
    }

    @ApiOperation(value = "Finds all temp credentials", response = CredentialsTempDTO.class)
    @RequestMapping(value = "/credentials", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CredentialsTempDTO>> findAll() {
    	//log.info("lblancas: "+this.getClass().getName()+".{findAll(GET) }");
        return controller.findAll();
    }

    @ApiResponses({
            @ApiResponse(code = 200, message = "Success operation"),
            @ApiResponse(code = 422, message = "Fail operation")
    })
    @ApiOperation(value = "Deletes the temp credential for the requested customer", response = CredentialsValidationResultDTO.class, notes = "The result json object contains the detail error status if any")
    @RequestMapping(value = "/credentials/credentials", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CredentialsValidationResultDTO> delete(@RequestBody CredentialsDeleteRequestDTO requestDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findAccomplishmnet() }");
        return controller.delete(requestDTO, request);
    }

    @ApiResponses({
            @ApiResponse(code = 200, message = "Success operation"),
            @ApiResponse(code = 422, message = "Fail operation")
    })
    @ApiOperation(value = "Creates the temp credential for the requested customer", response = CredentialsValidationResultDTO.class, notes = "The result json object contains the detail error status if any")
    @RequestMapping(value = "/credentials/credentials", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CredentialsValidationResultDTO> create(@RequestBody CredentialsSaveRequestDTO requestDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{create(POST) }");
        return controller.create(requestDTO, request);
    }

    @ApiResponses({
            @ApiResponse(code = 200, message = "Success operation"),
            @ApiResponse(code = 403, message = "Fail operation, not valid credentials")
    })
    @ApiOperation(value = "Validates the temp credential for the requested customer", response = CredentialsValidationResultDTO.class, notes = "The result json object contains the detail error status if any")
    @RequestMapping(value = "/credentials/credentials", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CredentialsValidationPersonalResultDTO> validate(@RequestBody CredentialsValidationRequestDTO requestDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{validate(PUT) }");
        return controller.validate(requestDTO, request);
    }

    @ApiResponses({
            @ApiResponse(code = 200, message = "Success operation"),
            @ApiResponse(code = 422, message = "Fail operation")
    })
    @ApiOperation(value = "Deletes the temp credential for the requested customer", response = CredentialsValidationResultDTO.class, notes = "The result json object contains the detail error status if any")
    @RequestMapping(value = "/credentials/credentials/fingers", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CredentialsValidationResultDTO> validateNoFingers(@RequestBody CredentialsValidationFingersRequestDTO requestDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{validateNoFingers(POST) }");
        return controller.validateNoFingers(requestDTO, request);
    }

    @ApiOperation(value = "Updates the status of the credential requested", response = CredentialsValidationResultDTO.class)
    @RequestMapping(value = "/credentials", method = RequestMethod.PATCH, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CredentialsValidationResultDTO> updateCredTemp(@RequestBody UpdateTempStatusCredRequestDTO requestDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{updateCredTemp(PATCH) }");
        return controller.updateCredTemp(requestDTO, request);

    }

}
