package com.teknei.bid.controller.rest.unauth;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.teknei.bid.controller.rest.EnrollmentStatusController;
import com.teknei.bid.dto.OperationResult;
import com.teknei.bid.dto.StartOperationResult;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/rest/unauth/enrollment/status")
@CrossOrigin
public class EnrollmentStatusControllerUnauth {
	private static final Logger log = LoggerFactory.getLogger(EnrollmentStatusControllerUnauth.class);
    @Autowired
    private EnrollmentStatusController controller;


    @ApiOperation(value = "Finds the status map for accomplishment steps related to the enrollment process")
    @RequestMapping(value = "/accomplishment/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> findAccomplishmnet(@PathVariable Long id) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findAccomplishmnet(GET) }");
        return controller.findAccomplishmnet(id);
    }

    @ApiOperation(value = "Starts main process. Expects JSON string as main input like {'deviceId' : 'X' ,'employee' : 'X' , 'curp' : 'X', 'nombre' : 'X' , 'primerApellido' : 'X' , 'segundoApellido': ' X' , 'telefono'|'celular' : 'X' , 'email' : 'X' , 'emprId' : 'X', 'customerType': 1|2, 'confType' : 1|2 }", response = StartOperationResult.class)
    @RequestMapping(value = "/start", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.POST)
    public ResponseEntity<StartOperationResult> startOperation(
            @RequestBody String startOperationRequest, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{startOperation(POST) }");
        return controller.startOperation(startOperationRequest, request);
    } 
    @ApiOperation(value = "StartOperation main process. Expects JSON string as main input like {'deviceId' : 'X' ,'employee' : 'X' , 'curp' : 'X', 'nombre' : 'X' , 'primerApellido' : 'X' , 'segundoApellido': ' X' , 'telefono'|'celular' : 'X' , 'email' : 'X' , 'emprId' : 'X', 'customerType': 1|2, 'confType' : 1|2 }", response = StartOperationResult.class)
    @RequestMapping(value = "/startOperation", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, method = RequestMethod.POST)
    public ResponseEntity<StartOperationResult> startOperationByPerfil(
            @RequestBody String startOperationRequest, HttpServletRequest request) {
    	log.info("lblancas: "+this.getClass().getName()+".{startOperation(POST) }");
        return controller.startOperationByPerfil(startOperationRequest, request);
    } 
  
    
    @ApiOperation(value = "Gets the data {\"idCliente\":50206,\"idPerfil\":1} ", response = Boolean.class)
    @RequestMapping(value = "/updatePrfByClient", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    public ResponseEntity<Boolean> updatePrfByClient(@RequestBody String searchDTO) 
    {
    	log.info("lblancas: "+this.getClass().getName()+".{updatePerfilByClient( "+searchDTO+") }");  
    	return controller.updatePrfByClient(searchDTO); 
    
    }
    
    
    @ApiOperation(value = "Terminates current opetation, expects a JSON string as input with {'operationId' : 'x'}", response = OperationResult.class)
    @RequestMapping(value = "/end", method = RequestMethod.POST, consumes = "application/json;charset=utf-8", produces = "application/json;charset=utf-8")
    public ResponseEntity<OperationResult> endOperation(
            @RequestBody String endOperationRequest) throws Exception {
    	//log.info("lblancas: "+this.getClass().getName()+".{endOperation(POST) }");
        return controller.endOperation(endOperationRequest);
    }

    @ApiOperation(value = "Cancels the active operation, expects the operation Id as input", response = OperationResult.class)
    @RequestMapping(value = "/cancel", method = RequestMethod.DELETE, produces = "application/json;charset=utf-8")
    public ResponseEntity<OperationResult> cancelOperation(
            @RequestParam long operationId) {
    	//log.info("lblancas: "+this.getClass().getName()+".{cancelOperation(DELETE) }");
        return controller.cancelOperation(operationId);
    }
}
