package com.teknei.bid.controller.rest.unauth;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.teknei.bid.controller.rest.EnrollmentPicturesController;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/rest/unauth/enrollment/pictures")
@CrossOrigin
public class EnrollmentPicturesControllerUnauth {
	private static final Logger log = LoggerFactory.getLogger(EnrollmentPicturesControllerUnauth.class);
    @Autowired
    private EnrollmentPicturesController controller;

    @ApiOperation(value = "Gets the image corresponding for the requestd params", notes = "The response will be a JSON with format: {'image' : 'content in b64'}", response = String.class)
    @RequestMapping(value = "/search/customer/imageB64/{option}/{curp}/{id}", method = RequestMethod.GET, produces = MediaType.
            APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> findPictureFromReferenceB64(@PathVariable Integer option, @PathVariable String curp, @PathVariable String id) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findPictureFromReferenceB64(GET) }");
        return controller.findPictureFromReferenceB64(option, curp, id);
    }

    @ApiOperation(value = "Gets the image corresponding for the requestd params")
    @RequestMapping(value = "/search/customer/image/{option}/{curp}/{id}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<byte[]> findPictureFromReference(@PathVariable Integer option, @PathVariable String curp, @PathVariable String id) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findPictureFromReference(GET) }");
        return controller.findPictureFromReference(option, curp, id);
    }

}
