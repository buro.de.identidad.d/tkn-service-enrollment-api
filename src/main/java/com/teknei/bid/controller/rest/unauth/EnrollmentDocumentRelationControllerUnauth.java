package com.teknei.bid.controller.rest.unauth;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.teknei.bid.controller.rest.EnrollmentDocumentRelationController;
import com.teknei.bid.dto.BidDocuRelDTO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/unauth/enrollment/documentRel")
@CrossOrigin
public class EnrollmentDocumentRelationControllerUnauth {
	private static final Logger log = LoggerFactory.getLogger(EnrollmentDocumentRelationControllerUnauth.class);
    @Autowired
    private EnrollmentDocumentRelationController controller;

    @ApiOperation(value = "Finds the active document relations", response = BidDocuRelDTO.class, notes = "By now, the source document has no data for front or back requirements, false is retrieved by default. Children only have this relation")
    @RequestMapping(value = "/findRelActive", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<BidDocuRelDTO>> findActiveDocumentRelations() {
    	//log.info("lblancas: "+this.getClass().getName()+".{findActiveDocumentRelations(GET) }");
        return controller.findActiveDocumentRelations();
    }

}
