package com.teknei.bid.controller.rest;

import com.google.gson.Gson;
import com.teknei.bid.controller.rest.util.crypto.Decrypt;
import com.teknei.bid.controller.rest.util.crypto.TokenUtils;
import com.teknei.bid.dto.*;
import com.teknei.bid.service.remote.CredentialsTempclient;
import feign.FeignException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/rest/v3/enrollment/credentials-temp")
@CrossOrigin
public class EnrollmentCredentialsTempController {

    @Autowired
    private Decrypt decrypt;
    @Autowired
    private TokenUtils tokenUtils;
    private static final Logger log = LoggerFactory.getLogger(EnrollmentCredentialsTempController.class);
    @Autowired
    private CredentialsTempclient credentialsTempclient;


    @ApiOperation(value = "Find the available status for credentials", response = Map.class)
    @RequestMapping(value = "/credentials/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, Long>> findStatusCred() {
    	//log.info("lblancas: "+this.getClass().getName()+".{findStatusCred() }");
        try {
            return credentialsTempclient.findStatusCred();
        } catch (Exception e) {
            log.error("Error finding available status for credentials with message: {}", e.getMessage());
        }
        return new ResponseEntity<>((Map<String, Long>) null, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ApiOperation(value = "Finds the credentials with given status value", response = CredentialsTempDTO.class)
    @RequestMapping(value = "/credentials/{idEstaCred}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CredentialsTempDTO>> findAllByIdStatus(@PathVariable Long idEstaCred) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findAllByIdStatus() }");
        try {
            return credentialsTempclient.findAllByIdStatus(idEstaCred);
        } catch (FeignException e) {
            if (e.status() == 404) {
                log.info("No credentials found for status: {}", idEstaCred);
                return new ResponseEntity<>((List<CredentialsTempDTO>) null, HttpStatus.NOT_FOUND);
            }
            log.error("Error finding credentials with status: {} with message: {}", idEstaCred, e.getMessage());
        } catch (Exception e) {
            log.error("Error finding credentials with status: {} with message: {}", idEstaCred, e.getMessage());
        }
        return new ResponseEntity<>((List<CredentialsTempDTO>) null, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ApiOperation(value = "Finds all temp credentials", response = CredentialsTempDTO.class)
    @RequestMapping(value = "/credentials", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CredentialsTempDTO>> findAll() {
    	//log.info("lblancas: "+this.getClass().getName()+".{findAll() }");
        try {
            return credentialsTempclient.findAll();
        } catch (FeignException e) {
            if (e.status() == 404) {
                log.info("No credentials found");
                return new ResponseEntity<>((List<CredentialsTempDTO>) null, HttpStatus.NOT_FOUND);
            }
            log.error("Error finding credentials with message: {}", e.getMessage());
        } catch (Exception e) {
            log.error("Error finding credentials with message: {}", e.getMessage());
        }
        return new ResponseEntity<>((List<CredentialsTempDTO>) null, HttpStatus.UNPROCESSABLE_ENTITY);
    }


    @ApiResponses({
            @ApiResponse(code = 200, message = "Success operation"),
            @ApiResponse(code = 422, message = "Fail operation")
    })
    @ApiOperation(value = "Deletes the temp credential for the requested customer", response = CredentialsValidationResultDTO.class, notes = "The result json object contains the detail error status if any")
    @RequestMapping(value = "/credentials/credentials", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CredentialsValidationResultDTO> delete(@RequestBody CredentialsDeleteRequestDTO requestDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{delete() }");
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        requestDTO.setUsernameRequesting(username);
        CredentialsValidationResultDTO resultDTO = new CredentialsValidationResultDTO();
        try {
            return credentialsTempclient.delete(requestDTO);
        } catch (FeignException fe) {
            resultDTO.setResult(false);
            resultDTO.setStatus(0);
            if (fe.status() == 422) {
                return new ResponseEntity<>(resultDTO, HttpStatus.UNPROCESSABLE_ENTITY);
            } else {
                log.info("Error not programmed from service: {} {}", fe.status(), fe.getMessage());
                return new ResponseEntity<>(resultDTO, HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            log.error("Unexpected error calling service for delete temp credentials: {}", e.getMessage());
            resultDTO.setStatus(-1);
            resultDTO.setResult(false);
            return new ResponseEntity<>(resultDTO, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiResponses({
            @ApiResponse(code = 200, message = "Success operation"),
            @ApiResponse(code = 422, message = "Fail operation")
    })
    @ApiOperation(value = "Creates the temp credential for the requested customer", response = CredentialsValidationResultDTO.class, notes = "The result json object contains the detail error status if any")
    @RequestMapping(value = "/credentials/credentials", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CredentialsValidationResultDTO> create(@RequestBody CredentialsSaveRequestDTO requestDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{create() }");
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        requestDTO.setUserRequesting(username);
        log.info("Password received: {}", requestDTO.getPassword());
        String password = decrypt.decrypt(requestDTO.getPassword());
        log.info("Password in clear: {}", password);
        requestDTO.setPassword(password);
        CredentialsValidationResultDTO resultDTO = new CredentialsValidationResultDTO();
        try {
            return credentialsTempclient.create(requestDTO);
        } catch (FeignException fe) {
            resultDTO.setResult(false);
            resultDTO.setStatus(0);
            if (fe.status() == 422) {
                return new ResponseEntity<>(resultDTO, HttpStatus.UNPROCESSABLE_ENTITY);
            } else {
                log.info("Error not programmed from service: {} {}", fe.status(), fe.getMessage());
                return new ResponseEntity<>(resultDTO, HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            log.error("Unexpected error calling service for delete temp credentials: {}", e.getMessage());
            resultDTO.setStatus(-1);
            resultDTO.setResult(false);
            return new ResponseEntity<>(resultDTO, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiResponses({
            @ApiResponse(code = 200, message = "Success operation"),
            @ApiResponse(code = 403, message = "Fail operation, not valid credentials")
    })
    @ApiOperation(value = "Validates the temp credential for the requested customer", response = CredentialsValidationResultDTO.class, notes = "The result json object contains the detail error status if any")
    @RequestMapping(value = "/credentials/credentials", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CredentialsValidationPersonalResultDTO> validate(@RequestBody CredentialsValidationRequestDTO requestDTO, HttpServletRequest request) {
    	log.info("INFO: "+this.getClass().getName()+".{validate() }");
        Gson gson = new Gson();
//        log.info("request>"+ gson.toJson(request).toString());
        log.info("requestDTO>"+ gson.toJson(requestDTO).toString());
        CredentialsValidationFingersRequestDTO validationFingersRequestDTO = new CredentialsValidationFingersRequestDTO();
        validationFingersRequestDTO.setUserRequesting(requestDTO.getUserRequesting());
        validationFingersRequestDTO.setUsername(requestDTO.getUsername());
        validationFingersRequestDTO.setSerial(requestDTO.getSerial());
        validationFingersRequestDTO.setPassword(requestDTO.getPassword());
        //END for validate fingers objects
		if (requestDTO.getTest() == null || requestDTO.getTest().isEmpty()) {
			String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
			requestDTO.setUserRequesting(username);
			log.info("Password received: {}", requestDTO.getPassword());
			String password = decrypt.decryptPass(requestDTO.getPassword()).trim();
			log.info("Password clear: "+password);
			requestDTO.setPassword(password);
		}
        CredentialsValidationResultDTO resultDTO = new CredentialsValidationResultDTO();
        CredentialsValidationPersonalResultDTO personalResultDTO = new CredentialsValidationPersonalResultDTO();
        try {
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            String password = passwordEncoder.encode(requestDTO.getUserRequesting());
            log.info("testpassword:"+password);
        	log.info("UserSend:"+requestDTO.getUsername());
        	log.info("PassSend:"+requestDTO.getPassword());
        	log.info("SerialSend:"+requestDTO.getSerial());
            ResponseEntity<CredentialsValidationResultDTO> responseEntity = credentialsTempclient.validate(requestDTO);
            CredentialsValidationResultDTO credentialsValidationResultDTO = responseEntity.getBody();
           
            log.info("Respuesta>"+ gson.toJson(responseEntity).toString());
            try {
                ResponseEntity<CredentialsValidationResultDTO> responseFingers = validateNoFingers(validationFingersRequestDTO, request);
                CredentialsValidationResultDTO fingersResponseBody = responseFingers.getBody();
                Integer fingersNo = fingersResponseBody.getStatus();
                credentialsValidationResultDTO.setStatus(fingersNo);
            } catch (Exception e) {
                log.error("Error getting fingers for request, setting 10 as default, message: {}", e.getMessage());
                credentialsValidationResultDTO.setStatus(10);
            }
            try {
                ResponseEntity<PersonalDataCredentialDTO> responseEntityPersonal = credentialsTempclient.validateUsername(requestDTO);
                PersonalDataCredentialDTO personalDataCredentialDTO = responseEntityPersonal.getBody();
                personalResultDTO.setName(personalDataCredentialDTO.getName());
                personalResultDTO.setLastnameLast(personalDataCredentialDTO.getLastnameLast());
                personalResultDTO.setLastnameFirst(personalDataCredentialDTO.getLastnameFirst());
            } catch (Exception e) {
                log.error("Error finding personal data from credentials with message: {}", e.getMessage());
                personalResultDTO.setLastnameFirst("NA");
                personalResultDTO.setLastnameLast("NA");
                personalResultDTO.setName("NA");
            }
            personalResultDTO.setStatus(credentialsValidationResultDTO.getStatus());
            personalResultDTO.setResult(credentialsValidationResultDTO.getResult());
            return new ResponseEntity<>(personalResultDTO, HttpStatus.OK);
        } catch (FeignException fe) {
        	 log.info("Error",fe);
            resultDTO.setResult(false);
            resultDTO.setStatus(0);
            if (fe.status() == 403) {
                return new ResponseEntity<>(personalResultDTO, HttpStatus.FORBIDDEN);
            } else {
                log.info("Error not programmed from service: {} {}", fe.status(), fe.getMessage());
                return new ResponseEntity<>(personalResultDTO, HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            log.error("Unexpected error calling service for delete temp credentials: {}", e.getMessage());
            resultDTO.setStatus(-1);
            resultDTO.setResult(false);
            return new ResponseEntity<>(personalResultDTO, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiResponses({
            @ApiResponse(code = 200, message = "Success operation"),
            @ApiResponse(code = 422, message = "Fail operation")
    })
    @ApiOperation(value = "Deletes the temp credential for the requested customer", response = CredentialsValidationResultDTO.class, notes = "The result json object contains the detail error status if any")
    @RequestMapping(value = "/credentials/credentials/fingers", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CredentialsValidationResultDTO> validateNoFingers(@RequestBody CredentialsValidationFingersRequestDTO requestDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{validateNoFingers() }");
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        requestDTO.setUserRequesting(username);
        String password = decrypt.decrypt(requestDTO.getPassword());
        requestDTO.setPassword(password);
        CredentialsValidationResultDTO resultDTO = new CredentialsValidationResultDTO();
        try {
            return credentialsTempclient.validateNoFingers(requestDTO);
        } catch (FeignException fe) {
            resultDTO.setResult(false);
            resultDTO.setStatus(0);
            if (fe.status() == 422) {
                return new ResponseEntity<>(resultDTO, HttpStatus.FORBIDDEN);
            } else {
                log.info("Error not programmed from service: {} {}", fe.status(), fe.getMessage());
                return new ResponseEntity<>(resultDTO, HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            log.error("Unexpected error calling service for delete temp credentials: {}", e.getMessage());
            resultDTO.setStatus(-1);
            resultDTO.setResult(false);
            return new ResponseEntity<>(resultDTO, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @RequestMapping(value = "/credentials", method = RequestMethod.PATCH, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CredentialsValidationResultDTO> updateCredTemp(@RequestBody UpdateTempStatusCredRequestDTO requestDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{updateCredTemp() }");
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        requestDTO.setUserReq(username);
        CredentialsValidationResultDTO resultDTO = new CredentialsValidationResultDTO();
        try {
            return credentialsTempclient.updateCredTemp(requestDTO);
        } catch (FeignException fe) {
            resultDTO.setResult(false);
            resultDTO.setStatus(0);
            if (fe.status() == 422) {
                return new ResponseEntity<>(resultDTO, HttpStatus.UNPROCESSABLE_ENTITY);
            } else {
                log.info("Error not programmed from service: {} {}", fe.status(), fe.getMessage());
                return new ResponseEntity<>(resultDTO, HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            log.error("Unexpected error calling service for update temp credentials: {}", e.getMessage());
            resultDTO.setStatus(-1);
            resultDTO.setResult(false);
            return new ResponseEntity<>(resultDTO, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

}
