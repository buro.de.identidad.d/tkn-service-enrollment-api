package com.teknei.bid.controller.rest.unauth;

import com.teknei.bid.controller.rest.EnrollmentCredentialsController;
import com.teknei.bid.controller.rest.util.crypto.TokenUtils;
import com.teknei.bid.dto.*;
import feign.FeignException;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@RestController
@RequestMapping(value = "/rest/unauth/enrollment/credentials")
@CrossOrigin
public class EnrollmentCredentialsControllerUnauth {
	private static final Logger log = LoggerFactory.getLogger(EnrollmentCredentialsControllerUnauth.class);
    @Autowired
    private EnrollmentCredentialsController controller;

    @ApiOperation(value = "Verifies the curp related to the customer. The request could be by customer internal identification or by external data")
    @RequestMapping(value = "/curp/validate", method = RequestMethod.POST)
    public ResponseEntity<String> validateCurp(@RequestBody CurpRequestDTO curpRequestDTO, HttpServletRequest request){
    	//log.info("lblancas: "+this.getClass().getName()+".{validateCurp(POST) }");
        return controller.validateCurp(curpRequestDTO, request);
    }

    @ApiOperation(value = "Obtain the curp related to the customer. The request could be by customer internal identification or by external data")
    @RequestMapping(value = "/curp/obtain", method = RequestMethod.POST)
    public ResponseEntity<String> getCurp(@RequestBody CurpRequestDTO curpRequestDTO, HttpServletRequest request){
    	//log.info("lblancas: "+this.getClass().getName()+".{getCurp(POST) }");
        return controller.getCurp(curpRequestDTO, request);
    }


    @ApiOperation(value = "Verifies the information against INE CECOBAN stage", response = String.class)
    @RequestMapping(value = "/verifyCecoban", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> verifyAgainstCecoban(@RequestBody PersonDataIneTKNRequest personDataIneTKNRequest, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{verifyAgainstCecoban(POST) }");
        return controller.verifyAgainstCecoban(personDataIneTKNRequest, request);
    }

    @Deprecated
    @RequestMapping(value = "/verify", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> verifyAgainstINE(@RequestBody PersonData personData) {
    	//log.info("lblancas: "+this.getClass().getName()+".{verifyAgainstINE(POST) }");
        return controller.verifyAgainstINE(personData);
    }

    @RequestMapping(value = "/credential/findDetail/{type}/{id}", method = RequestMethod.GET)
    public ResponseEntity<IneDetailDTO> findDetail(@PathVariable Integer type, @PathVariable Long id) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findDetail(GET) }");
        return controller.findDetail(type, id);
    }

    @ApiOperation(value = "Updates the information given by parsing systems. The user field is the logged user", response = OperationResult.class)
    @RequestMapping(value = "/credential/update/{type}/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OperationResult> updateCredentialData(@PathVariable Integer type, @PathVariable Long id, @RequestBody IneDetailDTO ineDetailDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{updateCredentialData(PUT) }");
        return controller.updateCredentialData(type, id, ineDetailDTO, request);
    }

    @ApiOperation(value = "Stores and validates the credentials sent as attachment list, also expects a JSON like {'operationId' : 1}", response = OperationResult.class)
    @RequestMapping(value = "/credential", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<OperationResult> credentialsOperation(
            @RequestPart(value = "json") String credentialsOperationRequest,
            @RequestPart(value = "file", required = false) List<MultipartFile> files) {
    	log.info("INFO: "+this.getClass().getName()+".{credentialsOperation(POST) <<<<<<<<<<<<OCR}");
        return controller.credentialsOperation(credentialsOperationRequest, files);
    }

    @ApiOperation(value = "Stores and validates the additional credentials sent as attachment list, also expects a JSON like {'operationId' : 1}", response = OperationResult.class)
    @RequestMapping(value = "/credentialsAdditional", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<OperationResult> additionalCredentialsOperation(@RequestPart(value = "json") String additionalCredentialsOperationRequest,
                                                                          @RequestPart(value = "file", required = false) List<MultipartFile> files) {
    	//log.info("lblancas: "+this.getClass().getName()+".{additionalCredentialsOperation(POST) }");
        return controller.additionalCredentialsOperation(additionalCredentialsOperationRequest, files);
    }

    @ApiOperation(value = "Stores and validates the result of the credentials validation, also expects a JSON like {'operationId' : 1}", response = OperationResult.class)
    @RequestMapping(value = "/credentialsValidation", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<OperationResult> credentialsValidationOperation(@RequestPart(value = "json") String additionalCredentialsOperationRequest,
                                                                          @RequestPart(value = "file", required = false) List<MultipartFile> files) {
    	//log.info("lblancas: "+this.getClass().getName()+".{credentialsValidationOperation(POST) }");
        return controller.credentialsValidationOperation(additionalCredentialsOperationRequest, files);
    }

    @ApiOperation(value = "Stores and validates credentials in background mode", response = OperationResult.class)
    @RequestMapping(value = "/credentialAsync", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OperationResult> credentialsAsync(@RequestBody RequestEncFilesDTO dto, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{credentialsAsync(POST) }");
        return controller.credentialsAsync(dto, request);
    }

    @ApiOperation(value = "Stores and validates credentials in background mode. Ciphered endpoint", response = OperationResult.class)
    @RequestMapping(value = "/credentialAsyncCiphered", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OperationResult> credentialsAsyncCiphered(@RequestBody RequestEncFilesDTO dto, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{credentialsAsyncCiphered(POST) }");
        return controller.credentialsAsyncCiphered(dto, request);
    }

    @ApiOperation(value = "Stores and validates credentials evidences in background mode", response = OperationResult.class)
    @RequestMapping(value = "/credentialCaptureAsync", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OperationResult> credentialsCaptureAsync(@RequestBody RequestEncFilesDTO dto, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{credentialsCaptureAsync(POST) }");
        return controller.credentialsCaptureAsync(dto, request);
    }

    @ApiOperation(value = "Stores and validates credentials evidences in background mode. Ciphered endpoint", response = OperationResult.class)
    @RequestMapping(value = "/credentialCaptureAsyncCiphered", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OperationResult> credentialsCaptureAsyncCiphered(@RequestBody RequestEncFilesDTO dto, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{credentialsCaptureAsyncCiphered(POST) }");
        return controller.credentialsCaptureAsyncCiphered(dto, request);
    }

    @ApiOperation(value = "Stores and validates additional credentials in background mode", response = OperationResult.class)
    @RequestMapping(value = "/credentialAdditionalAsync", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OperationResult> credentialsAdditionalAsync(@RequestBody RequestEncFilesDTO dto, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{credentialsAdditionalAsync(POST) }");
        return controller.credentialsAdditionalAsync(dto, request);
    }

    @ApiOperation(value = "Stores and validates additional credentials in background mode. Ciphered endpoint", response = OperationResult.class)
    @RequestMapping(value = "/credentialAdditionalAsyncCiphered", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OperationResult> credentialsAdditionalAsyncCiphered(@RequestBody RequestEncFilesDTO dto, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{credentialsAdditionalAsyncCiphered(POST) }");
        return controller.credentialsAdditionalAsyncCiphered(dto, request);
    }

    @RequestMapping(value = "/uploadPlain", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OperationResult> uploadPlain(@RequestBody RequestEncFilesDTO dto, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{uploadPlain(POST) }");
        return controller.uploadPlain(dto, request);
    }

    @RequestMapping(value = "/uploadPlainCiphered", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OperationResult> uploadPlainEnc(@RequestBody RequestEncFilesDTO dto, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{uploadPlainEnc(POST) }");
        return controller.uploadPlainEnc(dto, request);
    }

    @RequestMapping(value = "/credential/primaryIdRollback", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public BasicRequestDTO rollbackPrimaryIdConfirmation(@RequestBody BasicRequestDTO basicRequestDTO){
    	//log.info("lblancas: "+this.getClass().getName()+".{rollbackPrimaryIdConfirmation(POST) }");
        return controller.rollbackPrimaryIdConfirmation(basicRequestDTO);
    }
}
