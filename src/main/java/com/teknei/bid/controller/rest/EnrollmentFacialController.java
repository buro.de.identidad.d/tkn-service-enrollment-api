package com.teknei.bid.controller.rest;

import com.teknei.bid.controller.rest.util.crypto.Decrypt;
import com.teknei.bid.controller.rest.util.crypto.TokenUtils;
import com.teknei.bid.dto.OperationResult;
import com.teknei.bid.dto.RequestEncFilesDTO;
import com.teknei.bid.service.remote.FacialClient;
import com.teknei.bid.service.remote.impl.FacialAttachmentClient;
import com.teknei.bid.service.validation.JsonValidation;
import feign.FeignException;
import io.swagger.annotations.ApiOperation;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/rest/v3/enrollment/facial")
@CrossOrigin
public class EnrollmentFacialController {

    @Autowired
    private FacialAttachmentClient facialAttachmentClient;
    @Autowired
    private FacialClient facialClient;
    @Autowired
    private Decrypt decrypt;
    @Autowired
    private TokenUtils tokenUtils;
    private static final Logger log = LoggerFactory.getLogger(EnrollmentFacialController.class);


    @ApiOperation(value = "Uploads a picture in async way to the casefile related to the given customer on base64 format. Ciphered endpoint")
    @RequestMapping(value = "/uploadAsyncCiphered", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OperationResult> uploadPlainCiphered(@RequestBody RequestEncFilesDTO requestEncFilesDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{uploadPlainCiphered() }");
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        requestEncFilesDTO.setUsername(username);
        Thread t = buildThread(requestEncFilesDTO, true);
        t.start();
        OperationResult operationResult = new OperationResult();
        operationResult.setErrorMessage("BOOKED");
        operationResult.setResultOK(true);
        return new ResponseEntity<>(operationResult, HttpStatus.OK);
    }

    @ApiOperation(value = "Uploads a picture in async way to the casefile related to the given customer on base64 format")
    @RequestMapping(value = "/uploadAsync", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OperationResult> uploadPlain(@RequestBody RequestEncFilesDTO requestEncFilesDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{uploadPlain() }");
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        requestEncFilesDTO.setUsername(username);
        Thread t = buildThread(requestEncFilesDTO, false);
        t.start();
        OperationResult operationResult = new OperationResult();
        operationResult.setErrorMessage("BOOKED");
        operationResult.setResultOK(true);
        return new ResponseEntity<>(operationResult, HttpStatus.OK);
    }

    @ApiOperation(value = "Uploads a picture in plain mode and stores it in casefile")
    @RequestMapping(value = "/uploadPlain", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OperationResult> uploadPlainOnline(@RequestBody RequestEncFilesDTO requestEncFilesDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{uploadPlainOnline() }");
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        requestEncFilesDTO.setUsername(username);
        OperationResult operationResult = new OperationResult();
        try {
            ResponseEntity<String> responseEntity = facialClient.uploadPlain(requestEncFilesDTO);
            operationResult.setResultOK(true);
            operationResult.setErrorMessage("Se ha almacenado el rostro en el expediente");
            return new ResponseEntity<>(operationResult, HttpStatus.OK);
        } catch (FeignException fe) {
            return parseFeign(fe);
        } catch (Exception e) {
            log.error("Error upload plain online with message: {}", e.getMessage());
            operationResult.setResultOK(false);
            operationResult.setErrorMessage("20001");
            return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Uploads a picture in plain mode and stores it in casefile")
    @RequestMapping(value = "/uploadPlainCiphered", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OperationResult> uploadPlainOnlineEnc(@RequestBody RequestEncFilesDTO requestEncFilesDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{uploadPlainOnlineEnc() }");
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        requestEncFilesDTO.setB64Anverse(decrypt.decrypt(requestEncFilesDTO.getB64Anverse()));
        return uploadPlainOnline(requestEncFilesDTO, request);
    }

    private ResponseEntity<OperationResult> parseFeign(FeignException fe) {
    	//log.info("lblancas: "+this.getClass().getName()+".{parseFeign() }");
        OperationResult operationResult = new OperationResult();
        String errorTrace = fe.getMessage();
        String[] messageTrace = errorTrace.split("content:");
        if (messageTrace.length > 1) {
            String errorCode = messageTrace[messageTrace.length - 1];
            errorCode = errorCode.trim();
            try {
                Integer.parseInt(errorCode);
                operationResult.setResultOK(false);
                operationResult.setErrorMessage(errorCode);
                return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
            } catch (NumberFormatException ne) {
                log.error("No valid return error code from remote service for: {}", errorCode);
            }
        }
        operationResult.setResultOK(false);
        operationResult.setErrorMessage("20001");
        return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    private Thread buildThread(RequestEncFilesDTO dto, boolean ciphered) {
    	//log.info("lblancas: "+this.getClass().getName()+".{buildThread() }");
        Runnable target = () -> {
            try {
                if (ciphered) {
                    String targetStr = decrypt.decrypt(dto.getB64Anverse());
                    String anverse = new String(targetStr);
                    String reverse = null;
                    if (dto.getB64Reverse() != null && !dto.getB64Reverse().isEmpty()) {
                        String targetStrR = decrypt.decrypt(dto.getB64Reverse());
                        reverse = new String(targetStrR);
                    }
                    dto.setB64Anverse(anverse);
                    dto.setB64Reverse(reverse);
                }
                facialClient.uploadPlain(dto);
            } catch (Exception e) {
                log.error("Error uploading async credential in plain with message: {}", e.getMessage());
            }
        };
        Thread t = new Thread(target);
        return t;
    }
    @Deprecated
    @ApiOperation(value = "Stores the picture of the face for the case file. Expects the attachment and a JSON like {'operationId' : 1}", response = OperationResult.class)
    @RequestMapping(value = "/face", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    public ResponseEntity<OperationResult> addFace(
            @RequestPart(value = "json") String jsonRequest,
            @RequestPart(value = "file") MultipartFile file) 
    {
    	log.info("lblancas: "+this.getClass().getName()+".{addFace() }");
        OperationResult operationResult = new OperationResult();
        JSONObject credentialsOperationRequestJSON = null;
        if (!JsonValidation.validateJson(jsonRequest)) 
        {
            operationResult.setErrorMessage("Bad Request");
            return new ResponseEntity<>(operationResult, HttpStatus.BAD_REQUEST);
        }
        credentialsOperationRequestJSON = new JSONObject(jsonRequest);
        Long operationId = credentialsOperationRequestJSON.getLong("operationId");
        try 
        {
        	log.info("lblancas: "+this.getClass().getName()+".{addFace()  Inicia}");
            ResponseEntity<String> responseEntity = facialAttachmentClient.upload(file, operationId);
            log.info("lblancas: "+this.getClass().getName()+".{addFace()  Upload con Exito}");
            operationResult.setResultOK(true);
            operationResult.setErrorMessage("Se ha almacenado el rostro en el expediente");
            if (responseEntity == null) 
            {
            	log.info("lblancas: "+this.getClass().getName()+".{addFace()  responseEntity null}");
                operationResult.setResultOK(false);
                operationResult.setErrorMessage("20001");
                return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
            }
            else if (responseEntity.getStatusCode().is4xxClientError() || responseEntity.getStatusCode().is5xxServerError()) 
            {
            	log.info("lblancas: "+this.getClass().getName()+".{addFace()  No procesado } ["+
		            responseEntity.getStatusCode().is4xxClientError() +","+ 
		            responseEntity.getStatusCode().is5xxServerError()+"]");
                operationResult.setResultOK(false);
                operationResult.setErrorMessage(responseEntity.getBody());
                return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
            }
            else 
            {
            	log.info("lblancas: "+this.getClass().getName()+".{addFace() } Se ha almacenado el rostro en el expediente");
                operationResult.setResultOK(true);
                operationResult.setErrorMessage("Se ha almacenado el rostro en el expediente");
                return new ResponseEntity<>(operationResult, HttpStatus.OK);
            }

        }
        catch (HttpClientErrorException he) 
        {
        	log.error("Error(1) in addFace for: with message: {}" ,he.getMessage());
            operationResult.setResultOK(false);
            operationResult.setErrorMessage(he.getResponseBodyAsString());
            return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
        }
        catch (Exception e) 
        {
            log.error("Error(2) in addFace for: with message: {}", e.getMessage());
            operationResult.setResultOK(false);
            operationResult.setErrorMessage("20001");
            return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
    @Deprecated
    @ApiOperation(value = "Stores the picture of the face for the case file. Expects the attachment and a JSON like {'operationId' : 1}", response = OperationResult.class)
    @RequestMapping(value = "/faceX", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    public ResponseEntity<OperationResult> addFaceX(
            @RequestPart(value = "json") String jsonRequest,
            @RequestPart(value = "file") MultipartFile file) 
    {
    	log.info("lblancas: "+this.getClass().getName()+".{addFace() }");
        OperationResult operationResult = new OperationResult();
        JSONObject credentialsOperationRequestJSON = null;
        log.info("lblancas: Paso 0");
        if (!JsonValidation.validateJson(jsonRequest)) 
        {
            operationResult.setErrorMessage("Bad Request");
            return new ResponseEntity<>(operationResult, HttpStatus.BAD_REQUEST);
        }
        log.info("lblancas: Paso 1");
        credentialsOperationRequestJSON = new JSONObject(jsonRequest);
        log.info("lblancas: Paso 2");
        Long operationId = credentialsOperationRequestJSON.getLong("operationId");
        log.info("lblancas: Paso 3 :"+operationId);
        try {
            ResponseEntity<String> responseEntity = facialAttachmentClient.upload(file, operationId);
            log.info("lblancas: Paso 4");
            operationResult.setResultOK(true);
            log.info("lblancas: Paso 5");	
            operationResult.setErrorMessage("Se ha almacenado el rostro en el expediente");
            log.info("lblancas (respuesta) responseEntity>>"+responseEntity);
            if (responseEntity == null) {
            	log.info("lblancas (respuesta) 1");
                operationResult.setResultOK(false);
                operationResult.setErrorMessage("20001");
                return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
            }
            else if (responseEntity.getStatusCode().is4xxClientError() || responseEntity.getStatusCode().is5xxServerError()) 
            {
            	log.info("lblancas (respuesta) 2");
                operationResult.setResultOK(false);
                operationResult.setErrorMessage(responseEntity.getBody());
                return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
            } 
             else 
            {
            	 log.info("lblancas (respuesta) 3");
                operationResult.setResultOK(true);
                operationResult.setErrorMessage("Se ha almacenado el rostro en el expediente");
                return new ResponseEntity<>(operationResult, HttpStatus.OK);
            }

        } catch (HttpClientErrorException he) {
        	log.error("Error in addFace for: {} with message: {1}", jsonRequest, he.getMessage());
            operationResult.setResultOK(false);
            operationResult.setErrorMessage(he.getResponseBodyAsString());
            return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
        } catch (Exception e) {
            log.error("Error in addFace for: {} with message: {2}", jsonRequest, e.getMessage());
            operationResult.setResultOK(false);
            operationResult.setErrorMessage("20001");
            return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }


}
