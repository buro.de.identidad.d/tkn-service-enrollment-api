package com.teknei.bid.controller.rest;

import com.teknei.bid.controller.rest.util.crypto.TokenUtils;
import com.teknei.bid.dto.*;
import com.teknei.bid.service.remote.ManagementClient;

import feign.FeignException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping(value = "/rest/v3/management")
@CrossOrigin
public class ManagementResourcesController {

    @Autowired
    private TokenUtils tokenUtils;

    @Autowired
    private ManagementClient managementClient;

    @RequestMapping(value = "/admin/usua", method = RequestMethod.GET)
    public ResponseEntity<List<BidUsuaDTO>> findAllActive() {
        return managementClient.findAllActive();
    }

    @RequestMapping(value = "/admin/usua", method = RequestMethod.POST)
    public ResponseEntity<BidUsuaDTO> insertUsua(@RequestBody BidUsuaDTO dto, HttpServletRequest request) {
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        dto.setUsername(username);
        return managementClient.insertUsua(dto);
    }

    @RequestMapping(value = "/admin/usua", method = RequestMethod.PUT)
    public ResponseEntity<BidUsuaDTO> modifyUsua(@RequestBody BidUsuaDTO dto, HttpServletRequest request) {
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        dto.setUsername(username);
        return managementClient.modifyUsua(dto);
    }
    
    /**
     * Servicio para la modificacion de contraseña 
     * @author AJGD
     * @param dto tipo oper 0 = vigencia, 1 = recuperacion;
     * @param request
     * @return
     */
    @RequestMapping(value = "/admin/usua/updatePassword", method = RequestMethod.PUT)
    @ApiResponses(value = {
    	    @ApiResponse(code = 200, message = "Ok"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<String> updatePassword(@RequestBody BidUpdatePassUsuaDTO dto, HttpServletRequest request) {
    	try {
    		return managementClient.updatePassword(dto);
		} catch (FeignException e) {	
			switch (e.status()) {			
			case 404:
				return new ResponseEntity<>(e.getMessage().split("content:")[1].trim(), HttpStatus.NOT_FOUND);
			case 500:
				return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			default:
				return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
        
    }

    @RequestMapping(value = "/admin/usua/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<BidUsuaDTO> deleteUsua(@PathVariable Long id) {
        BidUsuaDTO dto = new BidUsuaDTO();
        dto.setIdUsua(id);
        dto.setPassword("password");
        dto.setUsua("usua");
        return managementClient.deleteUsua(dto);
    }

    @RequestMapping(value = "/admin/empr", method = RequestMethod.GET)
    public ResponseEntity<List<BidEmprDTO>> findAllEmprActive() {
        return managementClient.findAllEmprActive();
    }

    @RequestMapping(value = "/admin/empr", method = RequestMethod.POST)
    public ResponseEntity<BidEmprDTO> saveEmpr(@RequestBody BidEmprDTO dto, HttpServletRequest request) {
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        dto.setUsername(username);
        return managementClient.saveEmpr(dto);
    }

    @RequestMapping(value = "/admin/empr", method = RequestMethod.PUT)
    public ResponseEntity<BidEmprDTO> updateEmpr(@RequestBody BidEmprDTO dto, HttpServletRequest request) {
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        dto.setUsername(username);
        return managementClient.updateEmpr(dto);
    }

    @RequestMapping(value = "/admin/empr/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<BidEmprDTO> deleteEmpr(@PathVariable Long id, HttpServletRequest request) {
        BidEmprDTO dto = new BidEmprDTO();
        dto.setIdEmpr(id);
        dto.setEmpr("empr");
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        dto.setUsername(username);
        return managementClient.deleteEmpr(dto);
    }

    @RequestMapping(value = "/admin/disp", method = RequestMethod.GET)
    public ResponseEntity<List<BidDispDTO>> findAllDispActive() {
        return managementClient.findAllDispActive();
    }


    @RequestMapping(value = "/admin/disp", method = RequestMethod.POST)
    public ResponseEntity<BidDispDTO> createDisp(@RequestBody BidDispDTO dto, HttpServletRequest request) {
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        dto.setUsername(username);
        return managementClient.createDisp(dto);
    }

    @RequestMapping(value = "/admin/disp", method = RequestMethod.PUT)
    public ResponseEntity<BidDispDTO> updateDisp(@RequestBody BidDispDTO dto, HttpServletRequest request) {
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        dto.setUsername(username);
        return managementClient.updateDisp(dto);
    }

    @RequestMapping(value = "/admin/disp/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<BidDispDTO> deleteDisp(@PathVariable Long id, HttpServletRequest request) {
        BidDispDTO dto = new BidDispDTO();
        dto.setDescDisp("desc");
        dto.setIdDisp(id);
        dto.setNumSeri("nume");
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        dto.setUsername(username);
        return managementClient.deleteDisp(dto);
    }

    @RequestMapping(value = "/assign/companyDevice", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> assignCompanyDevice(@RequestBody BidAssignmentRequestDTO requestDTO, HttpServletRequest request) {
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        requestDTO.setUsername(username);
        return managementClient.assignCompanyDevice(requestDTO);
    }

    @RequestMapping(value = "/assign/companyUser", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> assignCompanyUser(@RequestBody BidAssignmentRequestDTO requestDTO, HttpServletRequest request) {
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        requestDTO.setUsername(username);
       return managementClient.assignCompanyUser(requestDTO);
    }

    @RequestMapping(value = "/assign/companyUser/{idUser}", method = RequestMethod.GET)
    public ResponseEntity<List<BidEmprUsua>> findCompanyFromUser(@PathVariable Long idUser) {
        return managementClient.findCompanyFromUser(idUser);
    }

    @RequestMapping(value = "/assign/companyDevice/{idDevice}", method = RequestMethod.GET)
    public ResponseEntity<List<BidEmprDisp>> findCompanyFromDevice(@PathVariable Long idDevice) {
        return managementClient.findCompanyFromDevice(idDevice);
    }


}