package com.teknei.bid.controller.rest;

import com.teknei.bid.controller.rest.util.crypto.TokenUtils;
import com.teknei.bid.dto.*;
import com.teknei.bid.service.remote.MailClient;
import feign.FeignException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/rest/v3/enrollment/mail")
@CrossOrigin
public class EnrollmentMailController {

    private static final Logger log = LoggerFactory.getLogger(EnrollmentMailController.class);
    @Autowired
    private MailClient mailClient;
    @Autowired
    private TokenUtils tokenUtils;
    @Autowired
    private EnrollmentCertController enrollmentCertController;

    @ApiOperation(value = "Sends the generated contract for the related customer", response = String.class)
    @RequestMapping(value = "/contractSigned", method = RequestMethod.POST)
    public ResponseEntity<String> sendContract(@RequestBody MailRequestDTO mailRequestDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{sendContract() }");
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        MailRequestDTOService serviceDTO = new MailRequestDTOService();
        serviceDTO.setIdClient(mailRequestDTO.getIdClient());
        serviceDTO.setUsername(username);	
        try {
            mailClient.sendContract(serviceDTO);
            return new ResponseEntity<>("OK", HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error sending contract to: {} with message: {}", mailRequestDTO, e.getMessage());
            return new ResponseEntity<>("ERROR", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Puts an OTP key in DB related to the current id for the customer. Deactivates the old ones", response = String.class)
    @RequestMapping(value = "/mail/verification/otp/resend", method = RequestMethod.POST)
    public ResponseEntity<String> reGenerateOTP(@RequestBody MailRequestDTO dto, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{reGenerateOTP() }");
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        MailRequestDTOService mailRequestDTO = new MailRequestDTOService();
        mailRequestDTO.setIdClient(dto.getIdClient());
        mailRequestDTO.setUsername(username);
        JSONObject jsonObject = new JSONObject();
        try {
            mailClient.reGenerateOTP(mailRequestDTO);
            jsonObject.put("success", true);
            jsonObject.put("message", "OK");
            return new ResponseEntity<>(jsonObject.toString(), HttpStatus.OK);
        } catch (Exception e) {
            jsonObject.put("success", false);
            jsonObject.put("message", "Unable to re-send verification code");
            log.error("Error in reGenerateOTP for: {}, {}", dto, e.getMessage());
            return new ResponseEntity<>(jsonObject.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Puts an OTP key in DB related to the current id for the customer", response = String.class)
    @RequestMapping(value = "/mail/verification/otp", method = RequestMethod.POST)
    public ResponseEntity<String> generateOTP(@RequestBody MailRequestDTO dto, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{generateOTP() }");
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        MailRequestDTOService mailRequestDTO = new MailRequestDTOService();
        mailRequestDTO.setIdClient(dto.getIdClient());
        mailRequestDTO.setUsername(username);
        try {
            mailClient.generateOTP(mailRequestDTO);
            return new ResponseEntity<>("OK", HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error in generateOTP for: {}", dto, e.getMessage());
            return new ResponseEntity<>("Error", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Puts an OTP key in DB related to the current id for the customer for consume a videocall conference", response = String.class)
    @RequestMapping(value = "/mail/verification/otp/videoconference", method = RequestMethod.POST)
    public ResponseEntity<String> generateOTPVideoconference(@RequestBody MailRequestDTO dto, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{generateOTPVideoconference() }");
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        MailRequestDTOService mailRequestDTO = new MailRequestDTOService();
        mailRequestDTO.setIdClient(dto.getIdClient());
        mailRequestDTO.setUsername(username);
        try {
            mailClient.generateOTPVideoconference(mailRequestDTO);
            return new ResponseEntity<>("OK", HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error in generateOTP for: {}", dto, e.getMessage());
            return new ResponseEntity<>("Error", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Validates OTP and uses it", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Validation successfull"),
            @ApiResponse(code = 404, message = "No OTP found"),
            @ApiResponse(code = 422, message = "Error validating OTP")
    })
    @RequestMapping(value = "/validateOTP", method = RequestMethod.POST)
    public ResponseEntity<String> validateOTP(@RequestBody OTPVerificationDTO dto, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{validateOTP() }");
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        try {
            OTPVerificationDTOService service = new OTPVerificationDTOService();
            service.setIdClient(dto.getIdClient());
            service.setOtp(dto.getOtp());
            service.setUsername(username);
            return mailClient.validateOTP(service);
        } catch (FeignException e) {
            switch (e.status()) {
                case 404:
                    return new ResponseEntity<>((String) null, HttpStatus.NOT_FOUND);
                case 422:
                    return new ResponseEntity<>((String) null, HttpStatus.UNPROCESSABLE_ENTITY);
                case 500:
                    log.error("Error validating OTP for: {} with message: {}", dto, e.getMessage());
                    return new ResponseEntity<>((String) null, HttpStatus.UNPROCESSABLE_ENTITY);
                default:
                    log.error("Error validating OTP for: {} with message: {}", dto, e.getMessage());
                    return new ResponseEntity<>((String) null, HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            log.error("Error validating OTP for: {} with message: {}", dto, e.getMessage());
            return new ResponseEntity<>((String) null, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Validates current OTP for customer and triggers the certificate request")
    @RequestMapping(value = "/validateOTP/cert", method = RequestMethod.POST)
    public ResponseEntity<String> validateOtpAndGenerateCert(@RequestBody OTPVerificationDTO dto, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{validateOtpAndGenerateCert() }");
        ResponseEntity<String> otpVerificationResponse = validateOTP(dto, request);
        if (otpVerificationResponse.getStatusCode().is2xxSuccessful()) {
            startThreadForCustomerCert(dto.getIdClient(), request);
        }
        return otpVerificationResponse;
    }

    private void startThreadForCustomerCert(Long customerId, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{startThreadForCustomerCert() }");
        OperationIdDTO operationIdDTO = new OperationIdDTO();
        operationIdDTO.setOperationId(customerId);
        Runnable runnable = () -> {
            enrollmentCertController.generateCert(operationIdDTO, request);
        };
        Thread t = new Thread(runnable);
        t.start();
    }
    
    //TODO prueba AJGD -------------------------------------------------------------------------->>>	
    
    @ApiOperation(value = "Gets all credit institution records", response = String.class)
    @RequestMapping(value = "/forgetPassword/{user}/{mail}", method = RequestMethod.GET)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Validation successfull"),
            @ApiResponse(code = 408, message = "User not found"),
            @ApiResponse(code = 500, message = "Error sending sms")
    })
	public ResponseEntity<String> forgetPassword(@PathVariable String user, @PathVariable String mail) {
		log.info("AJGD: " + this.getClass().getName() + ".{forgetPassword(2) }");
		try {
			return mailClient.forgetPassword(user, mail);
		} catch (FeignException e) {
			log.error("Error validating OTP for: {} with message: {}", e.getMessage());
			switch (e.status()) {			
			case 500:
				return new ResponseEntity<>(e.getMessage().split("content:")[1].trim(), HttpStatus.INTERNAL_SERVER_ERROR);
			default:
				return new ResponseEntity<>(e.getMessage().split("content:")[1].trim(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}
    
    @ApiOperation(value = "Validates OTP and uses it to forget Password", response = String.class)
    @ApiResponses(value = {
    		 @ApiResponse(code = 200, message = "Validation successfull"),
             @ApiResponse(code = 404, message = "No OTP found"),
             @ApiResponse(code = 408, message = "OTP expirated"),
             @ApiResponse(code = 500, message = "Error validating OTP")
    })
	@RequestMapping(value = "/forgetPassword/validateOTP", method = RequestMethod.POST)
	public ResponseEntity<String> forgetPasswordValidateOTP(@RequestBody OTPVerificationDTO dto,
			HttpServletRequest request) {
		log.info("AJGD: " + this.getClass().getName() + ".{forgetPasswordValidateOTP() }");
		try {
			return mailClient.forgetPasswordValidateOTP(dto);
		} catch (FeignException e) {	
			switch (e.status()) {			
			case 404:
				return new ResponseEntity<>(e.getMessage().split("content:")[1].trim(), HttpStatus.CONFLICT);
			case 408:
				return new ResponseEntity<>(e.getMessage().split("content:")[1].trim(), HttpStatus.REQUEST_TIMEOUT);
			case 500:
				log.error("Error validating OTP for: {} with message: {}", dto, e.getMessage());
				return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			default:
				log.error("Error validating OTP for: {} with message: {}", dto, e.getMessage());
				return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}
    
    //<<<-------------------

}
