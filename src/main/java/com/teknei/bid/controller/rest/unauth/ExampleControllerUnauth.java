package com.teknei.bid.controller.rest.unauth;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/rest/unauth")
@CrossOrigin
public class ExampleControllerUnauth {
	private static final Logger log = LoggerFactory.getLogger(ExampleControllerUnauth.class);
    @GetMapping
    public Map<String, String> example() {
    	//log.info("lblancas: "+this.getClass().getName()+".{example() }");
        Map<String, String> map = new HashMap<>();
        map.put("key", "value");
        return map;
    }
}
