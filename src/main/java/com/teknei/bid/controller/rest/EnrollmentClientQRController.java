package com.teknei.bid.controller.rest;

import com.teknei.bid.controller.rest.util.crypto.TokenUtils;
import com.teknei.bid.dto.ClieQRDTO;
import com.teknei.bid.dto.ClieQRDTORequest;
import com.teknei.bid.service.remote.CustomerClient;
import feign.FeignException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/rest/v3/enrollment/client/qr")
@CrossOrigin
public class EnrollmentClientQRController {

    @Autowired
    private CustomerClient customerClient;
    @Autowired
    private TokenUtils tokenUtils;
    private static final Logger log = LoggerFactory.getLogger(EnrollmentClientQRController.class);

    @RequestMapping(value = "/customerQR", method = RequestMethod.POST)
    public ResponseEntity<ClieQRDTO> generateQRCode(@RequestBody ClieQRDTORequest qrdtoRequest, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{generateQRCode() }");
        try {
            String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
            qrdtoRequest.setUsername(username);
            return customerClient.generateQRToCotinueWithBiometric(qrdtoRequest);
        } catch (Exception e) {
            log.error("Error generating QR code from service with message: {}", e.getMessage());
            return new ResponseEntity<>((ClieQRDTO) null, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @RequestMapping(value = "/customerQR/image", method = RequestMethod.POST)
    public ResponseEntity<byte[]> generateQRImageCode(@RequestBody ClieQRDTORequest qrdtoRequest, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{generateQRImageCode() }");
        try {
            String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
            qrdtoRequest.setUsername(username);
            ResponseEntity<ClieQRDTO> responseEntity = customerClient.generateQRToCotinueWithBiometric(qrdtoRequest);
            ClieQRDTO qr = responseEntity.getBody();
            if (qr != null) {
                String b64QR = qr.getQr();
                byte[] qrImage = Base64Utils.decodeFromString(b64QR);
                return new ResponseEntity<>(qrImage, HttpStatus.OK);
            } else {
                return new ResponseEntity<>((byte[]) null, HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            log.error("Error generating QR code from service with message: {}", e.getMessage());
            return new ResponseEntity<>((byte[]) null, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @RequestMapping(value = "/customerQR/{qrCode}", method = RequestMethod.GET)
    public ResponseEntity<String> validateQRCode(@PathVariable String qrCode, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{validateQRCode() }");
        try {
            String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
            ClieQRDTO clieQRDTO = new ClieQRDTO();
            clieQRDTO.setQr(qrCode);
            clieQRDTO.setUsername(username);
            return customerClient.validateQR(clieQRDTO);
        } catch (FeignException e) {
            if (e.status() == 404) {
                return new ResponseEntity<>("", HttpStatus.NOT_FOUND);
            }
            log.error("Error validating QR code from service with message: {}", e.getMessage());
            return new ResponseEntity<>("", HttpStatus.UNPROCESSABLE_ENTITY);
        } catch (Exception e) {
            log.error("Error validating QR code from service with message: {}", e.getMessage());
            return new ResponseEntity<>("", HttpStatus.UNPROCESSABLE_ENTITY);

        }
    }

}
