package com.teknei.bid.controller.rest.unauth;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.teknei.bid.controller.rest.EnrollmentFacialController;
import com.teknei.bid.dto.OperationResult;
import com.teknei.bid.dto.RequestEncFilesDTO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/rest/unauth/enrollment/facial")
@CrossOrigin
public class EnrollmentFacialControllerUnauth {
	private static final Logger log = LoggerFactory.getLogger(EnrollmentFacialControllerUnauth.class);
    @Autowired
    private EnrollmentFacialController controller;

    @ApiOperation(value = "Uploads a picture in async way to the casefile related to the given customer on base64 format. Ciphered endpoint")
    @RequestMapping(value = "/uploadAsyncCiphered", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OperationResult> uploadPlainCiphered(@RequestBody RequestEncFilesDTO requestEncFilesDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{uploadPlainCiphered(POST) }");
        return controller.uploadPlainCiphered(requestEncFilesDTO, request);
    }

    @RequestMapping(value = "/uploadAsync", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OperationResult> uploadPlain(@RequestBody RequestEncFilesDTO requestEncFilesDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{uploadPlain(POST) }");
        return controller.uploadPlain(requestEncFilesDTO, request);
    }


    @Deprecated
    @ApiOperation(value = "Stores the picture of the face for the case file. Expects the attachment and a JSON like {'operationId' : 1}", response = OperationResult.class)
    @RequestMapping(value = "/face", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    public ResponseEntity<OperationResult> addFace(
            @RequestPart(value = "json") String jsonRequest,
            @RequestPart(value = "file") MultipartFile file) {
    	log.info("lblancas: "+this.getClass().getName()+".{addFace(POST) }");
        return controller.addFace(jsonRequest, file);
    }

    @ApiOperation(value = "Uploads a picture in plain mode and stores it in casefile")
    @RequestMapping(value = "/uploadPlain", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OperationResult> uploadPlainOnline(@RequestBody RequestEncFilesDTO requestEncFilesDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{uploadPlainOnline(POST) }");
        return controller.uploadPlainOnline(requestEncFilesDTO, request);
    }

    @ApiOperation(value = "Uploads a picture in plain mode and stores it in casefile")
    @RequestMapping(value = "/uploadPlainCiphered", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OperationResult> uploadPlainOnlineEnc(@RequestBody RequestEncFilesDTO requestEncFilesDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{uploadPlainOnlineEnc(POST) }");
        return controller.uploadPlainOnlineEnc(requestEncFilesDTO, request);
    }
}
