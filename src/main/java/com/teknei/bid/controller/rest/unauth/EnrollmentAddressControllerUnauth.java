package com.teknei.bid.controller.rest.unauth;

import com.teknei.bid.controller.rest.EnrollmentAddressController;
import com.teknei.bid.dto.AddressDetailDTO;
import com.teknei.bid.dto.OperationResult;
import com.teknei.bid.dto.RequestEncFilesDTO;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@RestController
@RequestMapping(value = "/rest/unauth/enrollment/address")
@CrossOrigin
public class EnrollmentAddressControllerUnauth {
	private static final Logger log = LoggerFactory.getLogger(EnrollmentAddressControllerUnauth.class);
    @Autowired
    private EnrollmentAddressController controller;

    @ApiOperation(value = "Finds the detail of the address related record id", notes = "The id provided must be the related one to the customer record", response = AddressDetailDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Address found, response correct"),
            @ApiResponse(code = 404, message = "Address not found"),
            @ApiResponse(code = 500, message = "Error from server")})
    @RequestMapping(value = "/findDetail/{id}", method = RequestMethod.GET)
    public ResponseEntity<AddressDetailDTO> findDetailById(@PathVariable Long id) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findDetailById(GET) }");
        return controller.findDetailById(id);
    }

    @ApiOperation(value = "Updates manually the address retrieved by the customer", response = AddressDetailDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Address found, response correct"),
            @ApiResponse(code = 404, message = "Address not found")
    })
    @RequestMapping(value = "/updateManually/{id}/{type}", method = RequestMethod.POST)
    public ResponseEntity<AddressDetailDTO> updateManually(@RequestBody AddressDetailDTO addressDetailDTO, @PathVariable Long id, @PathVariable Integer type, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{updateManually(POST) }");
    	return controller.updateManually(addressDetailDTO, id, type, request);
    }

    @ApiOperation(value = "Retrieves the information parsed for an uploaded address document", response = String.class)
    @RequestMapping(value = "/address/find/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> findAddressDetail(@PathVariable Long id) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findAddressDetail(GET) }");
        return controller.findAddressDetail(id);
    }

    @Deprecated
    @ApiOperation(value = "Stores and validates the address document for the case file. Expects also a JSON like {'operationId' : 1}", notes = "Parses the address response", response = OperationResult.class)
    @RequestMapping(value = "/comprobanteParsed", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    public ResponseEntity<OperationResult> addComprobantParsed(@RequestPart(value = "json") String jsonRequest,
                                                               @RequestPart(value = "file") MultipartFile file) {
    	//log.info("lblancas: "+this.getClass().getName()+".{addComprobantParsed(POST) }");
        return controller.addComprobantParsed(jsonRequest, file);
    }

    @Deprecated
    @ApiOperation(value = "Stores and validates the address document for the case file. Expects also a JSON like {'operationId' : 1}", response = OperationResult.class)
    @RequestMapping(value = "/comprobante", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    public ResponseEntity<OperationResult> addComprobante(
            @RequestPart(value = "json") String jsonRequest,
            @RequestPart(value = "file") MultipartFile file) {
    	//log.info("lblancas: "+this.getClass().getName()+".{addComprobante(POST) }");
        return controller.addComprobante(jsonRequest, file);
    }

    @ApiOperation(value = "Adds document (ciphered) parses it and stores it in back")
    @RequestMapping(value = "/documentParsed", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OperationResult> addAddressDocumentPlainParsed(@RequestBody RequestEncFilesDTO requestEncFilesDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{addAddressDocumentPlainParsed(POST) }");
        return controller.addAddressDocumentPlainParsed(requestEncFilesDTO, request);
    }

    @ApiOperation(value = "Uploads a document (ciphered) and stores it in back")
    @RequestMapping(value = "/document", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OperationResult> addAddressDocumentPlain(@RequestBody RequestEncFilesDTO dto, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{addAddressDocumentPlain(POST) }");
        return controller.addAddressDocumentPlain(dto, request);
    }
    @ApiOperation(value = "Retrieves the information parsed for an uploaded address document", response = String.class)
    @RequestMapping(value = "/address/find/luis/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> findAddressDetailLuis(@PathVariable Long id) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findAddressDetailLuis(GET) }");
        return controller.findAddressDetail(id);
    }

}
