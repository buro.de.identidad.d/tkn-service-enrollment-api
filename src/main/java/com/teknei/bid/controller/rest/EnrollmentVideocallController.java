package com.teknei.bid.controller.rest;

import com.teknei.bid.dto.DocumentPictureRequestDTO;
import com.teknei.bid.dto.VideocallRequestDTO;
import com.teknei.bid.service.remote.VideocallClient;
import feign.FeignException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/rest/v3/enrollment/videocall")
@CrossOrigin
public class EnrollmentVideocallController {

    private static final Logger log = LoggerFactory.getLogger(EnrollmentVideocallController.class);
    @Autowired
    private VideocallClient videocallClient;

    @RequestMapping(value = "/videocall/add", method = RequestMethod.POST)
    public ResponseEntity<String> addQuobisData(@RequestBody VideocallRequestDTO requestDTO) {
    	//log.info("lblancas: "+this.getClass().getName()+".{addQuobisData() }");
    	log.info("Requesting with: {}", requestDTO);
        String response = null;
        try {
            ResponseEntity<String> responseEntity = videocallClient.addVideocallData(requestDTO);
            log.info("Response: {}", responseEntity.getBody());
            if (response != null && responseEntity.getStatusCode().is2xxSuccessful()) {
                return new ResponseEntity<>("OK", HttpStatus.CREATED);
            }
            return new ResponseEntity<>("Error", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            log.error("Error in addQuobisData for: {} with message: {}", requestDTO, e.getMessage());
            return new ResponseEntity<>("Error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/videocall/quobisStorage", method = RequestMethod.POST)
    public ResponseEntity<byte[]> getImageFromQuobisStorage(@RequestBody DocumentPictureRequestDTO documentPictureRequestDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{getImageFromQuobisStorage() }");
    	log.info("Requeting with: {}", documentPictureRequestDTO);
        try {
            return videocallClient.getImageFromQuobisStorage(documentPictureRequestDTO);
        } catch (FeignException fe) {
            switch (fe.status()) {
                case 404:
                    return new ResponseEntity<>((byte[]) null, HttpStatus.NOT_FOUND);
                case 422:
                    return new ResponseEntity<>((byte[]) null, HttpStatus.UNPROCESSABLE_ENTITY);
                case 500:
                    return new ResponseEntity<>((byte[]) null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
            return new ResponseEntity<>((byte[]) null, HttpStatus.UNPROCESSABLE_ENTITY);
        } catch (Exception e) {
            log.error("Error calling remote service with message: {}", e.getMessage());
            return new ResponseEntity<>((byte[]) null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/videocall/quobisStorageB64", method = RequestMethod.POST)
    public ResponseEntity<String> getImageFromQuobisStorageB64(@RequestBody DocumentPictureRequestDTO documentPictureRequestDTO, HttpServletRequest request) {
        
    	//log.info("lblancas: "+this.getClass().getName()+".{getImageFromQuobisStorageB64() }");
    	log.info("Requeting with: {}", documentPictureRequestDTO);
        try {
            ResponseEntity<byte[]> responseEntity = videocallClient.getImageFromQuobisStorage(documentPictureRequestDTO);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("imageB64", Base64Utils.encodeToString(responseEntity.getBody()));
            return new ResponseEntity<>(jsonObject.toString(), HttpStatus.OK);
        } catch (FeignException fe) {
            switch (fe.status()) {
                case 404:
                    return new ResponseEntity<>((String) null, HttpStatus.NOT_FOUND);
                case 422:
                    return new ResponseEntity<>((String) null, HttpStatus.UNPROCESSABLE_ENTITY);
                case 500:
                    return new ResponseEntity<>((String) null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
            return new ResponseEntity<>((String) null, HttpStatus.UNPROCESSABLE_ENTITY);
        } catch (Exception e) {
            log.error("Error calling remote service with message: {}", e.getMessage());
            return new ResponseEntity<>((String) null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
