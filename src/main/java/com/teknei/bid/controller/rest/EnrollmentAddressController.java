package com.teknei.bid.controller.rest;

import com.teknei.bid.controller.rest.util.crypto.Decrypt;
import com.teknei.bid.controller.rest.util.crypto.TokenUtils;
import com.teknei.bid.dto.AddressDetailDTO;
import com.teknei.bid.dto.OperationResult;
import com.teknei.bid.dto.RequestEncFilesDTO;
import com.teknei.bid.service.remote.AddressClient;
import com.teknei.bid.service.remote.impl.AddressAttachmentClient;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

import static com.teknei.bid.service.validation.JsonValidation.validateJson;

@RestController
@RequestMapping(value = "/rest/v3/enrollment/address")
@CrossOrigin
public class EnrollmentAddressController {


    private static final Logger log = LoggerFactory.getLogger(EnrollmentAddressController.class);
    @Autowired
    private AddressClient addressClient;
    @Autowired
    private AddressAttachmentClient addressAttachmentClient;
    @Autowired
    private TokenUtils tokenUtils;
    @Autowired
    private Decrypt decrypt;

    @ApiOperation(value = "Finds the detail of the address related record id", notes = "The id provided must be the related one to the customer record", response = AddressDetailDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Address found, response correct"),
            @ApiResponse(code = 404, message = "Address not found"),
            @ApiResponse(code = 500, message = "Error from server")})
    @RequestMapping(value = "/findDetail/{id}", method = RequestMethod.GET)
    public ResponseEntity<AddressDetailDTO> findDetailById(@PathVariable Long id) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findDetailById() }");
        return addressClient.findDetailById(id);
    }

    @ApiOperation(value = "Updates manually the address retrieved by the customer", response = AddressDetailDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Address found, response correct"),
            @ApiResponse(code = 404, message = "Address not found")
    })
    @RequestMapping(value = "/updateManually/{id}/{type}", method = RequestMethod.POST)
    public ResponseEntity<AddressDetailDTO> updateManually(@RequestBody AddressDetailDTO addressDetailDTO, @PathVariable Long id, @PathVariable Integer type, HttpServletRequest request) 
    {
    	//log.info("lblancas: "+this.getClass().getName()+".{updateManually() }");
        Map<String, Object> details = tokenUtils.getExtraInfo(request);
        if (details != null) {
            String username = (String) details.get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
            addressDetailDTO.setUsername(username);
        } else {
            addressDetailDTO.setUsername("api");
        }
        return addressClient.updateManually(addressDetailDTO, id, type);
    }

    @ApiOperation(value = "Retrieves the information parsed for an uploaded address document", response = String.class)
    @RequestMapping(value = "/address/find/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> findAddressDetail(@PathVariable Long id) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findAddressDetail() }");
        try {
            return addressClient.findById(id);
        } catch (Exception e) {
            log.error("Error in findAddressDetail for: {} with message: {}", id, e.getMessage());
            return new ResponseEntity<>("Not found", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Adds document (ciphered) parses it and stores it in back")
    @RequestMapping(value = "/documentParsed", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OperationResult> addAddressDocumentPlainParsed(@RequestBody RequestEncFilesDTO requestEncFilesDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{addAddressDocumentPlainParsed() }");
        ResponseEntity<OperationResult> primaryResponse = addAddressDocumentPlain(requestEncFilesDTO, request);
        if (primaryResponse.getStatusCode().is2xxSuccessful()) {
            Long operationId = requestEncFilesDTO.getOperationId();
            ResponseEntity<AddressDetailDTO> secondResponse = null;
            try {
                secondResponse = addressClient.findDetailById(operationId);
                if (secondResponse.getStatusCode().is2xxSuccessful()) {
                    OperationResult primaryOperationResult = primaryResponse.getBody();
                    AddressDetailDTO detailDTO = secondResponse.getBody();
                    JSONObject secondJson = new JSONObject();
                    secondJson.put("zipCode", detailDTO.getZipCode());
                    if(detailDTO.getSuburb() == null || detailDTO.getSuburb().isEmpty()){
                        secondJson.put("suburb", detailDTO.getMunicipio());
                    }else{
                        secondJson.put("suburb", detailDTO.getSuburb());
                    }
                    secondJson.put("street", detailDTO.getStreet());
                    secondJson.put("state", detailDTO.getState());
                    secondJson.put("municipio", detailDTO.getMunicipio());
                    secondJson.put("locality", detailDTO.getLocality());
                    secondJson.put("intNumber", detailDTO.getIntNumber());
                    secondJson.put("extNumber", detailDTO.getExtNumber());
                    secondJson.put("country", detailDTO.getCountry());
                    String outputSecond = secondJson.toString();
                    primaryOperationResult.setErrorMessage(new StringBuilder(primaryOperationResult.getErrorMessage()).append("|").append(outputSecond).toString());
                    return new ResponseEntity<>(primaryOperationResult, HttpStatus.OK);
                } else {
                    log.debug("No detail found for: {}", operationId);
                    throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
                }
            } catch (HttpClientErrorException ie) {
                return buildParsedResponseEmpty(primaryResponse.getBody());
            } catch (Exception e) {
                e.printStackTrace();
                log.error("Error in findDetail for: {} with message:{}", primaryResponse, e.getMessage());
                return buildParsedResponseEmpty(primaryResponse.getBody());
            }
        }
        return primaryResponse;
    }

    @Deprecated
    @ApiOperation(value = "Stores and validates the address document for the case file. Expects also a JSON like {'operationId' : 1}", notes = "Parses the address response", response = OperationResult.class)
    @RequestMapping(value = "/comprobanteParsed", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    public ResponseEntity<OperationResult> addComprobantParsed(@RequestPart(value = "json") String jsonRequest,
                                                               @RequestPart(value = "file") MultipartFile file) {
    	log.info("lblancas: "+this.getClass().getName()+".{addComprobantParsed() }");
        ResponseEntity<OperationResult> primaryResponse = addComprobante(jsonRequest, file);
        if (primaryResponse.getStatusCode().is2xxSuccessful()) 
        {
            JSONObject credentialsOperationRequestJSON = new JSONObject(jsonRequest);
            Long operationId = credentialsOperationRequestJSON.getLong("operationId");
            ResponseEntity<AddressDetailDTO> secondResponse = null;
            try 
            {
                secondResponse = addressClient.findDetailById(operationId);
                if (secondResponse.getStatusCode().is2xxSuccessful()) 
                {
                    OperationResult primaryOperationResult = primaryResponse.getBody();
                    AddressDetailDTO detailDTO = secondResponse.getBody();
                    JSONObject secondJson = new JSONObject();
                    secondJson.put("zipCode", detailDTO.getZipCode());
                    if(detailDTO.getSuburb() == null || detailDTO.getSuburb().isEmpty()){
                        secondJson.put("suburb", detailDTO.getMunicipio());
                    }else{
                        secondJson.put("suburb", detailDTO.getSuburb());
                    }
                    secondJson.put("street", detailDTO.getStreet());
                    secondJson.put("state", detailDTO.getState());
                    secondJson.put("municipio", detailDTO.getMunicipio());
                    secondJson.put("locality", detailDTO.getLocality());
                    secondJson.put("intNumber", detailDTO.getIntNumber());
                    secondJson.put("extNumber", detailDTO.getExtNumber());
                    secondJson.put("country", detailDTO.getCountry());
                    String outputSecond = secondJson.toString();
                    primaryOperationResult.setErrorMessage(new StringBuilder(primaryOperationResult.getErrorMessage()).append("|").append(outputSecond).toString());
                    return new ResponseEntity<>(primaryOperationResult, HttpStatus.OK);
                }
                else 
                {
                    log.debug("No detail found for: {}", operationId);
                    throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
                }
            } catch (HttpClientErrorException ie) {
                return buildParsedResponseEmpty(primaryResponse.getBody());
            } catch (Exception e) {
                log.error("Error in findDetail for: {} with message:{}", primaryResponse, e.getMessage());
                return buildParsedResponseEmpty(primaryResponse.getBody());
            }
        }
        return primaryResponse;
    }

    private ResponseEntity<OperationResult> buildParsedResponseEmpty(OperationResult primaryResponse) {
    	//log.info("lblancas: "+this.getClass().getName()+".{buildParsedResponseEmpty() }");
        JSONObject secondJson = new JSONObject();
        secondJson.put("zipCode", "");
        secondJson.put("suburb", "");
        secondJson.put("street", "");
        secondJson.put("state", "");
        secondJson.put("municipio", "");
        secondJson.put("locality", "");
        secondJson.put("intNumber", "");
        secondJson.put("extNumber", "");
        secondJson.put("country", "");
        String outputSecond = secondJson.toString();
        primaryResponse.setErrorMessage(new StringBuilder(primaryResponse.getErrorMessage()).append("|").append(outputSecond).toString());
        return new ResponseEntity<>(primaryResponse, HttpStatus.OK);
    }


    @ApiOperation(value = "Uploads a document (ciphered) and stores it in back")
    @RequestMapping(value = "/document", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OperationResult> addAddressDocumentPlain(@RequestBody RequestEncFilesDTO dto, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{addAddressDocumentPlain() }");
        OperationResult operationResult = new OperationResult();
        String anverse = null;
        try {
            anverse = decrypt.decrypt(dto.getB64Anverse());
            if (anverse == null) {
                anverse = dto.getB64Anverse();
            }
        } catch (Exception e) {
            log.error("Could not decipher message from source with message: {}", e.getMessage());
            anverse = dto.getB64Anverse();
        }
        dto.setB64Anverse(anverse);
        try {
            String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
            dto.setUsername(username);
            ResponseEntity<String> responseEntity = addressClient.uploadAddressDocument(dto);
            if (responseEntity.getStatusCode().is4xxClientError() || responseEntity.getStatusCode().is5xxServerError()) {
                operationResult.setResultOK(false);
                operationResult.setErrorMessage(responseEntity.getBody());
                return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
            }
            operationResult.setResultOK(true);
            operationResult.setErrorMessage("Se ha almacenado el comprobante de domicilio en la operacion de enrolamiento");
            return new ResponseEntity<>(operationResult, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error adding address document in plain with message: {}", e.getMessage());
            operationResult.setResultOK(false);
            operationResult.setErrorMessage("30001");
            return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @Deprecated
    @ApiOperation(value = "Stores and validates the address document for the case file. Expects also a JSON like {'operationId' : 1}", response = OperationResult.class)
    @RequestMapping(value = "/comprobante", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    public ResponseEntity<OperationResult> addComprobante(
            @RequestPart(value = "json") String jsonRequest,
            @RequestPart(value = "file") MultipartFile file) {
    	log.info("lblancas: "+this.getClass().getName()+".{addComprobante() }");
        OperationResult operationResult = new OperationResult();
        JSONObject credentialsOperationRequestJSON = null;
        if (!validateJson(jsonRequest)) {
            operationResult.setErrorMessage("Bad Request");
            return new ResponseEntity<>(operationResult, HttpStatus.BAD_REQUEST);
        }
        credentialsOperationRequestJSON = new JSONObject(jsonRequest);
        Long operationId = credentialsOperationRequestJSON.getLong("operationId");
        try {
            ResponseEntity<String> responseEntity = addressAttachmentClient.upload(file, operationId);
            if (responseEntity == null) {
                throw new IllegalArgumentException();
            } else if (responseEntity.getStatusCode().is4xxClientError() || responseEntity.getStatusCode().is5xxServerError()) {
                operationResult.setResultOK(false);
                operationResult.setErrorMessage(responseEntity.getBody());
                return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
            }
            operationResult.setErrorMessage("Se ha almacenado el comprobante de domicilio en la operacion de enrolamiento");
            operationResult.setResultOK(true);
            return new ResponseEntity<>(operationResult, HttpStatus.OK);
        } catch (Exception e) {
            operationResult.setResultOK(false);
            operationResult.setErrorMessage("30001");
            return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

}
