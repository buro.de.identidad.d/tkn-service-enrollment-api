package com.teknei.bid.controller.rest.unauth;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import org.jose4j.lang.JoseException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.util.Base64;
import java.util.Date;

import com.machinezoo.sourceafis.FingerprintMatcher;
import com.machinezoo.sourceafis.FingerprintTemplate;
import com.teknei.bid.controller.rest.EnrollmentBiometricIneProcess;
import com.teknei.bid.controller.rest.EnrollmentContractController;
import com.teknei.bid.controller.rest.util.crypto.Decrypt;
import com.teknei.bid.controller.rest.util.crypto.EncryptAesUtil;
import com.teknei.bid.dto.ContratoTmsDTO;
import com.teknei.bid.dto.OperationIdDTO;
import com.teknei.bid.dto.OperationResult;
import com.teknei.bid.dto.RequestEnrollIneVerification;
import com.teknei.bid.service.remote.ContractClient;

import org.apache.commons.io.FileUtils;
import org.jnbis.api.Jnbis;

import io.swagger.annotations.ApiOperation;
@RestController
@RequestMapping(value = "/rest/unauth/enrollment/biometricIne")
@CrossOrigin
public class EnrollmentBiometricIneProcessUnauth {
	private static final Logger log = LoggerFactory.getLogger(EnrollmentBiometricIneProcessUnauth.class);
    @Autowired
    private EnrollmentBiometricIneProcess controller;

    @ApiOperation(value = "Saves temporally the biometric data")
    @RequestMapping(value = "/addSlaps", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> addCaptureData(@RequestBody RequestEnrollIneVerification requestEnrollIneVerification, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{addCaptureData(POST) }");
        return controller.addCaptureData(requestEnrollIneVerification, request);
    }

    @ApiOperation(value = "Verifies the customer against INE-CECOBAN, returns institution response in raw")
    @RequestMapping(value = "/verifyFirst", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> verifyFirst(@RequestBody OperationIdDTO operationIdDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{verifyFirst(POST) }");
        return controller.verifyFirst(operationIdDTO, request);
    }

    @ApiOperation(value = "Confirms enrollment for opened process")
    @RequestMapping(value = "/confirmEnroll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OperationResult> enrollCustomer(@RequestBody OperationIdDTO operationIdDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{enrollCustomer(POST) }");
        return controller.enrollCustomer(operationIdDTO, request);
    }

    @ApiOperation(value = "Confirms enrollment for opened process")
    @RequestMapping(value = "/ineValidate", method = RequestMethod.POST)
    public ResponseEntity<String> ineValidate(@RequestBody String validateIneRequest) {
    	log.info("INFO: "+this.getClass().getName()+".{ineValidate(POST) }");
        return controller.ineValidate(validateIneRequest);
    }
    
    @ApiOperation(value = "LogCurp")
    @RequestMapping(value = "/logCurp", method = RequestMethod.POST)
    public ResponseEntity<String> logCurp(@RequestBody String req) {
    	log.info("INFO: "+this.getClass().getName()+".{logCurp(POST) }");
        return controller.logCurp(req);
    }
    
    //------------------------UTILITY-CLASS---------------------------------->
	@Autowired
	private Decrypt decrypt;
    
	@ApiOperation(value = "verifi ine by xapi")
	@RequestMapping(value = "/validate/fingerDecript", method = RequestMethod.POST)
	public ResponseEntity<String> fingerDecript(@RequestBody String validateIneRequest) {
		log.info("############# fingerDecript->");
		JSONObject json = new JSONObject(validateIneRequest);
		String dec1 = decrypt.decrypt(json.optString("fingerPrint"));	
		log.info("############# fingerDecript->" + dec1);
		return new ResponseEntity<>(dec1, HttpStatus.OK);		
	}
	
	@ApiOperation(value = "verifi ine by xapi")
	@RequestMapping(value = "/validate/fingerEncript", method = RequestMethod.POST)
	public ResponseEntity<String> fingerEncript(@RequestBody String validateIneRequest) {
		log.info("############# fingerEncript->");
		JSONObject json = new JSONObject(validateIneRequest);
		String dec1 = decrypt.encrypt(json.optString("fingerPrint"));	
		log.info("############# fingerEncript->" + dec1);
		return new ResponseEntity<>(dec1, HttpStatus.OK);		
	}
	
	String key = "X2km56dwYhIQ6glIx3C8AyGFVXduqtZF2rRokCYZ3HFV9soiUsXmrdCtsNXCtWG277iL5h+FIFsmJECTbQK3ayL9TxWMWvG0Utc+nwuJeJRykBqYzI1aJUSoZ6HivLHIIeAtooEIo9ayaL7F4CrLPeKnsJ+lmAZrPOlzk3bjpG4iTY+WPOtdciBceB0pegss7HJVh35PF8xykGQwkhJ7gvHC+5CFgjI3nbhKSW7PyEG2XOarWpv1sMqd4cvXrp97BionVtt3GSmSx1uVKepV4qGBZZ9r1qfN99kkPBFYzL7kKiAuzcLifpoJM9BkQI8rmAyT86gzKsFkVnnaFAXxqg==";
	@ApiOperation(value = "verifi ine by xapi")
	@RequestMapping(value = "/validate/bodyEncript", method = RequestMethod.POST)
	public ResponseEntity<String> bodyEncript(@RequestBody String validateIneRequest) {
		log.info("#############bodyEncript---->> ");
		JSONObject json = new JSONObject(validateIneRequest);
		EncryptAesUtil aesUtil = new EncryptAesUtil();		
		String encriptBody = "";
		try {
			encriptBody = aesUtil.encryptJose(json.toString(), key);
		} catch (JoseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.info("#############OuputJson->" + encriptBody);
		return new ResponseEntity<>(encriptBody, HttpStatus.OK);		
	}
	
	@ApiOperation(value = "verifi ine by xapi")
	@RequestMapping(value = "/validate/bodyDecript", method = RequestMethod.POST)
	public ResponseEntity<String> bodyDecript(@RequestBody String validateIneRequest) {
		EncryptAesUtil aesUtil = new EncryptAesUtil();		
		String dec1 = "";
		try {
			dec1 = aesUtil.decryptJose(validateIneRequest, key);
		} catch (JoseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		log.info("#############OuputJson->" + dec1);
		return new ResponseEntity<>(dec1, HttpStatus.OK);		
	}
	
	
	@Autowired
	private ContractClient contractClient;

	@ApiOperation(value = "verifi ine by xapi")
	@RequestMapping(value = "/validate/testContract", method = RequestMethod.POST)
	public ResponseEntity<String> testContract(@RequestBody String request) {
		log.info("############# testContract->");
		JSONObject json = new JSONObject(request);
		log.info("############# testContract->" + json.toString());
//		1.-obtener operacion
//		2.-buscar en el tas el contrato y la minucia 
//		3.-generar contrato 
		
		return contractClient.testContract(request);
	}
    
	
}
