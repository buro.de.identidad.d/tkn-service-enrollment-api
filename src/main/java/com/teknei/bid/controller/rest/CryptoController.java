package com.teknei.bid.controller.rest;


import com.teknei.bid.controller.rest.util.crypto.Decrypt;
import com.teknei.bid.dto.CipherRequestDTO;
import com.teknei.bid.dto.CipherResponseDTO;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/rest/v3/crypto")
@CrossOrigin
public class CryptoController {

    @Autowired
    private Decrypt decrypt;
    private static final Logger log = LoggerFactory.getLogger(CryptoController.class);


    @ApiOperation(value = "Ciphers a string base 64 value", response = CipherResponseDTO.class)
    @RequestMapping(value = "/crypto", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CipherResponseDTO> cipherValue(@RequestBody CipherRequestDTO requestDTO) {
    	//log.info("lblancas: "+this.getClass().getName()+".{cipherValue() }");
        CipherResponseDTO responseDTO = new CipherResponseDTO();
        String source = requestDTO.getPlainText();
        String value = null;
        try {
            value = decrypt.encrypt(source);
            responseDTO.setValue(value);
            return new ResponseEntity<>(responseDTO, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error ciphering plain text with message: {}", e.getMessage());
            responseDTO.setValue("");
            return new ResponseEntity<>(responseDTO, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

}
