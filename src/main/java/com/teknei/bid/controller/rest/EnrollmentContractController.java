package com.teknei.bid.controller.rest;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.teknei.bid.controller.rest.util.crypto.Decrypt;
import com.teknei.bid.controller.rest.util.crypto.TokenUtils;
import com.teknei.bid.dto.*;
import com.teknei.bid.service.remote.BiometricClient;
import com.teknei.bid.service.remote.ContractClient;
import com.teknei.bid.service.remote.impl.ContractAttachmentclient;
import feign.FeignException;
import io.swagger.annotations.ApiOperation;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Base64.Decoder;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@RestController
@RequestMapping(value = "/rest/v3/enrollment/contract")
@CrossOrigin
public class EnrollmentContractController {

    private static final Logger log = LoggerFactory.getLogger(EnrollmentContractController.class);
    @Autowired
    private ContractAttachmentclient contractAttachmentclient;
    @Autowired
    private ContractClient contractClient;
    @Autowired
    private BiometricClient biometricClient;
    @Autowired
    private Decrypt decrypt;
    @Autowired
    private TokenUtils tokenUtils;
    @Autowired
    private EnrollmentCertController certController;

    
    @ApiOperation(value = "Stores the customer credentials related to the account and the contract generation", response = Byte.class)
    @RequestMapping(value = "/credentials", method = RequestMethod.POST)
    public ResponseEntity<Boolean> addCustomerCredentials(@RequestBody CustomerCredentials credentials) {
    	//log.info("lblancas: "+this.getClass().getName()+".{addCustomerCredentials() }");
        return new ResponseEntity<>(Boolean.TRUE, HttpStatus.OK);
    }

   
    
    @ApiOperation(value = "Generates and gets the contract for the current operation", response = byte[].class)
    @RequestMapping(value = "/contrato/{id}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getUnsignedContract(@PathVariable("id") Long id, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{getUnsignedContract() }");
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        try {
            return contractClient.getUnsignedContract(id, username);
        } catch (Exception e) {
            log.error("Error in getUnsignedContract for: {} with message: {}", id, e.getMessage());
            return new ResponseEntity<>((byte[]) null, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
    
     // ************************** SAVE DOCUMENT OF TMS IN TAS ********************************************************
    @ApiOperation(value = "Save document of TMSourcing in TAS", response = String.class)
    @RequestMapping(value = "/saveContratos", method = RequestMethod.POST)
    public ResponseEntity<String> getSaveContractTMS(@RequestBody SaveDocTmsDTO saveDoc, HttpServletRequest request) {
    	
    	log.info("*************************** LBMV save document of TMS in TAS ************************");
    	
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        Long id = saveDoc.getId();
        
        try {
            return contractClient.getSaveContractTMS(id, saveDoc);
            
        } catch (Exception e) {
            e.fillInStackTrace();
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }
        
    }
    
    @ApiOperation(value = "Generates, pre fills and gets the contract for the current operation", response = byte[].class)
    @RequestMapping(value = "/contractPrefilled/{id}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getContractPrefilled(@PathVariable("id") Long id, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{getContractPrefilled() }");
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        try {
            return contractClient.getUnsignedContractWithCerts(id, username);
        } catch (Exception e) {
            log.error("Error in getUnsignedContract for: {} with message: {}", id, e.getMessage());
            return new ResponseEntity<>((byte[]) null, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
 
    @ApiOperation(value = "Sign the contract with the cyphered fingerprint. Verifies the identify of the customer", response = String.class)
    @RequestMapping(value = "/contrato/signCyphered", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> signContractCyphered(@RequestBody SignContractRequestDTO requestDTO, HttpServletRequest request) 
    {
    	log.info("lblancas: "+this.getClass().getName()+".signContractCyphered(1) = "+requestDTO.getOperationId());
    	
    	ResponseEntity<String> primero= signContractById(requestDTO,request); 
    	JSONObject jsonObject = new JSONObject(primero.getBody());
        String respu= jsonObject.getString("status");
        log.info("lblancas: "+this.getClass().getName()+">>>"+respu);
        if(respu.equals("OK"))
        	return primero;
        log.info("lblancas: "+this.getClass().getName()+".{signContractCyphered  segunda vez() }");
        
        String idClieRelacionado= biometricClient.getidClieRelacion(""+ requestDTO.getOperationId()); 
        if(idClieRelacionado!=null)
        {
        	try {
        		Long id=new Long(idClieRelacionado);
        		log.info("lblancas: "+this.getClass().getName()+" Id Relacioando>>>"+id);
        		ResponseEntity<String> segundo= signContractById(requestDTO,id,request);
        		return segundo;
        	}
        	catch(Exception e)
        	{
        		
        	}
        }
        
    	return null;
    }
    public ResponseEntity<String> signContractById(SignContractRequestDTO requestDTO, HttpServletRequest request) 
    {
    	log.info("lblancas: "+this.getClass().getName()+" Id>>>"+requestDTO.getOperationId());
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        String b64Finger = requestDTO.getBase64Finger();
        String clearFinger = decrypt.decrypt(b64Finger);
        JSONObject requestBiom = new JSONObject();
        requestBiom.put("id", String.valueOf(requestDTO.getOperationId()));
        requestBiom.put("ri", clearFinger);
        requestBiom.put("contentType", requestDTO.getContentType());
        try {
        	log.info("lblancas: "+this.getClass().getName()+".{signContractCyphered() }  ---  > busca biometricos  inicia ");
            biometricClient.searchByFingerAndId(requestBiom.toString());
            log.info("lblancas: "+this.getClass().getName()+".{signContractCyphered() }  ---  > busca biometricos  Termina");

            String hash = "{\"hash\": \"temphash\"}";
            try 
            {
                OperationIdDTO operationIdDTO = new OperationIdDTO();
                operationIdDTO.setOperationId(requestDTO.getOperationId()); 
                contractClient.getUnsignedContractWithCerts(requestDTO.getOperationId(), username); 
                log.info("lblancas: "+this.getClass().getName()+" Contrato sin firma" );
            } catch (Exception e) {
                log.error("Error generating contract with cert details: {}", e.getMessage());
                return new ResponseEntity<>((String) null, HttpStatus.FORBIDDEN);
            }
            ResponseEntity<String> responseHash = biometricClient.getHashForCustomer(String.valueOf(requestDTO.getOperationId()));
            hash = responseHash.getBody();
            log.info("lblancas: "+this.getClass().getName()+" Obtiene Hash" );

            JSONObject jsonObject = new JSONObject(hash);
            requestDTO.setHash(jsonObject.getString("hash"));
            requestDTO.setBase64Finger(clearFinger);
            requestDTO.setUsername(username);
            contractClient.signContract(requestDTO);
            log.info("lblancas: "+this.getClass().getName()+" Se firma Contrato" );
            JSONObject okResponse = new JSONObject();
            okResponse.put("status", "OK");
            return new ResponseEntity<>(okResponse.toString(), HttpStatus.OK);
        }
        catch (FeignException e) 
        {
            JSONObject jsonError = new JSONObject();
            switch (e.status()) 
            {
                case 404:
                    jsonError.put("status", "404");
                    return new ResponseEntity<>(jsonError.toString(), HttpStatus.NOT_FOUND);
                case 422:
                    jsonError.put("status", "422 - unable to generate contract");
                    return new ResponseEntity<>(jsonError.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
                default:
                    jsonError.put("status", "500 - error not recognized");
                    log.error("Error in contract sign with message: {}", e.getMessage());
                    return new ResponseEntity<>(jsonError.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            log.error("Error in contract sign with message: {}", e.getMessage());
            JSONObject jsonError = new JSONObject();
            jsonError.put("status", "500");
            return new ResponseEntity<>(jsonError.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    public ResponseEntity<String> signContractById(SignContractRequestDTO requestDTO,long idRelacionado, HttpServletRequest request) 
    {
    	log.info("lblancas: signContractById "+requestDTO);
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        String b64Finger = requestDTO.getBase64Finger();
        String clearFinger = decrypt.decrypt(b64Finger);
        JSONObject requestBiom = new JSONObject();
        requestBiom.put("id", String.valueOf(idRelacionado));
        requestBiom.put("ri", clearFinger);
        requestBiom.put("contentType", requestDTO.getContentType());
        try {
        	log.info("lblancas: "+this.getClass().getName()+".{signContractCyphered() }  ---  > busca biometricos  inicia ");
            biometricClient.searchByFingerAndId(requestBiom.toString());
            log.info("lblancas: "+this.getClass().getName()+".{signContractCyphered() }  ---  > busca biometricos  Termina");

            String hash = "{\"hash\": \"temphash\"}";
            try 
            {
                OperationIdDTO operationIdDTO = new OperationIdDTO();
                operationIdDTO.setOperationId(requestDTO.getOperationId()); 
                log.info("lblancas: "+this.getClass().getName()+" Contrato in firma  2>>"+requestDTO.getOperationId() );	
                contractClient.getUnsignedContractWithCerts(requestDTO.getOperationId(), username); 
                
            } catch (Exception e) {
                log.error("Error generating contract with cert details: {}", e.getMessage());
                return new ResponseEntity<>((String) null, HttpStatus.FORBIDDEN);
            }
            ResponseEntity<String> responseHash = biometricClient.getHashForCustomer(String.valueOf(idRelacionado));
            log.info("lblancas: "+this.getClass().getName()+" Id operation rel HASH " +idRelacionado );
            hash = responseHash.getBody();
            log.info("lblancas: "+this.getClass().getName()+" Obtiene Hash 2" );

            JSONObject jsonObject = new JSONObject(hash);
            requestDTO.setHash(jsonObject.getString("hash"));
            requestDTO.setBase64Finger(clearFinger);
            requestDTO.setUsername(username);
            //requestDTO.setOperationId(idRelacionado);
            log.info("lblancas: "+this.getClass().getName()+" Id operation " +requestDTO.getOperationId() );
            log.info("lblancas: "+this.getClass().getName()+" Id operation rel " +idRelacionado );
            contractClient.signContract(requestDTO);
            log.info("lblancas: "+this.getClass().getName()+" Se firma Contrato 2" );
            JSONObject okResponse = new JSONObject();
            okResponse.put("status", "OK");
            return new ResponseEntity<>(okResponse.toString(), HttpStatus.OK);
        }
        catch (FeignException e) 
        {
            JSONObject jsonError = new JSONObject();
            switch (e.status()) 
            {
                case 404:
                    jsonError.put("status", "404");
                    return new ResponseEntity<>(jsonError.toString(), HttpStatus.NOT_FOUND);
                case 422:
                    jsonError.put("status", "422 - unable to generate contract");
                    return new ResponseEntity<>(jsonError.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
                default:
                    jsonError.put("status", "500 - error not recognized");
                    log.error("Error in contract sign with message: {}", e.getMessage());
                    return new ResponseEntity<>(jsonError.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            log.error("Error in contract sign with message: {}", e.getMessage());
            JSONObject jsonError = new JSONObject();
            jsonError.put("status", "500");
            return new ResponseEntity<>(jsonError.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }


    @ApiOperation(value = "Sign the contract with the cyphered fingerprint. Verifies the identify of the customer", response = String.class)
    @RequestMapping(value = "/contrato/signCypheredjpg", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> signContractCypheredjpg(@RequestBody SignContractRequestDTO requestDTO, HttpServletRequest request) {
    	log.info("lblancas: "+this.getClass().getName()+".{signContractCypheredJPG() }");
        try 
        {
            contractClient.signContract(requestDTO);
            JSONObject okResponse = new JSONObject();
            okResponse.put("status", "OK");
            return new ResponseEntity<>(okResponse.toString(), HttpStatus.OK);
        } catch (FeignException e) {
            JSONObject jsonError = new JSONObject();
            switch (e.status()) {
                case 404:
                    jsonError.put("status", "404");
                    return new ResponseEntity<>(jsonError.toString(), HttpStatus.NOT_FOUND);
                case 422:
                    jsonError.put("status", "422 - unable to generate contract");
                    return new ResponseEntity<>(jsonError.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
                default:
                    jsonError.put("status", "500 - error not recognized");
                    log.error("Error in contract sign with message: {}", e.getMessage());
                    return new ResponseEntity<>(jsonError.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            log.error("Error in contract sign with message: {}", e.getMessage());
            JSONObject jsonError = new JSONObject();
            jsonError.put("status", "500");
            return new ResponseEntity<>(jsonError.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
    
    @ApiOperation(value = "Sign the contract with the cyphered fingerprint. Verifies the identify of the customer", response = String.class)
    @RequestMapping(value = "/contrato/signCypheredLABB", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<byte[]> signContractCypheredLABB(@RequestBody Long  id, String  username) {
    	//log.info("lblancas: "+this.getClass().getName()+".{signContractCyphered() }");
        return contractClient.getUnsignedContractWithCerts(id, username);
    }


    @Deprecated
    @ApiOperation(value = "Sign the contract with the fingerprint. Verifies the identify of the customer", response = String.class)
    @RequestMapping(value = "/contrato/sign", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> signContract(@RequestBody SignContractRequestDTO requestDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{signContract() }");
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        String b64Finger = requestDTO.getBase64Finger();
        JSONObject requestBiom = new JSONObject();
        requestBiom.put("id", String.valueOf(requestDTO.getOperationId()));
        requestBiom.put("ri", b64Finger);
        requestBiom.put("contentType", requestDTO.getContentType());
        try {
            biometricClient.searchByFingerAndId(requestBiom.toString());
            try {
                OperationIdDTO operationIdDTO = new OperationIdDTO();
                operationIdDTO.setOperationId(requestDTO.getOperationId());
                ResponseEntity<String> responseEntity = certController.generateCert(operationIdDTO, request);
                if (responseEntity.getStatusCode().is2xxSuccessful() || responseEntity.getStatusCodeValue() == 409) {
                    //return contractController.getUnsignedContract(idCustomer, request);
                    contractClient.getUnsignedContractWithCerts(requestDTO.getOperationId(), username);
                } else {
                    return new ResponseEntity<>((String) null, HttpStatus.FORBIDDEN);
                }
            } catch (Exception e) {
                log.error("Error generating contract with cert details: {}", e.getMessage());
            }
            requestDTO.setUsername(username);
            ResponseEntity<String> responseHash = biometricClient.getHashForCustomer(String.valueOf(requestDTO.getOperationId()));
            String hash = responseHash.getBody();
            JSONObject jsonObject = new JSONObject(hash);
            requestDTO.setHash(jsonObject.getString("hash"));
            contractClient.signContract(requestDTO);
            JSONObject okResponse = new JSONObject();
            okResponse.put("status", "OK");
            return new ResponseEntity<>(okResponse.toString(), HttpStatus.OK);
        } catch (FeignException e) {
            JSONObject jsonError = new JSONObject();
            switch (e.status()) {
                case 404:
                    jsonError.put("status", "404");
                    return new ResponseEntity<>(jsonError.toString(), HttpStatus.NOT_FOUND);
                case 422:
                    jsonError.put("status", "422 - unable to generate contract");
                    return new ResponseEntity<>(jsonError.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
                default:
                    jsonError.put("status", "500 - error not recognized");
                    log.error("Error in contract sign with message: {}", e.getMessage());
                    return new ResponseEntity<>(jsonError.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            log.error("Error in contract sign with message: {}", e.getMessage());
            JSONObject jsonError = new JSONObject();
            jsonError.put("status", "500");
            return new ResponseEntity<>(jsonError.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Generates and gets the contract for the current operation", response = String.class)
    @RequestMapping(value = "/contratoB64/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> getUnsignedContractB64(@PathVariable("id") Long id, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{getUnsignedContractB64() }");
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        try {
            return contractClient.getUnsignedContractB64(id, username);
        } catch (Exception e) {
            log.error("Error in getUnsignedContract for: {} with message: {}", id, e.getMessage());
            return new ResponseEntity<>((String) null, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
    

    @ApiOperation(value = "Finds the values that must be accepted by the customer", response = AcceptancePerCustomerDTO.class)
    @RequestMapping(value = "/contrato/contratoDemo", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getContractDemo(@RequestBody ContractDemoDTO dto,HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{getContractDemo() } "+dto.toString());
        return contractClient.getDemo(dto);
    }
 
    @RequestMapping(value = "/ResumInmuebles", method = RequestMethod.POST)
    public ResponseEntity<byte[]> getResumInmuebles(@RequestBody TMSDocumentosDTO dto){
    	
    	log.info(" ========= PDF RESUMEN(INMUEBLES) de TMSourcing =========== ");
    	ResponseEntity<byte[] > entityB = null;
    	
    	try {
    		
    	ResponseEntity<String> respEntity = contractClient.getPDFInmuebles(dto);
    		
    	String resultado = respEntity.getBody(); 
	   	Decoder b64 = Base64.getDecoder();
	   	byte[] res = b64.decode(resultado); 
	   	
	   	entityB = new ResponseEntity<>(res, HttpStatus.OK);
	   	
    	}catch(Exception e) {
    		
    		e.fillInStackTrace();
    		
    		}
    	
	   	 return entityB;
    }
    
    @RequestMapping(value = "/ResumRecursos", method = RequestMethod.POST)
    public ResponseEntity<byte[]> getResumRecursos(@RequestBody TMSDocumentosDTO dto){
    	
    	log.info(" ========== PDF RESUMEN(RECURSOS) de TMSourcing =============== ");
    	
    	ResponseEntity<byte[] > entityB = null;
    	
    	try {
    		
    	ResponseEntity<String> respEntity = contractClient.getPDFRecursos(dto);
    	
    	String resultado = respEntity.getBody(); 
	   	 Decoder b64 = Base64.getDecoder();
	   	 byte[] res = b64.decode(resultado); 
	   	//log.info(" ======----- respuesta en byte -------======" +res);
	   	entityB = new ResponseEntity<>(res, HttpStatus.OK);
	   	
    	}catch(Exception e) {
    		
    		e.fillInStackTrace();
    			
    		}
    	
	   	 return entityB;
    		
    }
    
    @ApiOperation(value = "Finds the values that must be accepted by the customer", response = AcceptancePerCustomerDTO.class)
    @RequestMapping(value = "/contrato/TMSFideicomiso", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<byte[]> getContracTMSFideicomiso(@RequestBody TMSFideicomisoDTO dto) {
    	
    	log.info("lbmv:[2]: "+this.getClass().getName()+".{getContracTMSFideicomiso() } "+dto.toString());
    	
    	ResponseEntity<String> entity = contractClient.getContracTMSFideicomiso(dto);
    	
    	String resultado = entity.getBody(); 
	   	 Decoder b64 = Base64.getDecoder();
	   	 byte[] res = b64.decode(resultado); 
	   	 ResponseEntity<byte[] > entityB = new ResponseEntity<>(res, HttpStatus.OK);
	  	 return entityB;   	 
    }
    
    @ApiOperation(value = "Finds the values that must be accepted by the customer", response = AcceptancePerCustomerDTO.class)
    @RequestMapping(value = "/contrato/TMS/Fideicomiso", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<byte[]> getContracTMSFideicomiso2(@RequestBody String dto) {
		log.info("lbmv:[2]: " + this.getClass().getName() + ".{getContracTMSFideicomiso2() } " + dto.toString());
		ResponseEntity<byte[]> entity = contractClient.getContracTMSFideicomiso2(dto);		
//		Decoder b64 = Base64.getDecoder();
//		byte[] res = b64.decode(entity.getBody());
//		ResponseEntity<byte[]> entityB = new ResponseEntity<>(res, HttpStatus.OK);
		return entity;
	}    
    
    @ApiOperation(value = "Finds the values that must be accepted by the customer", response = AcceptancePerCustomerDTO.class)
    @RequestMapping(value = "/contrato/contratoTms", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    
    public ResponseEntity<byte[]> getContratoTms(@RequestBody ContratoTmsDTO dto) {
    	
    	log.info("lbmv:[2]: "+this.getClass().getName()+".{getContratoTms() } "+dto.toString());
    	
    	ResponseEntity<String> entity = contractClient.getContratoTms(dto);
    	
    	String resultado = entity.getBody(); 
	   	 Decoder b64 = Base64.getDecoder();
	   	 byte[] res = b64.decode(resultado); 
	   	 ResponseEntity<byte[] > entityB = new ResponseEntity<>(res, HttpStatus.OK);
	   	 
	   	 
	   	 return entityB;
	   	 
    }

    @Deprecated
    @ApiOperation(value = "Adds the signed contract to the case file. It expects the file as attachment and the operationId in the URL", response = OperationResult.class)
    @RequestMapping(value = "/contrato/add/{id}", method = RequestMethod.POST)
    public ResponseEntity<OperationResult> addSignedContract(@RequestPart(value = "file") MultipartFile file, @PathVariable("id") Long id) {
    	//log.info("lblancas: "+this.getClass().getName()+".{addSignedContract() }");
        OperationResult operationResult = new OperationResult();
        try {
            ResponseEntity<String> responseEntity = contractAttachmentclient.addSignedContract(file, id);
            if (responseEntity == null) {
                throw new IllegalArgumentException();
            } else if (responseEntity.getStatusCode().is4xxClientError() || responseEntity.getStatusCode().is5xxServerError()) {
                operationResult.setErrorMessage(responseEntity.getBody());
                operationResult.setResultOK(false);
                return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
            }
            operationResult.setResultOK(true);
            operationResult.setErrorMessage(responseEntity.getBody());
            return new ResponseEntity<>(operationResult, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error in addSignedContract for: {} with message: {}", id, e.getMessage());
            operationResult.setErrorMessage("ERROR");
            operationResult.setResultOK(false);
            return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Uploads contract signed and stores in the document manager", notes = "The content should be ciphered", response = OperationResult.class)
    @RequestMapping(value = "/uploadPlain", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OperationResult> addPlainSignedContract(@RequestBody ContractSignedDTO contractSignedDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{addPlainSignedContract() }");
    	
        String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
        String contract = null;
        try {
            String content = decrypt.decrypt(contractSignedDTO.getFileB64());
            if (content == null) {
                contract = contractSignedDTO.getFileB64();
            } else {
                contract = content;
            }
        } catch (Exception e) {
            contract = contractSignedDTO.getFileB64();
            log.error("Error deciphering content, using plain: {}", e.getMessage());
        }
        contractSignedDTO.setFileB64(contract);
        contractSignedDTO.setUsername(username);
        OperationResult operationResult = new OperationResult();
        try {
            ResponseEntity<String> responseEntity = contractClient.addPlainSignedContract(contractSignedDTO);
            operationResult.setResultOK(true);
            operationResult.setErrorMessage(responseEntity.getBody());
            return new ResponseEntity<>(operationResult, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error in addSignedContract for: {} with message: {}", contractSignedDTO.getOperationId(), e.getMessage());
            operationResult.setErrorMessage("ERROR");
            operationResult.setResultOK(false);
            return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
    //TODO Aqui esta el pedo de los contratos
    @ApiOperation(value = "Finds the values that must be accepted by the customer", response = AcceptancePerCustomerDTO.class)
    @RequestMapping(value = "/acceptancePerCustomer", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<AcceptancePerCustomerDTO>> getAcceptancesPerCustomer(@RequestBody OperationIdDTO operationIdDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{getAcceptancesPerCustomer() }");
        try {
            return contractClient.getAcceptancesPerCustomer(operationIdDTO);
        } catch (Exception e) {
            log.info("Error finding acceptances per customer for: {} with message: {}", operationIdDTO, e.getMessage());
            return new ResponseEntity<>((List<AcceptancePerCustomerDTO>) null, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Updates the acceptances of the customer", response = String.class)
    @RequestMapping(value = "/acceptancePerCustomer", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> updateAcceptancePerCustomer(@RequestBody List<AcceptancePerCustomerDTO> dtoList, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{updateAcceptancePerCustomer() }");
        try {
            String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
            dtoList.forEach(c -> c.setUsernameRequesting(username));
            return contractClient.updateAcceptancePerCustomer(dtoList);
        } catch (Exception e) {
            log.error("Error updating acceptances per customer with message: {}", e.getMessage());
            return new ResponseEntity<>((String) null, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
    
    
    //----------------------------
    
    @ApiOperation(value = "Finds the values that must be accepted by the customer", response = AcceptancePerCustomerDTO.class)
    @RequestMapping(value = "/manualSiginService",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> getContratoManual(@PathVariable("id") Long id) {
		log.info("INFO : " + this.getClass().getName() + "manualSiginService ");
    	//Generando de documento.
	    ResponseEntity<String> res = contractClient.getContratoManual(id);
    	log.info("EstatusCOde: "+res.getStatusCode());
    	JSONObject jres = new JSONObject(res.getBody());
    	log.info("response: "+jres.toString());    
    	// regresar b64
    	
		return new ResponseEntity<>(jres.toString(), HttpStatus.OK);	
	}    

}
