package com.teknei.bid.controller.rest.unauth;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.teknei.bid.controller.rest.EnrollmentVideocallController;
import com.teknei.bid.dto.DocumentPictureRequestDTO;
import com.teknei.bid.dto.VideocallRequestDTO;
import com.teknei.bid.service.remote.VideocallClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/rest/unauth/enrollment/videocall")
@CrossOrigin
public class EnrollmentVideocallControllerUnauth {
	private static final Logger log = LoggerFactory.getLogger(EnrollmentVideocallControllerUnauth.class);
    @Autowired
    private EnrollmentVideocallController controller;

    @RequestMapping(value = "/videocall/add", method = RequestMethod.POST)
    public ResponseEntity<String> addQuobisData(@RequestBody VideocallRequestDTO requestDTO) {
    	//log.info("lblancas: "+this.getClass().getName()+".{addQuobisData(POST) }");
        return controller.addQuobisData(requestDTO);
    }

    @RequestMapping(value = "/videocall/quobisStorage", method = RequestMethod.POST)
    public ResponseEntity<byte[]> getImageFromQuobisStorage(@RequestBody DocumentPictureRequestDTO documentPictureRequestDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{getImageFromQuobisStorage(POST) }");
        return controller.getImageFromQuobisStorage(documentPictureRequestDTO, request);
    }

    @RequestMapping(value = "/videocall/quobisStorageB64", method = RequestMethod.POST)
    public ResponseEntity<String> getImageFromQuobisStorageB64(@RequestBody DocumentPictureRequestDTO documentPictureRequestDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{getImageFromQuobisStorageB64(POST) }");
        return controller.getImageFromQuobisStorageB64(documentPictureRequestDTO, request);
    }
}
