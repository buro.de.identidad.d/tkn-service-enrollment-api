package com.teknei.bid.controller.rest;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.teknei.bid.controller.rest.util.crypto.Decrypt;
import com.teknei.bid.controller.rest.util.crypto.TokenUtils;
import com.teknei.bid.dto.ClientDetailDTO;
import com.teknei.bid.dto.OperationIdDTO;
import com.teknei.bid.dto.OperationResult;
import com.teknei.bid.dto.PersonDataIneTKNRequestService;
import com.teknei.bid.dto.RequestEnrollIneVerification;
import com.teknei.bid.service.remote.BiometricClient;
import com.teknei.bid.service.remote.CustomerClient;
import com.teknei.bid.service.remote.IdentificationClient;
import com.teknei.bid.util.EnrollmentVerificationSingleton;
import com.teknei.bid.util.LogUtil;

import feign.FeignException;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/rest/v3/enrollment/biometricIne")
@CrossOrigin
public class EnrollmentBiometricIneProcess {

	@Autowired
	private IdentificationClient identificationClient;
	@Autowired
	private TokenUtils tokenUtils;
	@Autowired
	private Decrypt decrypt;
	@Autowired
	private BiometricClient biometricClient;
	@Autowired
	private CustomerClient customerClient;

	private static final Logger log = LoggerFactory.getLogger(EnrollmentBiometricIneProcess.class);

	@ApiOperation(value = "Saves temporally the biometric data")
	@RequestMapping(value = "/addSlaps", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<String> addCaptureData(@RequestBody RequestEnrollIneVerification requestEnrollIneVerification,
			HttpServletRequest request) {
		// log.info("lblancas: "+this.getClass().getName()+".{addCaptureData() }");
		EnrollmentVerificationSingleton.getInstance().addCustomerData(requestEnrollIneVerification.getOperationId(),
				requestEnrollIneVerification);
		return new ResponseEntity<>("OK", HttpStatus.OK);
	}

	@ApiOperation(value = "Verifies the customer against INE-CECOBAN, returns institution response in raw")
	@RequestMapping(value = "/verifyFirst", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<String> verifyFirst(@RequestBody OperationIdDTO operationIdDTO, HttpServletRequest request) {
		// log.info("lblancas: "+this.getClass().getName()+".{verifyFirst() }");
		RequestEnrollIneVerification requestEnrollIneVerification = EnrollmentVerificationSingleton.getInstance()
				.getCustomerData(operationIdDTO.getOperationId());
		if (requestEnrollIneVerification == null) {
			return new ResponseEntity<>("NOT FOUND", HttpStatus.NOT_FOUND);
		}
		String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
		PersonDataIneTKNRequestService service = new PersonDataIneTKNRequestService();
		service.setId(requestEnrollIneVerification.getOperationId());
		String leftIndexClear = decrypt.decrypt(requestEnrollIneVerification.getLeftIndexB64());
		String rightIndexClear = decrypt.decrypt(requestEnrollIneVerification.getRightIndexB64());
		String leftSlapClear = decrypt.decrypt(requestEnrollIneVerification.getSlapLeftB64());
		String rightSlapClear = decrypt.decrypt(requestEnrollIneVerification.getSlapRightB64());
		String thumbSlapClear = decrypt.decrypt(requestEnrollIneVerification.getSlapThumbsB64());
		service.setRightIndexB64(rightIndexClear);
		service.setLeftIndexB64(leftIndexClear);
		JSONObject jsonRequest = new JSONObject();
		jsonRequest.put("id", requestEnrollIneVerification.getOperationId());
		jsonRequest.put("sl", leftSlapClear);
		jsonRequest.put("ls", leftSlapClear);
		jsonRequest.put("sr", rightSlapClear);
		jsonRequest.put("rs", rightSlapClear);
		jsonRequest.put("st", thumbSlapClear);
		jsonRequest.put("ts", thumbSlapClear);
		try {
			ResponseEntity<String> responseBiometric = biometricClient.searchBySlaps(jsonRequest.toString());
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("error", "NO NEW CUSTOMER");
			return new ResponseEntity<>(jsonObject.toString(), HttpStatus.FORBIDDEN);
		} catch (FeignException e) {
			if (e.status() == 404) {
				// Not found in biometric system, so its ok to continue
				log.info("Customer in memory for id: {}", operationIdDTO.getOperationId());
				service.setUsername(username);
				// return identificationClient.verifyAgainstINE(service);//Commented by timeout
				// in provider
				return fakeCecoban();
			} else {
				log.error("Error calling biometric client with error: {} and message. {}", e.status(), e.getMessage());
			}
		} catch (Exception e) {
			log.error("Error calling biometric client with message: {}", e.getMessage());
			return new ResponseEntity<>("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>("ABNORMAL PROCESS END", HttpStatus.UNPROCESSABLE_ENTITY);
	}

	@ApiOperation(value = "Confirms enrollment for opened process")
	@RequestMapping(value = "/confirmEnroll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<OperationResult> enrollCustomer(@RequestBody OperationIdDTO operationIdDTO,
			HttpServletRequest request) {
		// log.info("lblancas: "+this.getClass().getName()+".{enrollCustomer() }");
		String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
		OperationResult result = new OperationResult();
		JSONObject jsonObjectRequest = new JSONObject();
		jsonObjectRequest.put("operationId", operationIdDTO.getOperationId());
		String documentId = null;
		String scanId = null;
		RequestEnrollIneVerification requestEnrollIneVerification = EnrollmentVerificationSingleton.getInstance()
				.getCustomerData(operationIdDTO.getOperationId());
		try {
			log.info("Enrolling for : {}", requestEnrollIneVerification.getOperationId());
		} catch (Exception e) {
			log.error("Error logging: {}", e.getMessage());
		}
		if (requestEnrollIneVerification == null) {
			result.setErrorMessage("NOT FOUND");
			result.setResultOK(false);
			return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
		}
		try {
			ResponseEntity<ClientDetailDTO> responseEntity = customerClient
					.findDetailId(operationIdDTO.getOperationId());
			documentId = responseEntity.getBody().getDocumentId();
			scanId = responseEntity.getBody().getScanId();
		} catch (Exception e) {
			log.error("Error finding information for customer, pre requisite is exists in db, message: {}",
					e.getMessage());
			result.setResultOK(false);
			result.setErrorMessage("NO CUSTOMER REGISTERED");
			return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
		}
		String rs = decrypt.decrypt(requestEnrollIneVerification.getSlapRightB64());
		String ls = decrypt.decrypt(requestEnrollIneVerification.getSlapLeftB64());
		String ts = decrypt.decrypt(requestEnrollIneVerification.getSlapThumbsB64());
		jsonObjectRequest.put("scanId", scanId);
		jsonObjectRequest.put("documentId", documentId);
		jsonObjectRequest.put("rs", rs);
		jsonObjectRequest.put("sr", rs);
		jsonObjectRequest.put("ls", ls);
		jsonObjectRequest.put("sl", ls);
		jsonObjectRequest.put("ts", ts);
		jsonObjectRequest.put("st", ts);
		jsonObjectRequest.put("username", username);
		try {
			ResponseEntity<String> responseEntity = biometricClient.addMinucias(jsonObjectRequest.toString(),
					operationIdDTO.getOperationId(), 2);
		} catch (FeignException fe) {
			ResponseEntity<OperationResult> resultResponseEntity = parseFeign(fe);
			if (resultResponseEntity.getBody().getErrorMessage().equals("40003")) {
				ResponseEntity<String> responseEntity = null;
				try {
					responseEntity = biometricClient.searchBySlaps(operationIdDTO.getOperationId().toString());
					String bodyString = responseEntity.getBody();
					JSONObject jsonObject = new JSONObject(bodyString);
					String id = jsonObject.getString("id");
					OperationResult operationResult1 = new OperationResult();
					operationResult1.setResultOK(false);
					JSONObject operationJsonResult = new JSONObject();
					operationJsonResult.put("id", Long.valueOf(id));
					operationJsonResult.put("status", "40003");
					operationResult1.setErrorMessage(operationJsonResult.toString());
					return new ResponseEntity<>(operationResult1, HttpStatus.UNPROCESSABLE_ENTITY);
				} catch (Exception e) {
					log.error("Duplicated record, unable to find related one with message: {}", e.getMessage());
					return resultResponseEntity;
				}
			} else {
				return resultResponseEntity;
			}
		} catch (Exception e) {
			log.error("Error in addMinuciasSlaps for: {} - {} with message: {}", operationIdDTO.getOperationId(), 2,
					e.getMessage());
			result.setResultOK(false);
			result.setErrorMessage("40002");
			return new ResponseEntity<>(result, HttpStatus.UNPROCESSABLE_ENTITY);
		}
		result.setErrorMessage("OK");
		result.setResultOK(true);
		EnrollmentVerificationSingleton.getInstance().removeData(operationIdDTO.getOperationId());
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	private ResponseEntity<OperationResult> parseFeign(FeignException fe) {
		// log.info("lblancas: "+this.getClass().getName()+".{parseFeign() }");
		OperationResult operationResult = new OperationResult();
		String errorTrace = fe.getMessage();
		String[] messageTrace = errorTrace.split("content:");
		if (messageTrace.length > 1) {
			String errorCode = messageTrace[messageTrace.length - 1];
			errorCode = errorCode.trim();
			try {
				Integer.parseInt(errorCode);
				operationResult.setResultOK(false);
				operationResult.setErrorMessage(errorCode);
				return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
			} catch (NumberFormatException ne) {
				log.error("No valid return error code from remote service for: {}", errorCode);
			}
		}
		operationResult.setResultOK(false);
		operationResult.setErrorMessage("40002");
		return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
	}

	private ResponseEntity<String> fakeCecoban() {
		// log.info("lblancas: "+this.getClass().getName()+".{fakeCecoban() }");
		JSONObject responseInner = new JSONObject();
		JSONObject minutiaeResponse = new JSONObject();
		minutiaeResponse.put("similitud7", "75");
		minutiaeResponse.put("similitud2", "75");
		responseInner.put("fechaHoraPeticion", LocalDateTime.now().format(DateTimeFormatter.BASIC_ISO_DATE));
		responseInner.put("peticionId", UUID.randomUUID().toString());
		responseInner.put("tiempoProcesamiento", "1000");
		responseInner.put("indiceSolicitud", "300");
		responseInner.put("minutiaeResponse", minutiaeResponse);

		JSONObject response = new JSONObject();
		response.put("response", responseInner);

		JSONObject respCompInner = new JSONObject();
		respCompInner.put("anioRegistro", "1990");
		respCompInner.put("claveElector", "NA");
		respCompInner.put("numeroEmisionCredencial", "123");
		respCompInner.put("curp", "AACJ881203HMCMRR08");
		respCompInner.put("apellidoPaterno", "AMARO");
		respCompInner.put("apellidoMaterno", "CORIA");
		respCompInner.put("ocr", "NA");
		JSONObject respComp = new JSONObject();

		JSONObject respRegisInner = new JSONObject();
		respRegisInner.put("tipoSituacionRegistral", "NA");

		JSONObject respRegis = new JSONObject();
		respRegis.put("respuestaSituacionRegistral", respRegisInner);

		JSONObject dataRespInner = new JSONObject();
		dataRespInner.put("respuestaComparacion", respCompInner);
		dataRespInner.put("respuestaSituacionRegistral", respRegis);

		response.put("dataResponse", dataRespInner);

		return new ResponseEntity<>(response.toString(), HttpStatus.OK);
	}

//    -----------------------------------------------------------------------------

	@ApiOperation(value = "verifi ine by xapi")
	@RequestMapping(value = "/validate/ine", method = RequestMethod.POST)
	public ResponseEntity<String> ineValidate(@RequestBody String validateIneRequest) {

		log.info(" INICIA : >>>> ineValidate (/validate/ine)");
		JSONObject json = new JSONObject(validateIneRequest);
		log.info("ineValidate"+LogUtil.logJsonObject(json));
		JSONObject other = json.getJSONObject("others");
		JSONArray biometrics = other.getJSONArray("biometrics");
		biometrics.getJSONObject(0).put("type", "WSQ");	
		log.info(json.optString("noEncript"));
		
		// desencriptando minucias para validacion ine ..
		if (json.optString("noEncript").isEmpty() || Boolean.parseBoolean(json.optString("noEncript"))  == false) {
			JSONArray validateIneArr = new JSONArray();
			JSONObject validateIneObj = new JSONObject();
			log.info("Decript for");
			for (int i=0; i<biometrics.length(); i++) {
				log.info(biometrics.getJSONObject(i).optString("fingerNumber"));		
					
				if (biometrics.getJSONObject(i).optString("fingerNumber").equals("2")) {
						validateIneObj = new JSONObject();
					log.info("decrypt---2");
					validateIneObj.put("type", "WSQ");
					validateIneObj.put("fingerPrint", decrypt.
							decrypt(biometrics.getJSONObject(i).optString("fingerPrint")));
					validateIneObj.put("fingerNumber",2);
					validateIneArr.put(validateIneObj);
				} else if (biometrics.getJSONObject(i).optString("fingerNumber").equals("7")) {
						validateIneObj = new JSONObject();
					log.info("decrypt---7");
					validateIneObj.put("type", "WSQ");
					validateIneObj.put("fingerPrint", decrypt.
							decrypt(biometrics.getJSONObject(i).optString("fingerPrint")));
					validateIneObj.put("fingerNumber",7);
					validateIneArr.put(validateIneObj);
				}
			}
			other.put("biometrics", validateIneArr);
			json.put("others", other);
			log.info("INFO: {}.ineValidate()", this.getClass().getName());
		} 
		json.put("isValidateIne", true);
	
		ResponseEntity<String> result = null;
	
		// enviando solicitud a validacion ine ..
		result = identificationClient.ineValidate(json.toString());
		JSONObject inevalidation = new JSONObject(result.getBody());
		log.info("inevalidation Response>>>: "+inevalidation.toString());
		String errorCode = inevalidation.optString("errorCode");
		if(errorCode == null || errorCode.isEmpty()){
			errorCode = "01";
		}
		// desencriptando minucias para enrrolamiento ..
		JSONObject jsonToSend = new JSONObject();
		jsonToSend.put("username", json.optString("name")+"");
		JSONObject jsonAddMinucias = new JSONObject(validateIneRequest);
		JSONArray biometricsMbss = jsonAddMinucias.optJSONObject("others").optJSONArray("biometrics");
		
		log.info("Decript for");
		for (int i=0; i<biometricsMbss.length(); i++) {
			log.info(biometricsMbss.getJSONObject(i).optString("fingerNumber"));
			if (biometricsMbss.getJSONObject(i).optString("fingerNumber").equals("1") &&
					!biometricsMbss.getJSONObject(i).optString("fingerPrint").isEmpty()) {
				log.info(biometricsMbss.getJSONObject(i).optString("fingerNumber"));
				jsonToSend.put("lt", decrypt.decrypt(biometricsMbss.getJSONObject(i).optString("fingerPrint")));
			} else if (biometricsMbss.getJSONObject(i).optString("fingerNumber").equals("2") &&
					!biometricsMbss.getJSONObject(i).optString("fingerPrint").isEmpty()) {
				log.info(biometricsMbss.getJSONObject(i).optString("fingerNumber"));
				jsonToSend.put("li", decrypt.decrypt(biometricsMbss.getJSONObject(i).optString("fingerPrint")));
			} else if (biometricsMbss.getJSONObject(i).optString("fingerNumber").equals("3") &&
					!biometricsMbss.getJSONObject(i).optString("fingerPrint").isEmpty()) {
				log.info(biometricsMbss.getJSONObject(i).optString("fingerNumber"));
				jsonToSend.put("lm", decrypt.decrypt(biometricsMbss.getJSONObject(i).optString("fingerPrint")));
			} else if (biometricsMbss.getJSONObject(i).optString("fingerNumber").equals("4") &&
					!biometricsMbss.getJSONObject(i).optString("fingerPrint").isEmpty()) {
				jsonToSend.put("lr", decrypt.decrypt(biometricsMbss.getJSONObject(i).optString("fingerPrint")));
			} else if (biometricsMbss.getJSONObject(i).optString("fingerNumber").equals("5") &&
					!biometricsMbss.getJSONObject(i).optString("fingerPrint").isEmpty()) {
				jsonToSend.put("ll", decrypt.decrypt(biometricsMbss.getJSONObject(i).optString("fingerPrint")));
			} else if (biometricsMbss.getJSONObject(i).optString("fingerNumber").equals("6") &&
					!biometricsMbss.getJSONObject(i).optString("fingerPrint").isEmpty()) {
				jsonToSend.put("rt", decrypt.decrypt(biometricsMbss.getJSONObject(i).optString("fingerPrint")));
			} else if (biometricsMbss.getJSONObject(i).optString("fingerNumber").equals("7") &&
					!biometricsMbss.getJSONObject(i).optString("fingerPrint").isEmpty()) {
				jsonToSend.put("ri", decrypt.decrypt(biometricsMbss.getJSONObject(i).optString("fingerPrint")));
			} else if (biometricsMbss.getJSONObject(i).optString("fingerNumber").equals("8") &&
					!biometricsMbss.getJSONObject(i).optString("fingerPrint").isEmpty()) {
				jsonToSend.put("rm", decrypt.decrypt(biometricsMbss.getJSONObject(i).optString("fingerPrint")));
			} else if (biometricsMbss.getJSONObject(i).optString("fingerNumber").equals("9") &&
					!biometricsMbss.getJSONObject(i).optString("fingerPrint").isEmpty()) {
				jsonToSend.put("rr", decrypt.decrypt(biometricsMbss.getJSONObject(i).optString("fingerPrint")));
			} else if (biometricsMbss.getJSONObject(i).optString("fingerNumber").equals("10") &&
					!biometricsMbss.getJSONObject(i).optString("fingerPrint").isEmpty()) {
				jsonToSend.put("rl", decrypt.decrypt(biometricsMbss.getJSONObject(i).optString("fingerPrint")));
			} 
		}

		log.info("addMinucias Request: "+LogUtil.logJsonObject(jsonToSend));
		log.info("errorCode: "+errorCode);
		// fallo informacion	
		if (errorCode.equals("07") || errorCode.equals("01") || errorCode.equals("05") || errorCode.equals("08")) {
				log.info("addMinucias GRAY");
			ResponseEntity<String> responseEntity = biometricClient.addMinucias(jsonToSend.toString(),
					Long.parseLong(json.optString("idOperation")), 11);
		}
		if (errorCode.equals("00")) {
				log.info("addMinucias INE");
			ResponseEntity<String> responseEntity = biometricClient.addMinucias(jsonToSend.toString(),
					Long.parseLong(json.optString("idOperation")), 10);
		}

		/*
		 * if(errorCode.equals("33")) { log.info("BYPASS: "+errorCode); JSONObject
		 * respuesta = new JSONObject(); respuesta.put("request",
		 * LogUtil.logJsonObject(jsonToSend)+""); respuesta.put("response",
		 * LogUtil.logJsonObject(jsonToSend)+""); respuesta.put("errorCode", "00");
		 * log.info("BYPASS: "+errorCode+">>>>>>"+respuesta.toString()); return new
		 * ResponseEntity<>(respuesta.toString(), HttpStatus.OK); }
		 */
				
		return result;
	}
	


	@ApiOperation(value = "verifi disable curp")
	@RequestMapping(value = "/logCurp", method = RequestMethod.POST)
	public ResponseEntity<String> logCurp(@RequestBody String req) {
		log.info("INFO: {}.ineValidate()", this.getClass().getName());
		return identificationClient.logCurp(req);
	}

}
