package com.teknei.bid.controller.rest;

import com.teknei.bid.dto.BidDocuRelDTO;
import com.teknei.bid.service.remote.CustomerClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/v3/enrollment/documentRel")
@CrossOrigin
public class EnrollmentDocumentRelationController {

    private static final Logger log = LoggerFactory.getLogger(EnrollmentDocumentRelationController.class);
    @Autowired
    private CustomerClient customerClient;

    @RequestMapping(value = "/findRelActive", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<BidDocuRelDTO>> findActiveDocumentRelations() {
    	//log.info("lblancas: "+this.getClass().getName()+".{findActiveDocumentRelations() }");
        try {
            return customerClient.findActiveDocumentRelations();
        } catch (Exception e) {
            log.error("Error finding active documents with message: {}", e.getMessage());
            return new ResponseEntity<>((List<BidDocuRelDTO>) null, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

}
