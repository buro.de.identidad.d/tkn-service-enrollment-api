package com.teknei.bid.controller.rest.util.crypto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import java.math.BigInteger;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class RSA {


    public static final String ALGORITHM = "RSA/ECB/PKCS1Padding";
    private static final Logger log = LoggerFactory.getLogger(RSA.class);

    /**
     * @return
     * @throws NoSuchAlgorithmException
     */
    public KeyPair buildKeyPair() throws NoSuchAlgorithmException {
    	//log.info("lblancas: "+this.getClass().getName()+".{buildKeyPair() }");
        final int keySize = 2048;
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(keySize);
        return keyPairGenerator.genKeyPair();
    }

    /**
     * @param text
     * @param key
     * @return
     */
    public byte[] encrypt(String text, byte[] key) {
    	//log.info("lblancas: "+this.getClass().getName()+".{encrypt() }");
        byte[] cipherText = null;
        try {
            final Cipher cipher = Cipher.getInstance(ALGORITHM);
            KeyFactory kf = KeyFactory.getInstance("RSA");
            PublicKey publicKey = kf.generatePublic(new X509EncodedKeySpec(key));
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            cipherText = cipher.doFinal(text.getBytes());
        } catch (Exception e) {
            log.error("Could not encrypt: {}", e.getMessage());
        }
        return cipherText;
    }

    /**
     * @param text
     * @param key
     * @return
     */
    public String decrypt(byte[] text, byte[] key) {
    	//log.info("lblancas: "+this.getClass().getName()+".{decrypt() }");
        byte[] dectyptedText = null;
        try {

            final Cipher cipher = Cipher.getInstance(ALGORITHM);
            KeyFactory kf = KeyFactory.getInstance("RSA");
            PrivateKey privateKey = kf.generatePrivate(new PKCS8EncodedKeySpec(key));

            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            dectyptedText = cipher.doFinal(text);

        } catch (Exception ex) {
            log.error("Could not decrypt: {}", ex.getMessage());
        }

        return new String(dectyptedText);
    }

    /**
     * @param e
     * @param m
     * @return
     */
    public PublicKey bigIntegerToPublicKey(BigInteger e, BigInteger m) {
    	//log.info("lblancas: "+this.getClass().getName()+".{bigIntegerToPublicKey() }");
        RSAPublicKeySpec keySpec = new RSAPublicKeySpec(m, e);
        try {
            KeyFactory fact = KeyFactory.getInstance("RSA");
            PublicKey pubKey = (PublicKey) fact.generatePublic(keySpec);
            return pubKey;
        } catch (Exception ex) {
            log.error("Could not 'bigIntegerToPublicKey': {} ", ex.getMessage());
            return null;
        }
    }

    /**
     * @param d
     * @param m
     * @return
     */
    public PrivateKey bigIntegerToPrivateKey(BigInteger d, BigInteger m) {
    	//log.info("lblancas: "+this.getClass().getName()+".{bigIntegerToPrivateKey() }");
        RSAPrivateKeySpec keySpec = new RSAPrivateKeySpec(m, d);
        try {
            KeyFactory fact = KeyFactory.getInstance("RSA");
            PrivateKey privKey = fact.generatePrivate(keySpec);
            return privKey;
        } catch (Exception ex) {
            log.error("Could not 'bigIntegerToPrivateKey': {} ", ex.getMessage());
            return null;
        }
    }
}
