package com.teknei.bid.controller.rest.unauth;

import com.teknei.bid.controller.rest.CryptoController;
import com.teknei.bid.dto.CipherRequestDTO;
import com.teknei.bid.dto.CipherResponseDTO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@RestController
@RequestMapping(value = "/rest/unauth/crypto")
@CrossOrigin
public class CryptoControllerUnauth {
	private static final Logger log = LoggerFactory.getLogger(EnrollmentBiometricControllerUnauth.class);
    @Autowired
    private CryptoController controller;

    @ApiOperation(value = "Ciphers a string base 64 value", response = CipherResponseDTO.class)
    @RequestMapping(value = "/crypto", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CipherResponseDTO> cipherValue(@RequestBody CipherRequestDTO requestDTO) {
    	//log.info("lblancas: "+this.getClass().getName()+".{cipherValue(POST) }");
        return controller.cipherValue(requestDTO);
    }

}
