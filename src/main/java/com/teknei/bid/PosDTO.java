package com.teknei.bid;

import lombok.Data;

import java.io.Serializable;

@Data
public class PosDTO implements Serializable {

	   private String text;
	   private int x;
	   private int y;
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	   
	   
}