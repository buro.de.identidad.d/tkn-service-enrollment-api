package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class FideicomisariosDTO implements Serializable {

	private String nombre = "";
	private String edad = "";
	private String parentesco = "";
	private String porcentaje = "";
	List<FideicomisariosDTO> segundoLugar = new ArrayList<FideicomisariosDTO>();
	
}