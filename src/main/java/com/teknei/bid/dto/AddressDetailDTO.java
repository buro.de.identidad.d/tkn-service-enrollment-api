package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class AddressDetailDTO implements Serializable {

    private String street;
    private String extNumber;
    private String intNumber;
    private String suburb;
    private String locality;
    private String zipCode;
    private String municipio;
    private String state;
    private String country;
    private String username;

}