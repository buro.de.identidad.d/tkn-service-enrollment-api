package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class FacialRequestDTO implements Serializable {

    private Long operationId;
    private String facialB64;

}