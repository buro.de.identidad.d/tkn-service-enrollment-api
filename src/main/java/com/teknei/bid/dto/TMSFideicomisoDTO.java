package com.teknei.bid.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class TMSFideicomisoDTO implements Serializable {
	
	private String nombre = "";
    private String apPat = "";
    private String apMat = "";
    private String fechaNaci = "";
    private String profecion = "";
    private String nacionalidad = "";
    private String calle = "";
    private String numExt = "";
    private String numInt = "";
    private String colonia = "";
    private String municipio = "";
    private String estado = "";
    private String codigoP = "";
    private String paisNaci = "";
    private String email = "";
    private String curp = "";
    private String numTel_Principal = "";
    private String telOficina = "";
    private String realNombre = "";
    private String realAppat = "";
    private String realApmat = "";
    private String realOcupacion = "";
    private String realNacionalidad = "";
    private String realFechaNaci = "";
    private String realCurp = "";
    private String indexLeft = "";
    private boolean conhuella;
	
	@Override
	public String toString() {
		return "SignContractRequestDTO [nombre =" + nombre  
				+ ", apPat =" + apPat + ", apMat =" + apMat + ", fechaNaci =" + fechaNaci + ", profecion =" + profecion + ", nacionalidad =" + nacionalidad
				+ ", calle =" + calle + ", numExt =" + numExt + ", numInt =" + numInt + ", colonia =" + colonia
				+ ", curp =" + curp + ", municipio =" + municipio + ", estado =" + estado + ", codigoP =" + codigoP
				+ ", paisNaci =" + paisNaci + ", email =" + email + ", numTel_Principal =" + numTel_Principal
				+ ", telOficina =" + telOficina + ", realNombre =" + realNombre + ", realAppat =" + realAppat + ", realApmat =" + realApmat
				+ ", realOcupacion =" + realOcupacion + ", realNacionalidad =" + realNacionalidad + ", realFechaNaci =" + realFechaNaci
				+ ", realCurp =" + realCurp + ", indexLeft =" + indexLeft + ", conhuella =" + conhuella + "]";
	}

}
