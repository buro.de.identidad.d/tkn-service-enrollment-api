package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class CurpRequestByExternalData implements Serializable {

    private String name;
    private String surname;
    private String surnameLast;
    private String birthDate;
    private String entidad;
    private String gender;

}