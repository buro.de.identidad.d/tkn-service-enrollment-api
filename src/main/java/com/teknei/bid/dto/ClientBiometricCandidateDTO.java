package com.teknei.bid.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class ClientBiometricCandidateDTO implements Serializable{

    private Long id;
    private String curp;
    private String scanId;
    private String documentId;
    private String urlPhoto1;
    private String urlPhoto2;
    private String urlPhoto3;

}