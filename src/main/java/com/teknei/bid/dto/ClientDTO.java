package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ClientDTO implements Serializable {

    private Long id;
    private List<Integer> idClientType;
    private String username;
    private String name;
    private String surnameFirst;
    private String surnameLast;
    private String email;
    private String curp;
    private List<ClieTelDTO> telephones;
    private String obs;
    private Long emprId;
    private Integer telType;

}