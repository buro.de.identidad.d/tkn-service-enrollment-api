package com.teknei.bid.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class FideicomisoDTO implements Serializable {

	String patrimonial;
	String beneficiario;

}

