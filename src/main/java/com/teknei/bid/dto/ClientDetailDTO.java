package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class ClientDetailDTO extends ClientDTO implements Serializable{

    private String scanId;
    private String documentId;
    private String dire;
    private String personalNumber;
    private String mrz;
    private String ocr;
    private String vig;
    private String curpDocument;

}