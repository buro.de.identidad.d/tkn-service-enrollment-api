package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class OTPVerificationDTO implements Serializable{

	private static final long serialVersionUID = -2453862003547119596L;
	private Long idClient;
    private String otp;
    private String username;
}