package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class BidClientAccountDestinyDTO implements Serializable {

    private Long idClient;
    private Long idCreditInstitution;
    private String clabe;
    private String alias;
    private Long ammount;
    private Boolean active;
    private String username;
    private Boolean newIndicator;
    private String holder;
}