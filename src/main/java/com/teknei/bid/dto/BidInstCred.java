package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
public class BidInstCred implements Serializable {
    private Long idInstCred;
    private String abm;
    private String nomCort;
    private String nomLarg;
    private Integer idEsta;
    private Integer idTipo;
    private String usrCrea;
    private Timestamp fchCrea;
    private String usrModi;
    private Timestamp fchModi;
}
