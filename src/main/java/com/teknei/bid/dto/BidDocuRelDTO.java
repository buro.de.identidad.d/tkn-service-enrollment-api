package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class BidDocuRelDTO implements Serializable {

    private Long id;
    private String code;
    private String desc;
    private Boolean front;
    private Boolean back;
    private List<BidDocuRelDTO> children;

}