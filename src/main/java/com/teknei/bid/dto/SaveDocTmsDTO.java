package com.teknei.bid.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class SaveDocTmsDTO implements Serializable {
	
	private byte [] documento;
    private boolean firma;
    private Long id;
    private ArrayList<byte[]> arrayDoc;

}
