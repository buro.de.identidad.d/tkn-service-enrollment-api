package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class PersonalDataCredentialDTO implements Serializable {

    private int status;
    private String name;
    private String lastnameFirst;
    private String lastnameLast;

}