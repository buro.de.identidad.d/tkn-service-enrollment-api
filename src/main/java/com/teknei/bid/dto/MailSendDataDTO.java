package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class MailSendDataDTO implements Serializable {

    private String otp;
    private String mail;
    private String fullname;
    private String optional;

}