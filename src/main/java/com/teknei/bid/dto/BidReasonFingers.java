package com.teknei.bid.dto;

public class BidReasonFingers {
	private long idreason;
    private String description;
    private String code;
	public long getIdreason() {
		return idreason;
	}
	public void setIdreason(long idreason) {
		this.idreason = idreason;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
    
}
