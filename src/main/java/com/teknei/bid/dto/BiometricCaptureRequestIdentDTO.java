package com.teknei.bid.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@EqualsAndHashCode(of = {"id", "serial"})
@AllArgsConstructor
@NoArgsConstructor
public class BiometricCaptureRequestIdentDTO implements Serializable {

    private Long id;
    private String serial;
    private String username;
    private String type;

}