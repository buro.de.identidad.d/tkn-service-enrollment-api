package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
public class CredentialsTempDTO implements Serializable {

    private Long idCredTemp;
    private String usua;
    private String nomOpe;
    private String apePate;
    private String apeMate;
    private Integer noDact;
    private Long idDisp;
    private String serial;
    private Long idEstaCred;
    private String estaCredDsc;
    private Timestamp fchCrea;

}