package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by Amaro on 07/08/2017.
 */
@Data
public class OperationResult implements Serializable{

    private boolean resultOK;
    private String errorMessage;

}
