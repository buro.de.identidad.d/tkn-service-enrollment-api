package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class StepStatusDTO implements Serializable {

    private String codEstaProc;
    private String descEstaProc;
    private Boolean accomplished;
    private Long timestamp;
    private String userAccomplished;
    private String platformAccomplished;

}