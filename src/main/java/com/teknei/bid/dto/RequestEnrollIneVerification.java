package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class RequestEnrollIneVerification implements Serializable {

    private String slapLeftB64;
    private String slapRightB64;
    private String slapThumbsB64;
    private String leftIndexB64;
    private String rightIndexB64;
    private Long operationId;

}