package com.teknei.bid.dto;

import lombok.Data;
import java.io.Serializable;

@Data
public class ContratoTmsDTO implements Serializable {
	
	private String nombre = "";
    private String apPat = "";
    private String apMat = "";
    private String calle = "";
    private String numExt = "";
    private String numInt = "";
    private String colonia = "";
    private String municipio = "";
    private String estado = "";
    private String codigoP = "";
    private String curp = "";
    private String numTel_Principal = "";
    private String indexLeft = "";
    private boolean conhuella;
    private String operation_id;
    
    @Override
	public String toString() {
		return "SignContractRequestDTO [nombre =" + nombre  
				+ ", apPat =" + apPat + ", apMat =" + apMat + ", calle =" + calle + ", numExt =" + numExt + ", numInt =" + numInt + ", colonia =" + colonia
				+ ", curp =" + curp + ", municipio =" + municipio + ", estado =" + estado + ", codigoP =" + codigoP
				+ ", numTel_Principal =" + numTel_Principal + ", indexLeft =" + indexLeft + ", conhuella =" + conhuella + ", operationID =" + operation_id + "]";
	}
}
