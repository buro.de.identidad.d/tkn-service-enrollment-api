package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class BidEmprUsua implements Serializable {
    private Long idEmpr;
    private Long idUsua;
}
