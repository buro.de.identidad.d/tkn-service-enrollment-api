package com.teknei.bid.dto;
import java.io.Serializable;
import java.util.Date;

public class ServiceConfigurationDTO  implements Serializable 
{
	private Long   operationId;
	private int    settingscersion;  //SETTINGS_VERSION
	private String urlidscan;        // URL_ID_SCAN
	private String licenseidscan;    // LICENSE_ID_SCAN
	private String urlteknei;        // URL_TEKNEI
	private String urlmobbsign;      // URL_MOBBSIGN
	private String licenseobbsign;  // MOBBSIGN_LICENSE
	private String urlauthaccess;    // URL_AUTHACCESS
	private String fingerprintreader;// FINGERPRINT_READER
	private String idcompany;        // ID_ENTERPRICE
	private String timeoutServices;  // TIMEOUT_SERVICES
	private String timelatency;      // TIME_WAIT_LATENCY
	private String urlhostbid;       //URL_HOST_BID
	private String urlportbid;       //URL_PORT_BID
	
	public ServiceConfigurationDTO() 
	{
		String fecha=""+(new Date()).getTime();
		int settingscersion=1;
		String urlidscan="http:\\test:7812\t";
		String licenseidscan="LIC9847";
		String urlteknei="http:\\test:7812\t";
		String urlmobbsign="http:\\test:7834\t";
		String licenseobbsign="lic.987\t";
		String urlauthaccess="http:\\test:7878\t";
		String fingerprintreader="Finger";
		String idcompany="1";
		String timeoutServices=fecha;
		String timelatency="500";
		String urlhostbid="http:\\test:9090\t";
		String urlportbid="http:\\test:9898\t";
		this.settingscersion = settingscersion;
		this.urlidscan = urlidscan;
		this.licenseidscan = licenseidscan;
		this.urlteknei = urlteknei;
		this.urlmobbsign = urlmobbsign;
		this.licenseobbsign = licenseobbsign;
		this.urlauthaccess = urlauthaccess;
		this.fingerprintreader = fingerprintreader;
		this.idcompany = idcompany;
		this.timeoutServices = timeoutServices;
		this.timelatency = timelatency;
		this.urlhostbid = urlhostbid;
		this.urlportbid = urlportbid;
	}
	public int getSettingscersion() {
		return settingscersion;
	}
	public void setSettingscersion(int settingscersion) {
		this.settingscersion = settingscersion;
	}
	public String getUrlidscan() {
		return urlidscan;
	}
	public void setUrlidscan(String urlidscan) {
		this.urlidscan = urlidscan;
	}
	public String getLicenseidscan() {
		return licenseidscan;
	}
	public void setLicenseidscan(String licenseidscan) {
		this.licenseidscan = licenseidscan;
	}
	public String getUrlteknei() {
		return urlteknei;
	}
	public void setUrlteknei(String urlteknei) {
		this.urlteknei = urlteknei;
	}
	public String getUrlmobbsign() {
		return urlmobbsign;
	}
	public void setUrlmobbsign(String urlmobbsign) {
		this.urlmobbsign = urlmobbsign;
	}
	public String getLicenseobbsign() {
		return licenseobbsign;
	}
	public void setLicenseobbsign(String licenseobbsign) {
		this.licenseobbsign = licenseobbsign;
	}
	public String getUrlauthaccess() {
		return urlauthaccess;
	}
	public void setUrlauthaccess(String urlauthaccess) {
		this.urlauthaccess = urlauthaccess;
	}
	public String getFingerprintreader() {
		return fingerprintreader;
	}
	public void setFingerprintreader(String fingerprintreader) {
		this.fingerprintreader = fingerprintreader;
	}
	public String getIdcompany() {
		return idcompany;
	}
	public void setIdcompany(String idcompany) {
		this.idcompany = idcompany;
	}
	public String getTimeoutServices() {
		return timeoutServices;
	}
	public void setTimeoutServices(String timeoutServices) {
		this.timeoutServices = timeoutServices;
	}
	public String getTimelatency() {
		return timelatency;
	}
	public void setTimelatency(String timelatency) {
		this.timelatency = timelatency;
	}
	public String getUrlhostbid() {
		return urlhostbid;
	}
	public void setUrlhostbid(String urlhostbid) {
		this.urlhostbid = urlhostbid;
	}
	public String getUrlportbid() {
		return urlportbid;
	}
	public void setUrlportbid(String urlportbid) {
		this.urlportbid = urlportbid;
	}
	@Override
	public String toString() {
		return "ServiceConfigurationDTO [settingscersion=" + settingscersion + ", urlidscan=" + urlidscan
				+ ", licenseidscan=" + licenseidscan + ", urlteknei=" + urlteknei + ", urlmobbsign=" + urlmobbsign
				+ ", licenseobbsign=" + licenseobbsign + ", urlauthaccess=" + urlauthaccess + ", fingerprintreader="
				+ fingerprintreader + ", idcompany=" + idcompany + ", timeoutServices=" + timeoutServices
				+ ", timelatency=" + timelatency + ", urlhostbid=" + urlhostbid + ", urlportbid=" + urlportbid + "]";
	}
		

}
