package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class OpenCasefileDTO implements Serializable {

    private Long operationId;
    private String username;
    private String userOpeCrea;

}