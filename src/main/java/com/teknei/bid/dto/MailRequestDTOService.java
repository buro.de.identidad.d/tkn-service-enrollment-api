package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class MailRequestDTOService implements Serializable {

    private Long idClient;
    private String username;

}
