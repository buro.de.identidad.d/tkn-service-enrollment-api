package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class CurpRequestDTO implements Serializable {

    private Integer type;
    private Boolean document;
    private CurpRequestByCustomerData requestByCustomerData;
    private CurpRequestByExternalData requestByExternalData;
    private CurpValidateByCustomerData validateByCustomerData;
    private CurpValidateByExternalData validateByExternalData;

}