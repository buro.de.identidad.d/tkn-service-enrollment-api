package com.teknei.bid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
 
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableDiscoveryClient
@EnableSwagger2
@EnableFeignClients
@EnableHystrix
public class TknServiceEnrollmentApiApplication {
	private static final Logger log = LoggerFactory.getLogger(TknServiceEnrollmentApiApplication.class);
    public static void main(String[] args) {
        SpringApplication.run(TknServiceEnrollmentApiApplication.class, args);
    }

    @Bean
    public Docket api() {
    	//log.info("lblancas:: method:api()");
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.teknei.bid.controller.rest.unauth"))
                .paths(PathSelectors.any())
                .build();
    }

    @Bean
    public TaskExecutor threadPoolTaskExecutor() 
    {
    	//log.info("lblancas:: method:threadPoolTaskExecutor()");
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(4);
        executor.setMaxPoolSize(4);
        executor.setThreadNamePrefix("default_task_executor_thread");
        executor.initialize();
        return executor;
    }

}
